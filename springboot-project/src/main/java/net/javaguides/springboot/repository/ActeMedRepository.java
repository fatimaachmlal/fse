package net.javaguides.springboot.repository;

import java.util.Date;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import net.javaguides.springboot.model.ActeMed;
import net.javaguides.springboot.model.ActeRadio;
import net.javaguides.springboot.model.Medecin;

@Repository
public interface ActeMedRepository extends JpaRepository<ActeMed, Long> {
	 //and AM.Ordonnance_Radiologie=false
	  List<ActeMed> findAllByMedecin(Medecin medcin);
	
		@Query("SELECT new ActeMed(AM.id,P.nom,P.prenom,P.cin,P.telephone , AM.dateActe,AM.medecin.id , P.id) FROM Adherent as A , Personne as P, ActeMed as AM ,Fse as f where A.id = P.id and A.id = AM.adherent.id and f.acteMed.id = AM.id and AM.Ordonnance_Radiologie='true' and f.acteRadio.id is null and P.cin=:cin order by AM.dateActe")
		List<ActeMed> findActeMedByCinAdherent(@Param("cin") String cin);
		
		@Query("SELECT new ActeMed(P.id,P.nom,P.prenom,P.cin,P.telephone , AM.dateActe ) FROM Medecin as M , Personne as P, ActeMed as AM  where M.id = P.id and M.id = AM.medecin.id and AM.medecin.id=:id")
		List<ActeMed> findActeMedByMedecin(@Param("id") long id);
		

		@Query("SELECT   new ActeMed(AM.id, AM.codeActe,AM.lettreCle,AM.prix,AM.dateActe,P.nom,P.prenom,P.telephone ,P.cin,AM.Ordonnance_Biologie,AM.Ordonnance_Radiologie, AM.Ordonnance_Pharmacie,AM.Type_de_soins,AM.Ald ,f.numeroDossier) FROM Adherent as A , Personne as P, ActeMed as AM ,Fse as f where AM.adherent.id = A.id and A.id = P.id and f.acteMed.id=AM.id and AM.medecin.id=:IdMedecin")
		List<ActeMed> findActeMedecinAdherentinfo( @Param("IdMedecin")long IdMedecin);
		
		
		@Query("SELECT new ActeMed(P.nom,P.prenom,P.telephone ) FROM Medecin as M , Personne as P, ActeMed as AM  where AM.medecin.id = M.id and M.id = P.id and AM.id=:IdActMed")
		 List<ActeMed> findActeMedecinMedinfoByIdActMed( @Param("IdActMed")long IdActMed);
		
}
