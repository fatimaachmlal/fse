package net.javaguides.springboot.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

import net.javaguides.springboot.model.Admin;
import net.javaguides.springboot.model.Medecin;
import net.javaguides.springboot.model.User;


public interface AdminRepository extends JpaRepository<Admin, Long>{
	 Optional<Admin> findByUsername(String username);

	  Boolean existsByUsername(String username);
	  Boolean existsByEmail(String email);

}
