package net.javaguides.springboot.repository;

import java.time.LocalDateTime;
import java.util.Date;
import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import net.javaguides.springboot.model.ActeMed;
import net.javaguides.springboot.model.Fse;
import net.javaguides.springboot.model.Sexe;

@Repository
public interface FseRepository extends JpaRepository<Fse, Long> {
	@Query("SELECT new Fse(F.id) FROM ActeMed as AM , Medecin as M, Fse as F  where M.id = AM.medecin.id and AM.id = F.acteMed.id and M.id=:id_med and AM.id=:id_act_med and AM.dateActe=:DateActe")
	List<Fse> findFseByMedAdherentActe( long id_act_med,long id_med,Date DateActe );
	

	
    @Transactional
    @Modifying
    @Query("UPDATE Fse f SET f.acteLabo.id = :IdActeLabo WHERE f.id = :fseId")
    int AddActeLaboToFSE(@Param("IdActeLabo") long IdActeLabo, @Param("fseId") long fseId);
    
    @Transactional
    @Modifying
    @Query("UPDATE Fse f SET f.acteRadio.id = :IdActeRadio WHERE f.id = :fseId")
    int AddActeRadioToFSE(@Param("IdActeRadio") long IdActeRadio, @Param("fseId") long fseId);
    
    @Transactional
    @Modifying
    @Query("UPDATE Fse f SET f.actePhar.id = :IdActePharm WHERE f.id = :fseId")
    int AddActePharToFSE(@Param("IdActePharm") long IdActePharm, @Param("fseId") long fseId);
    

   // 
    @Query("SELECT distinct  new Fse(P.immatriculation,P.nom, P.prenom,P.cin,P.adresse,P.ville,P.dateNaissance,P.sexe) FROM Adherent as A , Personne as P, Fse as F  where F.adherent.id = A.id and A.id = P.id and F.adherent.id=:id_adherent")
    Fse findFseWithAdhrentInfo(@Param("id_adherent") long id_adherent);
    
    
    @Query("SELECT new Fse(P.nom, P.prenom, P.adresse, P.ville,P.telephone, M.inpe, AM.lettreCle, AM.dateActe,AM.prix,P.specialite) FROM ActeMed as AM , Medecin as M,  Personne as P,Fse as F  where F.acteMed.id=AM.id and AM.medecin.id=M.id and M.id=P.id and F.adherent.id=:id_adherent and F.id=:Id_fse ")
    Fse findFseWithMedecintInfo(@Param("id_adherent") long id_adherent,@Param("Id_fse")long Id_fse);
    
    @Query("SELECT new Fse( P.adresse, P.ville, R.inpe, AR.lettreCle, AR.dateActe,AR.ngap,R.raisonSociale,AR.prix) FROM ActeRadio as AR ,Personne as P , Radiologie as R,Fse as F  where F.acteRadio.id=AR.id and AR.radiologie.id=R.id and R.id=P.id and  F.adherent.id=:id_adherent and F.id=:Id_fse ")
    Fse findFseWithRadiotInfo(@Param("id_adherent") long id_adherent,@Param("Id_fse")long Id_fse);

    @Query("SELECT new Fse(P.adresse, P.ville, L.inpe, AL.lettreCle, AL.dateActe,AL.ngap,L.raisonSociale,AL.prix) FROM ActeLabo as AL ,Personne as P , Laboratoire as L , Fse as F  where F.acteLabo.id=AL.id and AL.laboratoire.id=L.id and L.id=P.id and  F.adherent.id=:id_adherent and F.id=:Id_fse ")
    Fse findFseWithLaboInfo(@Param("id_adherent") long id_adherent,@Param("Id_fse")long Id_fse);

    
    
    @Query("SELECT new Fse( P.adresse, P.ville,Ph.inpe, AP.prix, AP.dateActe,Ph.raisonSociale) FROM ActePhar as AP ,Personne as P , Pharmacie as Ph , Fse as F  where F.actePhar.id=AP.id and AP.pharmacie.id=Ph.id and Ph.id=P.id and  F.adherent.id=:id_adherent and F.id=:Id_fse")
    Fse findFseWithPharmaInfo(@Param("id_adherent") long id_adherent,@Param("Id_fse")long Id_fse);

    @Query("SELECT new Fse(F.id, F.numeroDossier,P.nom, P.prenom,AM.dateActe ) FROM ActeMed as AM , Medecin as M,Personne as P ,Fse as F  where F.acteMed.id=AM.id and AM.medecin.id=M.id and M.id=P.id and F.adherent.id=:id_adherent")
    List<Fse> findFseByAdherent(@Param("id_adherent") long id_adherent);
    
    
    
     
    
    
    
    

}