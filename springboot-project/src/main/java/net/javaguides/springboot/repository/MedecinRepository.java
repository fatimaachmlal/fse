package net.javaguides.springboot.repository;


import java.util.Date;
import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import net.javaguides.springboot.model.Medecin;
import net.javaguides.springboot.model.User;
@Repository
public interface MedecinRepository extends JpaRepository<Medecin, Long>{

	  Optional<Medecin> findByUsername(String username);

	  Boolean existsByUsername(String username);

	  Boolean existsByEmail(String email);
	  @Query("SELECT new Medecin(P.nom,P.prenom,P.cin,P.telephone,M.inpe,M.ice,P.id,P.email,P.sexe,P.ville,P.adresse,P.codePostal,M.typeMutuelle,M.specialite,P.dateNaissance,P.motDePasse) FROM Medecin as M ,Personne as P where P.id=M.id and  P.id=:id ")
	  public Medecin findProfileMedecinById(@Param("id") Long id);
	  
	  @Query("SELECT COUNT(M) FROM Medecin as M, Admin as A where A.id=:IdAmin")
		long countMedecin(@Param("IdAmin")long IdAmin);
	  
//	@Query("SELECT new Medecin(P.id,P.nom,P.prenom,P.dateNaissance,P.cin,P.email,P.telephone,P.motDePasse,P.adresse,P.ville,P.codePostal,P.sexe,P.situationFamiliale,P.specialite,P.inpe,P.ice,P.typeMutuelle,P.username,I.cachetSignature,I.PhotoProfile,I.carteProfessionnelle,I.photoCarteCin) FROM Medecin as P inner join Image as I on (P.id = I.medecin.id)  where P.id=:id")
	//public List<Medecin> findProfilMedecinComplet(@Param("id") Long id);
}
