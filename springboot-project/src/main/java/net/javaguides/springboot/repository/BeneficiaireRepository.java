package net.javaguides.springboot.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;


import net.javaguides.springboot.model.Beneficiaire;

@Repository
public interface BeneficiaireRepository extends JpaRepository<Beneficiaire, Long>{
	
	 @Query("SELECT new Beneficiaire(B.id,B.nom,B.prenom,B.cin,B.dateNaissance,B.sexe,B.lieuNaissance,B.nDossier,B.codeAld,B.relationFamiliale,B.adherent) FROM Beneficiaire as B where B.adherent.id=:id_adherent")	 		
	    List<Beneficiaire> findBeneficiaireByAdherent(@Param("id_adherent") long id_adherent);
	 
	 @Query("SELECT new Beneficiaire(B.adherent,B.id,B.nom,B.prenom,B.cin,B.lieuNaissance,B.relationFamiliale) FROM Beneficiaire as B where B.adherent.cin=:cin")	 		
	    List<Beneficiaire> findBeneficiaireByAdherentCin(@Param("cin") String cin);
	

}
