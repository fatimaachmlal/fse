package net.javaguides.springboot.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import net.javaguides.springboot.model.Laboratoire;
import net.javaguides.springboot.model.Radiologie;

@Repository
public interface RadiologieRepository extends JpaRepository<Radiologie, Long>{
	Optional<Radiologie> findByUsername(String username);
	
	 @Query("SELECT COUNT(R) FROM Radiologie as R, Admin as A where A.id=:IdAmin")
		
	   long  countRadiologie(@Param("IdAmin")long IdAmin);

}
