package net.javaguides.springboot.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import net.javaguides.springboot.model.Laboratoire;
import net.javaguides.springboot.model.Medecin;

@Repository
public interface LaboratoireRepository extends JpaRepository<Laboratoire, Long> {
	Optional<Laboratoire> findByUsername(String username);
	Boolean existsByUsername(String username);

	  Boolean existsByEmail(String email);
	  @Query("SELECT COUNT(L) FROM Laboratoire as L")
	
	   long  countLaboratoire();
}
