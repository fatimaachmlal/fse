package net.javaguides.springboot.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import net.javaguides.springboot.model.Personne;

@Repository
public interface PersonneRepostiry extends JpaRepository<Personne, Long>{

}
