package net.javaguides.springboot.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import net.javaguides.springboot.model.ActeMed;
import net.javaguides.springboot.model.ActePhar;
import net.javaguides.springboot.model.ActeRadio;

@Repository
public interface ActePharRepository extends JpaRepository<ActePhar, Long> {
	@Query("SELECT new ActeMed(AM.id,P.nom,P.prenom,P.cin,P.telephone , AM.dateActe,AM.medecin.id,P.id ) FROM Adherent as A , Personne as P, ActeMed as AM ,Fse as f where A.id = P.id and A.id = AM.adherent.id and f.acteMed.id = AM.id and AM.Ordonnance_Pharmacie='true' and f.actePhar.id is null and P.cin=:cin order by AM.dateActe")
	List<ActeMed> findActeMedPharmaByCinAdherent(@Param("cin") String cin);
	


	@Query("SELECT new ActePhar(AP.id,AP.prix,AP.dateActe,P.nom,P.prenom,P.telephone ,P.cin,f.numeroDossier ) FROM Adherent as A , Personne as P, ActePhar as AP,Fse as f where AP.adherent.id = A.id and A.id = P.id and  f.actePhar.id=AP.id and AP.pharmacie.id=:IdPharma  Order By AP.dateActe DESC")
	List<ActePhar> findActePharmaAdherentinfo( @Param("IdPharma")long IdPharma);
	
	@Query("SELECT new ActePhar(P.nom,P.prenom,P.telephone ) FROM Medecin as M , Personne as P, ActePhar as AP where AP.medecin.id = M.id and M.id = P.id and AP.id=:IdActPharma")
	 List<ActePhar> findActePharmaMedinfoByIdActPharma( @Param("IdActPharma")long IdActPharma);
}
