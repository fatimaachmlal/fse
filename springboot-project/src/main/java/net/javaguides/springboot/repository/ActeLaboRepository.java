package net.javaguides.springboot.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import net.javaguides.springboot.model.ActeLabo;
import net.javaguides.springboot.model.ActeMed;


@Repository
public interface ActeLaboRepository extends JpaRepository<ActeLabo, Long> {
	

	@Query("SELECT new ActeMed(AM.id,P.nom,P.prenom,P.cin,P.telephone , AM.dateActe,AM.medecin.id, P.id) FROM Adherent as A , Personne as P, ActeMed as AM ,Fse as f where A.id = P.id and A.id = AM.adherent.id and f.acteMed.id = AM.id and AM.Ordonnance_Biologie='true' and f.acteLabo.id is null and P.cin=:cin order by AM.dateActe")
	List<ActeMed> findActeMedLaboByCinAdherent(@Param("cin") String cin);

	@Query("SELECT new ActeLabo(AL.id, AL.codeActe,AL.lettreCle,AL.prix,AL.dateActe,P.nom,P.prenom,P.telephone ,P.cin,f.numeroDossier,AL.ngap) FROM Adherent as A , Personne as P, ActeLabo as AL, Fse as f where AL.adherent.id = A.id  and A.id = P.id and f.acteLabo.id=AL.id and AL.laboratoire.id=:IdLabo Order By AL.dateActe DESC ")
	List<ActeLabo> findActeloboAdherentinfo( @Param("IdLabo")long IdLabo);
	
	@Query("SELECT new ActeLabo(P.nom,P.prenom,P.telephone ) FROM Medecin as M , Personne as P, ActeLabo as AL where AL.medecin.id = M.id and M.id = P.id and AL.id=:IdActLabo")
	 List<ActeLabo> findActeloboMedinfoByIdActLabo( @Param("IdActLabo")long IdActLabo);
}
