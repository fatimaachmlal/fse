package net.javaguides.springboot.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import net.javaguides.springboot.model.AssurenceComplementaire;

public interface assurenceComplementaireRepository extends JpaRepository<AssurenceComplementaire, Long> {

}
