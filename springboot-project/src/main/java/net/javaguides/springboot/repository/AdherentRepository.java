package net.javaguides.springboot.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import net.javaguides.springboot.model.Adherent;
import net.javaguides.springboot.model.Ald;
import net.javaguides.springboot.model.Beneficiaire;


@Repository
public interface AdherentRepository extends JpaRepository<Adherent, Long>{
	
	Optional<Adherent> findByUsername(String username);
	
	Adherent findByCin(String Cin);
	
	Boolean existsByEmail(String email);
	 
	Boolean existsByUsername(String username);
	
	 @Query("SELECT COUNT(AD) FROM Adherent as AD,Admin as A where A.id=:IdAmin")
		long countAdherent(@Param("IdAmin")long IdAmin);
	
	
	
	 
}
