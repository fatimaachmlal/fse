package net.javaguides.springboot.repository;


import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import net.javaguides.springboot.model.Ald;

@Repository
public interface AldRepository extends JpaRepository<Ald, Long>{
	 @Query("SELECT new Ald(A.id,A.codeAld,A.nDossier) FROM Ald as A where A.adherent.id=:id_adherent")	 		
	    List<Ald> findAldByAdherent(@Param("id_adherent") long id_adherent);
	
	  

}
