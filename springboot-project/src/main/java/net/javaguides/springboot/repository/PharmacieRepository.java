package net.javaguides.springboot.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import net.javaguides.springboot.model.Laboratoire;
import net.javaguides.springboot.model.Pharmacie;

@Repository
public interface PharmacieRepository extends JpaRepository<Pharmacie, Long>{

	Optional<Pharmacie> findByUsername(String username);
	 @Query("SELECT COUNT(PH) FROM Pharmacie as PH, Admin as A where A.id=:IdAmin")
		long countPharmacie(@Param("IdAmin")long IdAmin);

}
