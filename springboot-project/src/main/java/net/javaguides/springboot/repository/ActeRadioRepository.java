package net.javaguides.springboot.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import net.javaguides.springboot.model.ActeLabo;
import net.javaguides.springboot.model.ActeMed;
import net.javaguides.springboot.model.ActeRadio;

@Repository
public interface ActeRadioRepository extends JpaRepository<ActeRadio, Long> {
	@Query("SELECT new ActeMed(AM.id,P.nom,P.prenom,P.cin,P.telephone , AM.dateActe,AM.medecin.id,P.id) FROM Adherent as A , Personne as P, ActeMed as AM ,Fse as f where A.id = P.id and A.id = AM.adherent.id and f.acteMed.id = AM.id and AM.Ordonnance_Radiologie='true' and f.acteRadio.id is null and P.cin=:cin order by AM.dateActe")
	List<ActeMed> findActeMedRAdioByCinAdherent(@Param("cin") String cin);
	
	
	
	@Query("SELECT new ActeRadio(AR.id, AR.codeActe,AR.lettreCle,AR.prix,AR.dateActe,P.nom,P.prenom,P.telephone ,P.cin,f.numeroDossier,AR.ngap) FROM Adherent as A , Personne as P, ActeRadio as AR , Fse as f where AR.adherent.id = A.id and A.id = P.id and f.acteRadio.id=AR.id and AR.radiologie.id=:IdRadio Order By AR.dateActe DESC")
	List<ActeRadio> findActeRadioAdherentinfo( @Param("IdRadio")long IdRadio);
	
	@Query("SELECT new ActeRadio(P.nom,P.prenom,P.telephone ) FROM Medecin as M , Personne as P, ActeRadio as AR where AR.medecin.id = M.id and M.id = P.id and AR.id=:IdActRadio")
	 List<ActeRadio> findActeRadioMedinfoByIdActRadio( @Param("IdActRadio")long IdActRadio);
}
