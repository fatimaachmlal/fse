package net.javaguides.springboot;

import javax.annotation.Resource;

import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import net.javaguides.springboot.service.FilesStorageServiceL;

@SpringBootApplication
public class SpringbootProjectApplication implements CommandLineRunner{
	  @Resource
	  FilesStorageServiceL storageService;
	public static void main(String[] args) {
		SpringApplication.run(SpringbootProjectApplication.class, args);
	}
	  @Override
	  public void run(String... arg) throws Exception {
	    storageService.deleteAll();
	    storageService.init();
	  }

}
