package net.javaguides.springboot.payload.response;

import java.util.Date;

import javax.persistence.Column;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import net.javaguides.springboot.model.Sexe;
import net.javaguides.springboot.model.SituationFamiliale;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ProfilResponse { 
	  private String nom;
	  private String inpe;
	  private Long id;
	  private String prenom;
	  private SituationFamiliale situationFamiliale;
	  private Sexe sexe;
	  private String ville;
	  private String adresse;
	  private String telephone;
	  private String email;
	  private String cin;
	  private Date dateNaissance;
	  private String raisonSociale;
	  private String ice;
	  private String codePostal;
	  private String MotdePasse;
	  private String typeMutuelle;
	  private String specialite;
   	  private String immatriculation;

	  
	public String getMotdePasse() {
		return MotdePasse;
	}
	public void setMotdePasse(String motdePasse) {
		MotdePasse = motdePasse;
	}
	public String getNom() {
		return nom;
	}
	public String getInpe() {
		return inpe;
	}
	public Long getId() {
		return id;
	}
	public String getPrenom() {
		return prenom;
	}
	public SituationFamiliale getSituationFamiliale() {
		return situationFamiliale;
	}
	public Sexe getSexe() {
		return sexe;
	}
	public String getVille() {
		return ville;
	}
	public String getAdresse() {
		return adresse;
	}
	public String getTelephone() {
		return telephone;
	}
	public String getEmail() {
		return email;
	}
	public String getCin() {
		return cin;
	}
	public Date getDateNaissance() {
		return dateNaissance;
	}
	public String getRaisonSociale() {
		return raisonSociale;
	}
	public String getIce() {
		return ice;
	}
	public String getTypeMutuelle() {
		return typeMutuelle;
	}
	public String getSpecialite() {
		return specialite;
	}
	public String getImmatriculation() {
		return immatriculation;
	}
	  public String getCodePostal() {
		return codePostal;
	}
	public void setCodePostal(String codePostal) {
		this.codePostal = codePostal;
	}
	 
   	  
	public void setId(Long id) {
		// TODO Auto-generated method stub
		this.id=id;
		
	}
	public void setNom(String nom2) {
		// TODO Auto-generated method stub
		this.nom=nom2;
		
	}
	public void setPrenom(String prenom2) {
		// TODO Auto-generated method stub
		this.prenom=prenom2;
	}
	public void setSituationFamiliale(SituationFamiliale situationFamiliale2) {
		// TODO Auto-generated method stub
		this.situationFamiliale=situationFamiliale2;
		
	}
	public void setSexe(Sexe sexe2) {
		// TODO Auto-generated method stub
		this.sexe=sexe2;
		
	}
	public void setVille(String ville2) {
		// TODO Auto-generated method stub
		this.ville=ville2;
		
	}
	public void setAdresse(String adresse2) {
		// TODO Auto-generated method stub
		this.adresse=adresse2;
		
	}
	public void setTelephone(String telephone2) {
		// TODO Auto-generated method stub
		this.telephone=telephone2;
		
	}
	public void setEmail(String email2) {
		// TODO Auto-generated method stub
		this.email=email2;
		
	}
	public void setCin(String cin2) {
		// TODO Auto-generated method stub
		this.cin=cin2;
		
	}
	public void setDateNaissance(java.sql.Date dateNaissance2) {
		// TODO Auto-generated method stub
		this.dateNaissance=dateNaissance2;
		
	}
	public void setTypeMutuelle(String typeMutuelle2) {
		// TODO Auto-generated method stub
		this.typeMutuelle=typeMutuelle2;
		
	}
	public void setImmatriculation(String immatriculation2) {
		// TODO Auto-generated method stub
		this.immatriculation=immatriculation2;
		
	}
	public void setInpe(String inpe2) {
		// TODO Auto-generated method stub
		this.inpe=inpe2;		
	}
	public void setDateNaissance(Date dateNaissance2) {
		// TODO Auto-generated method stub
		this.dateNaissance=dateNaissance2;
		
	}
	public void setRaisonSociale(String raisonSociale2) {
		// TODO Auto-generated method stub
		this.raisonSociale=raisonSociale2;
		
	}
	public void setIce(String ice2) {
		// TODO Auto-generated method stub
		this.ice=ice2;
		
	}
	public void setSpecialite(String specialite2) {
		// TODO Auto-generated method stub
		this.specialite=specialite2;
		
	}
	

		
	  
	  
}
