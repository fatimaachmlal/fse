package net.javaguides.springboot.payload.response;

import java.util.Date;

import javax.persistence.Transient;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import lombok.Data;
import net.javaguides.springboot.model.ActeMed;
import net.javaguides.springboot.model.Adherent;
import net.javaguides.springboot.model.Medecin;
@Data

public class InfoActByCinAdhenet {
	private long id;
	
	private Date dateActe;

	private String nom;

	private String prenom;

	private String cin;

	private String telephone;

	private long medecin_id;

}
