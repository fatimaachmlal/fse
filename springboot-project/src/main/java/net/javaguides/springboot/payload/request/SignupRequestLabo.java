package net.javaguides.springboot.payload.request;

import java.util.HashSet;
import java.util.Set;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import net.javaguides.springboot.dto.PersonneRequest;
import net.javaguides.springboot.model.ActeMed;
import net.javaguides.springboot.model.Fse;
@Data
@AllArgsConstructor(staticName = "build")
@NoArgsConstructor
@EqualsAndHashCode(callSuper = true)
public class SignupRequestLabo extends PersonneRequest{
	@NotNull(message = "specialite ne doit pas être null")
	private String raisonSociale;
	
    @Pattern(regexp = "^\\d{9}$",message = "L'inpe est invalide ")
	private String inpe;
	
    @Pattern(regexp = "^\\d{15}$",message = "L'ice est invalide ")
	private String ice;
    @NotNull
	private String typeMutuelle;
 
	private Set<ActeMed> actes=new HashSet<>();
	private Set<Fse> fses=new HashSet<>();

	private byte[] photoCarteCin;
	

	private byte[] carteProfessionnelle;
	

	private byte[] cachetSignature;
	

	private byte[] PhotoProfile;
	private String username;

    private Set<String> role;
}
