package net.javaguides.springboot.payload.response;

import java.util.Date;
import java.util.List;

import javax.validation.constraints.NotNull;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import net.javaguides.springboot.dto.ActeMedRequest;
import net.javaguides.springboot.model.Adherent;
import net.javaguides.springboot.model.Medecin;
@Data
@AllArgsConstructor
@NoArgsConstructor
public class AdherentResponse {
	  private String non;
	  private String prenom;
	  private Long id;
	  private String cin;
	  private String ville;
	  private Date dateNaissance;
	  private String telphone;
	  private String immatriculation;
	  private String typeMutuelle;
	  
	public String getTypeMutuelle() {
		return typeMutuelle;
	}
	public void setTypeMutuelle(String typeMutuelle) {
		this.typeMutuelle = typeMutuelle;
	}
	public String getImmatriculation() {
		return immatriculation;
	}
	public void setImmatriculation(String immatriculation) {
		this.immatriculation = immatriculation;
	}
	public String getNon() {
		return non;
	}
	public void setNon(String non) {
		this.non = non;
	}
	public String getPrenom() {
		return prenom;
	}
	public void setPrenom(String prenom) {
		this.prenom = prenom;
	}
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getCin() {
		return cin;
	}
	public void setCin(String cin) {
		this.cin = cin;
	}
	public String getVille() {
		return ville;
	}
	public void setVille(String ville) {
		this.ville = ville;
	}
	public Date getDateNaissance() {
		return dateNaissance;
	}
	public void setDateNaissance(Date dateNaissance) {
		this.dateNaissance = dateNaissance;
	}
	public String getTelphone() {
		return telphone;
	}
	public void setTelphone(String telphone) {
		this.telphone = telphone;
	}
	

}
