package net.javaguides.springboot.service.Impl;

import java.time.LocalDateTime;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import net.javaguides.springboot.dto.ActeMedRequest;
import net.javaguides.springboot.dto.FseRequest;
import net.javaguides.springboot.exception.ResourceNotFoundException;
import net.javaguides.springboot.model.ActeMed;
import net.javaguides.springboot.model.Medecin;
import net.javaguides.springboot.repository.ActeMedRepository;
import net.javaguides.springboot.service.ActeMedService;

@Service
public class ActeMedServiceImpl implements ActeMedService {
	@Autowired
	private ActeMedRepository acteMedRepository;
	@Autowired
	private FseServiceImpl fseServiceImpl;
	//@Autowired
	//private MedecinServiceImpl medecinServiceImpl;
	

	public ActeMedServiceImpl(ActeMedRepository acteMedRepository) {
		super();
		this.acteMedRepository = acteMedRepository;
	}


	@Override
	public ActeMed saveActeMed(ActeMedRequest acteMedRequest) {
		ActeMed acteMed=new ActeMed();
		acteMed.setPrix(acteMedRequest.getPrix());
		acteMed.setMedecin(acteMedRequest.getMedecin());
		acteMed.setAdherent(acteMedRequest.getAdherent());	
		acteMed.setBeneficiaire(acteMedRequest.getBeneficiaire());	
		acteMed.setLettreCle(acteMedRequest.getLettreCle());
		acteMed.setDateActe(acteMedRequest.getDateActe());
		acteMed.setCodeActe(acteMedRequest.getCodeActe());
		acteMed.setOrdonnance_Biologie(acteMedRequest.getOrdonnance_Biologie());
		acteMed.setOrdonnance_Radiologie(acteMedRequest.getOrdonnance_Radiologie());
		acteMed.setOrdonnance_Pharmacie(acteMedRequest.getOrdonnance_Pharmacie());
		acteMed.setType_de_soins(acteMedRequest.getType_de_soins());
		acteMed.setAld(acteMedRequest.getAld());

		ActeMed NewacteMed=new ActeMed();
		NewacteMed = acteMedRepository.save(acteMed);
		
		FseRequest fse =new FseRequest();
		fse.setAdherent(acteMedRequest.getAdherent());
		fse.setDateCreation(acteMedRequest.getDateActe());
		
		//LocalDateTime date=LocalDateTime.of(2022, 05, 01, 10, 8, 2);

		//fse.setDate(date);
		
		fse.setAdherent(acteMedRequest.getAdherent());	

		fse.setActeMed(NewacteMed);

		fseServiceImpl.saveFse(fse);
		 
		return acteMed ;
	}


	@Override
	public ActeMed updateActeMed(ActeMed acteMed, long id) {
		ActeMed existingActeMed= acteMedRepository.findById(id).orElseThrow(
				() -> new ResourceNotFoundException("ActeMed", "Id", id));
		existingActeMed.setCodeActe(acteMed.getCodeActe());
		existingActeMed.setLettreCle(acteMed.getLettreCle());
		existingActeMed.setPrix(acteMed.getPrix());
		existingActeMed.setDateActe(acteMed.getDateActe());
		existingActeMed.setMedecin(acteMed.getMedecin());
		
		acteMedRepository.save(existingActeMed);
		return existingActeMed;
	}


	@Override
	public List<ActeMed> getAllActesMed() {
		
		return acteMedRepository.findAll();
	}



	@Override
	public ActeMed getActeMedById(long id) {
		return acteMedRepository.findById(id).orElseThrow(() -> 
		new ResourceNotFoundException("ActeMed", "Id", id));
	}


	@Override
	public void deleteActeMed(long id) {
		acteMedRepository.findById(id).orElseThrow(() -> 
		new ResourceNotFoundException("ActeMed", "Id", id));
		acteMedRepository.deleteById(id);
		
	}
/*
 * 
 * public List<ActeMed> findAllByMedecinId(long id) {
		Medecin medcin=new Medecin();
		
		medcin=medecinServiceImpl.getMedecinById(id);
		System.out.println(medcin);
	//	acteMedRepository.findAllByMedecin(medcin);
		return null;
	}
 * 
 * */	

	@Override
	public List<ActeMed> findActeMedByCinAdherent(String cin) {
		return acteMedRepository.findActeMedByCinAdherent(cin)	;
		
	}


	@Override
	public List<ActeMed> findActeMedByMedecin(long id) {
		
		return acteMedRepository.findActeMedByMedecin(id);
	}


	@Override
	public List<ActeMed> findActeMedecinAdherentinfo(long IdMedecin) {
		return acteMedRepository.findActeMedecinAdherentinfo(IdMedecin);
	}


	@Override
	public List<ActeMed> findActeMedecinMedinfoByIdActMed(long IdActMed) {
		return acteMedRepository.findActeMedecinMedinfoByIdActMed(IdActMed);
	}








}
