package net.javaguides.springboot.service.Impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import net.javaguides.springboot.dto.ActePharRequest;
import net.javaguides.springboot.exception.ResourceNotFoundException;
import net.javaguides.springboot.model.ActeMed;
import net.javaguides.springboot.model.ActePhar;
import net.javaguides.springboot.repository.ActePharRepository;
import net.javaguides.springboot.service.ActePharService;


@Service
public class ActePharServiceImpl implements ActePharService{
	
	
	@Autowired
	private ActePharRepository actePharRepository;
	
	
	public ActePharServiceImpl(ActePharRepository actePharRepository) {
		super();
		this.actePharRepository = actePharRepository;
	}
	
	

	@Override
	public ActePhar saveActePhar(ActePharRequest actePharRequest) {
		ActePhar actePhar = new ActePhar();
		actePhar.setDateActe(actePharRequest.getDateActe());
		actePhar.setPrix(actePharRequest.getPrix());
		actePhar.setPharmacie(actePharRequest.getPharmacie());
		actePhar.setAdherent(actePharRequest.getAdherent());
		actePhar.setMedecin(actePharRequest.getMedecin());
		
		return actePharRepository.save(actePhar);
	}

	@Override
	public ActePhar modifierActePhar(ActePhar actePhar, Long id) {
		ActePhar existingActePhar = actePharRepository.findById(id).orElseThrow(
				()->new ResourceNotFoundException("ActePhar", "Id", id)); 
		existingActePhar.setPrix(actePhar.getPrix());
		existingActePhar.setDateActe(actePhar.getDateActe());
		existingActePhar.setPharmacie(actePhar.getPharmacie());
		
		return null;
	}

	@Override
	public List<ActePhar> visualiserActesPhar() {
		
		return actePharRepository.findAll();
	}

	@Override
	public ActePhar rechercherActePharById(Long id) {
		return actePharRepository.findById(id).orElseThrow(
				()-> new ResourceNotFoundException("ActePhar", "Id", id));
		
	}

	@Override
	public void supprimerActePhar(Long id) {
		actePharRepository.findById(id).orElseThrow(
				()-> new ResourceNotFoundException("ActePhar", "Id", id));
		actePharRepository.deleteById(id);
		
		
	}



	@Override
	public List<ActeMed> findActeMedPharmaByCinAdherent(String cin) {
		return actePharRepository.findActeMedPharmaByCinAdherent(cin);
	}



	@Override
	public List<ActePhar> findActePharmaAdherentinfo(long IdPharma) {
		return actePharRepository.findActePharmaAdherentinfo(IdPharma);
	}



	@Override
	public List<ActePhar> findActePharmaMedinfoByIdActPharma(long IdActPharma) {
		return actePharRepository.findActePharmaMedinfoByIdActPharma(IdActPharma);
	}

}
