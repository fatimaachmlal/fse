package net.javaguides.springboot.service.Impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import net.javaguides.springboot.dto.ActeRadioRequest;
import net.javaguides.springboot.exception.ResourceNotFoundException;
import net.javaguides.springboot.model.ActeMed;
import net.javaguides.springboot.model.ActeRadio;
import net.javaguides.springboot.repository.ActeRadioRepository;

import net.javaguides.springboot.service.ActeRadioService;


@Service
public class ActeRadiologieServiceImpl implements ActeRadioService{
	
	@Autowired
	private ActeRadioRepository acteRadioRepository;
	

	public ActeRadiologieServiceImpl(ActeRadioRepository acteRadioRepository) {
		super();
		this.acteRadioRepository = acteRadioRepository;
	}
	
	

	@Override
	public ActeRadio creerActeRadion(ActeRadioRequest acteRadioRequest) {
		ActeRadio acteRadio = new ActeRadio();
		acteRadio.setCodeActe(acteRadioRequest.getCodeActe());
		acteRadio.setNgap(acteRadioRequest.getNgap());
		acteRadio.setLettreCle(acteRadioRequest.getLettreCle());
		acteRadio.setPrix(acteRadioRequest.getPrix());
		acteRadio.setDateActe(acteRadioRequest.getDateActe());
		acteRadio.setRadiologie(acteRadioRequest.getRadiologie());
		acteRadio.setAdherent(acteRadioRequest.getAdherent());
		acteRadio.setMedecin(acteRadioRequest.getMedecin());
		System.out.println(acteRadio);
		return acteRadioRepository.save(acteRadio);
	}

	@Override
	public ActeRadio modificationActeRadio(ActeRadio acteRadio, Long id) {
		ActeRadio existingActeRadio = acteRadioRepository.findById(id).orElseThrow(
				()->new ResourceNotFoundException("ActeRadio", "Id", id));
		existingActeRadio.setCodeActe(acteRadio.getCodeActe());
		existingActeRadio.setDateActe(acteRadio.getDateActe());
		existingActeRadio.setLettreCle(acteRadio.getLettreCle());
		existingActeRadio.setPrix(acteRadio.getPrix());
		existingActeRadio.setRadiologie(acteRadio.getRadiologie());
		acteRadioRepository.save(existingActeRadio);
		
		return existingActeRadio;
	}

	@Override
	public List<ActeRadio> visualiserActeRadio() {
		
		return acteRadioRepository.findAll();
	}

	@Override
	public ActeRadio rechercherActeRadiById(Long id) {
		
		return acteRadioRepository.findById(id).orElseThrow(
				()->new ResourceNotFoundException("ActeRadio", "Id", id));
	}

	@Override
	public void supprimerActeRadio(Long id) {
		acteRadioRepository.findById(id).orElseThrow(
				()->new ResourceNotFoundException("ActeRadio", "Id", id));
		acteRadioRepository.deleteById(id);
		
	}



	@Override
	public List<ActeMed> findActeMedRAdioByCinAdherent(String cin) {
		return acteRadioRepository.findActeMedRAdioByCinAdherent(cin);
	}



	@Override
	public List<ActeRadio> findActeRadioAdherentinfo(long IdRadio) {
		return acteRadioRepository.findActeRadioAdherentinfo(IdRadio);
	}



	@Override
	public List<ActeRadio> findActeRadioMedinfoByIdActRadio(long IdActRadio) {
		return acteRadioRepository.findActeRadioMedinfoByIdActRadio(IdActRadio);
	}

	
	

}
