
package net.javaguides.springboot.service.Impl;

import java.util.List;
//import java.util.Optional;

import org.springframework.stereotype.Service;

import net.javaguides.springboot.dto.BeneficiaireRequest;
import net.javaguides.springboot.exception.ResourceNotFoundException;
import net.javaguides.springboot.model.Adherent;
import net.javaguides.springboot.model.Beneficiaire;
import net.javaguides.springboot.model.Fse;
import net.javaguides.springboot.repository.BeneficiaireRepository;
import net.javaguides.springboot.service.BeneficiaireService;

@Service
public class BeneficiaireServiceImpl implements BeneficiaireService{

	private BeneficiaireRepository beneficiaireRepository;
	
	

	public BeneficiaireServiceImpl(BeneficiaireRepository beneficiaireRepository) {
		super();
		this.beneficiaireRepository = beneficiaireRepository;
	}

	@Override
	public Beneficiaire saveBeneficiaire(BeneficiaireRequest beneficiaireRequest) {
		
		
		Beneficiaire beneficiaire= new Beneficiaire();		
		beneficiaire.setPrenom(beneficiaireRequest.getPrenom());		
		beneficiaire.setNom(beneficiaireRequest.getNom());
		beneficiaire.setDateNaissance(beneficiaireRequest.getDateNaissance());		
        beneficiaire.setLieuNaissance(beneficiaireRequest.getLieuNaissance());		
		beneficiaire.setNDossier(beneficiaireRequest.getNDossier());
		beneficiaire.setCodeAld(beneficiaireRequest.getCodeAld());	
		Adherent adherent = new Adherent();
		adherent.setId(beneficiaireRequest.getAdhrentId());		
		//beneficiaire.setAlds(beneficiaireRequest.getAlds());
		beneficiaire.setAdherent(adherent);
		beneficiaire.setCin(beneficiaireRequest.getCin());		
		beneficiaire.setSexe(beneficiaireRequest.getSexe());
		beneficiaire.setRelationFamiliale(beneficiaireRequest.getRelationFamiliale());
		return beneficiaireRepository.save(beneficiaire);
	}

	@Override
	public List<Beneficiaire> getAllBeneficiaires() {
		return beneficiaireRepository.findAll();
	}

	@Override
	public Beneficiaire getBeneficiaireById(long id) {

//		}
		return beneficiaireRepository.findById(id).orElseThrow(() ->
						new ResourceNotFoundException("Beneficiaire", "Id", id));

	}

	@Override
	public Beneficiaire updateBeneficiaire(Beneficiaire beneficiaire, long id) {


		Beneficiaire existingBeneficiaire = beneficiaireRepository.findById(id).orElseThrow(
				() -> new ResourceNotFoundException("Beneficiaire", "Id", id));


		existingBeneficiaire.setNom(beneficiaire.getNom());
		existingBeneficiaire.setPrenom(beneficiaire.getPrenom());

		existingBeneficiaire.setDateNaissance(beneficiaire.getDateNaissance());






		beneficiaireRepository.save(existingBeneficiaire);
		return existingBeneficiaire;
	}

	@Override
	public void deleteBeneficiaire(long id) {

		// check whether a beneficiaire exist in a DB or not
		beneficiaireRepository.findById(id).orElseThrow(() ->
								new ResourceNotFoundException("Beneficiaire", "Id", id));
		beneficiaireRepository.deleteById(id);
	}
	@Override
	public List<Beneficiaire> findBeneficiaireByAdherent(long id_adherent) {
		return beneficiaireRepository.findBeneficiaireByAdherent(id_adherent);
	}
	@Override
	public List<Beneficiaire> findBeneficiaireByAdherentCIN(String cin) {
		return beneficiaireRepository.findBeneficiaireByAdherentCin(cin);
	}
	
	
	

	
}


