package net.javaguides.springboot.service;

import java.util.List;
import java.util.Optional;

import net.javaguides.springboot.dto.RadiologieRequest;
import net.javaguides.springboot.model.Radiologie;

public interface RadiologieService {
	
	Radiologie savesaveRadiologie(RadiologieRequest radiologierequest);
	List<Radiologie> getAllRadiologie();
	Radiologie rechercherRadiologieId(Long id);
	Radiologie modifierRadiologie(Radiologie radiologie, Long id);
	void supprimerRadiologie(Long id);
	long countRadiologie(long IdAmin);
	

}
