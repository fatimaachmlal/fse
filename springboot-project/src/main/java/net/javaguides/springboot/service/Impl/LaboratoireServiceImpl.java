package net.javaguides.springboot.service.Impl;

import java.io.IOException;
import java.util.Base64;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import net.javaguides.springboot.dto.LaboratoireRequest;
import net.javaguides.springboot.exception.ResourceNotFoundException;
import net.javaguides.springboot.model.Laboratoire;
import net.javaguides.springboot.repository.LaboratoireRepository;
import net.javaguides.springboot.service.LaboratoireService;

@Service
public class LaboratoireServiceImpl implements LaboratoireService {
	
	@Autowired
	private LaboratoireRepository laboratoireRepository;
	
	public LaboratoireServiceImpl(LaboratoireRepository laboratoireRepository) {
		super();
		this.laboratoireRepository = laboratoireRepository;
	}

	@Override
	public Laboratoire saveLaboratoire(LaboratoireRequest laboratoireRequest) {
		Laboratoire laboratoire=new Laboratoire();
		laboratoire.setVille(laboratoireRequest.getVille());
		laboratoire.setTelephone(laboratoireRequest.getTelephone());
		laboratoire.setRaisonSociale(laboratoireRequest.getRaisonSociale());
		laboratoire.setSituationFamiliale(laboratoireRequest.getSituationFamiliale());
		laboratoire.setSexe(laboratoireRequest.getSexe());
		laboratoire.setPrenom(laboratoireRequest.getPrenom());
		//laboratoire.setPhotoProfile(laboratoireRequest.getPhotoProfile());
		laboratoire.setNom(laboratoireRequest.getNom());
		laboratoire.setMotDePasse(laboratoireRequest.getMotDePasse());
		laboratoire.setInpe(laboratoireRequest.getInpe());
		laboratoire.setIce(laboratoireRequest.getIce());
		//laboratoire.setFses(laboratoireRequest.getFses());
		//laboratoire.setFichierCin(laboratoireRequest.getFichierCin());
		laboratoire.setEmail(laboratoireRequest.getEmail());
		laboratoire.setDateNaissance(laboratoireRequest.getDateNaissance());
		laboratoire.setCodePostal(laboratoireRequest.getCodePostal());
		laboratoire.setCin(laboratoireRequest.getCin());
		//laboratoire.setCartePro(laboratoireRequest.getCartePro());
		laboratoire.setAdresse(laboratoireRequest.getAdresse());
		laboratoire.setActes(laboratoireRequest.getActes());
		String fileName = StringUtils.cleanPath(laboratoireRequest.getFile().getOriginalFilename());
		/*if(fileName.contains(".."))
		{
			System.out.println("not a a valid file");
		}
		try {
			//laboratoire.setCachet(Base64.getEncoder().encodeToString(laboratoireRequest.getFile().getBytes()));
		} catch (IOException e) {
			e.printStackTrace();
		}*/
		return laboratoireRepository.save(laboratoire);
	}

	@Override
	public List<Laboratoire> getAllLaboratoires() {
		return laboratoireRepository.findAll();
	}

	@Override
	public Laboratoire getLaboratoireById(long id) {
		return laboratoireRepository.findById(id).orElseThrow(() -> 
		new ResourceNotFoundException("Laboratoire", "Id", id));
	}

	@Override
	public Laboratoire updateLaboratoire(Laboratoire laboratoire, long id) {
		Laboratoire existingLabo=laboratoireRepository.findById(id).orElseThrow(() -> 
		new ResourceNotFoundException("Laboratoire", "Id", id));
		existingLabo.setPrenom(laboratoire.getPrenom());
		existingLabo.setNom(laboratoire.getNom());
		existingLabo.setEmail(laboratoire.getEmail());
		existingLabo.setActes(laboratoire.getActes());
		existingLabo.setAdresse(laboratoire.getAdresse());
		//existingLabo.setCachet(laboratoire.getCachet());
		//existingLabo.setCartePro(laboratoire.getCartePro());
		existingLabo.setCin(laboratoire.getCin());
		existingLabo.setCodePostal(laboratoire.getCodePostal());
		existingLabo.setDateNaissance(laboratoire.getDateNaissance());
		//existingLabo.setFichierCin(laboratoire.getFichierCin());
		//existingLabo.setFses(laboratoire.getFses());
		existingLabo.setIce(laboratoire.getIce());
		existingLabo.setInpe(laboratoire.getInpe());
		existingLabo.setMotDePasse(laboratoire.getMotDePasse());
		existingLabo.setSexe(laboratoire.getSexe());
		//existingLabo.setPhotoProfile(laboratoire.getPhotoProfile());
		existingLabo.setSituationFamiliale(laboratoire.getSituationFamiliale());
		existingLabo.setTelephone(laboratoire.getTelephone());
		existingLabo.setVille(laboratoire.getVille());
		
		laboratoireRepository.save(existingLabo);
		return existingLabo;
	}

	@Override
	public void deleteLaboratoire(long id) {
		laboratoireRepository.findById(id).orElseThrow(() -> 
		new ResourceNotFoundException("Laboratoire", "Id", id));
		laboratoireRepository.deleteById(id);
		
	}
	@Override
	public long countLaboratoire() {
		return laboratoireRepository.countLaboratoire();
	}

}
