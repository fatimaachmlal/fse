package net.javaguides.springboot.service;

import java.util.Date;
import java.util.List;

import org.springframework.data.repository.query.Param;

import net.javaguides.springboot.dto.ActeMedRequest;
import net.javaguides.springboot.model.ActeMed;

public interface ActeMedService {
	ActeMed saveActeMed(ActeMedRequest acteMedRequest);
	ActeMed updateActeMed(ActeMed acteMed, long id);
    List<ActeMed> getAllActesMed();
    ActeMed getActeMedById(long id);
    void deleteActeMed(long id);
	List<ActeMed> findActeMedByCinAdherent( String cin);
	List<ActeMed> findActeMedByMedecin( long id);
	
	List<ActeMed> findActeMedecinAdherentinfo(long IdMedecin);
	
	
	 List<ActeMed> findActeMedecinMedinfoByIdActMed(long IdActMed);




}
