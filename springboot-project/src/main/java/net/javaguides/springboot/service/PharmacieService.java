package net.javaguides.springboot.service;

import java.util.List;

import net.javaguides.springboot.dto.PharmacieRequest;
import net.javaguides.springboot.model.Pharmacie;

public interface PharmacieService {
	
	Pharmacie savePharmacie(PharmacieRequest pharmacieresquest);
	List<Pharmacie> getAllPharmacie();
	Pharmacie rechercherPharmacieId(Long id);
	Pharmacie modifierPharmacie(Pharmacie pharmacie, Long id);
	void supprimerPharmacie(Long id);
	 long countPharmacie(long IdAmin);
}
