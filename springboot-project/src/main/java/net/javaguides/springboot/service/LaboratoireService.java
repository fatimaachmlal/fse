package net.javaguides.springboot.service;

import java.util.List;

import net.javaguides.springboot.dto.LaboratoireRequest;
import net.javaguides.springboot.model.Laboratoire;


public interface LaboratoireService {
	Laboratoire saveLaboratoire(LaboratoireRequest laboratoireRequest);
    List<Laboratoire> getAllLaboratoires();
    Laboratoire getLaboratoireById(long id);
    Laboratoire updateLaboratoire(Laboratoire laboratoire, long id);
	void deleteLaboratoire(long id);
	long countLaboratoire();
}
