package net.javaguides.springboot.service.Impl;



import java.util.ArrayList;
import java.util.List;
//import java.util.Optional;
import java.util.Set;

import org.springframework.stereotype.Service;

import net.javaguides.springboot.dto.AdherentRequest;
import net.javaguides.springboot.exception.ResourceNotFoundException;
import net.javaguides.springboot.model.Adherent;
import net.javaguides.springboot.model.Ald;
import net.javaguides.springboot.model.Beneficiaire;
import net.javaguides.springboot.model.Personne;
import net.javaguides.springboot.repository.AdherentRepository;
import net.javaguides.springboot.service.AdherentService;

@Service
public class AdherentServiceImpl implements AdherentService{

	private AdherentRepository adherentRepository;
	
	public AdherentServiceImpl(AdherentRepository adherentRepository) {
		super();
		this.adherentRepository = adherentRepository;
	}

	@Override
	public Adherent saveAdherent(AdherentRequest adherentRequest) {
		
        Adherent adherent= new Adherent();
		
        adherent.setTypeMutuelle(adherentRequest.getTypeMutuelle());
        adherent.setImmatriculation(adherentRequest.getImmatriculation());
		
		adherent.setVille(adherentRequest.getVille());
		
		adherent.setSituationFamiliale(adherentRequest.getSituationFamiliale());
		
		adherent.setSexe(adherentRequest.getSexe());
		adherent.setPrenom(adherentRequest.getPrenom());
		adherent.setNom(adherentRequest.getNom());
		adherent.setMotDePasse(adherentRequest.getMotDePasse());
		
		adherent.setEmail(adherentRequest.getEmail());
		adherent.setDateNaissance(adherentRequest.getDateNaissance());
		adherent.setCodePostal(adherentRequest.getCodePostal());
		adherent.setCin(adherentRequest.getCin());
	
		adherent.setAdresse(adherentRequest.getAdresse());
		
		return adherentRepository.save(adherent);
	}

	@Override
	public List<Adherent> getAllAdherents() {
		
		
		return adherentRepository.findAll();
	}

	@Override
	public Adherent getAdherentById(long id) {

//		}
		return adherentRepository.findById(id).orElseThrow(() -> 
						new ResourceNotFoundException("Adherent", "Id", id));
		
	}

	@Override
	public Adherent updateAdherent(Adherent adherent, long id) {
		
		
		Adherent existingAdherent = adherentRepository.findById(id).orElseThrow(
				() -> new ResourceNotFoundException("Adherent", "Id", id)); 
		
		existingAdherent.setTypeMutuelle(adherent.getTypeMutuelle());
		existingAdherent.setTypeMutuelle(adherent.getTypeMutuelle());
		existingAdherent.setImmatriculation(adherent.getImmatriculation());
		
	
		
		//existingAdherent.setTypeMutuelle(adherent.getTypeMutuelle());
		
		
		adherentRepository.save(existingAdherent);
		return existingAdherent;
	}

	@Override
	public void deleteAdherent(long id) {
		
		// check whether a adherent exist in a DB or not
		adherentRepository.findById(id).orElseThrow(() -> 
								new ResourceNotFoundException("Adherent", "Id", id));
		adherentRepository.deleteById(id);
	}

//	@Override
	//public Adherent saveAdherent(AdherentRequest adherentRequest) {
		// TODO Auto-generated method stub
		//return null;
	//}
	
	@Override
	public Adherent getAdherentByCin(String cin) {
		return adherentRepository.findByCin	(cin);
	}
	
	public List<Beneficiaire> getBeneficiaires(long adhrentId){
		/*
		Adherent adherent = adherentRepository.getById(adhrentId);
		Set<Beneficiaire> beneficiaireSet = adherent.getBeneficiaires();
		List<Beneficiaire> beneficiaireList = new ArrayList<>(beneficiaireSet);
		return beneficiaireList;
		*/
		return null;
	}
	
	@Override
	public long countAdherent(long IdAmin) {
		return adherentRepository.countAdherent(IdAmin);
	}
	
	

	
	
}

