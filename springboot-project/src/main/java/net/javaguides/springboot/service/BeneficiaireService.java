package net.javaguides.springboot.service;




import java.util.List;



import net.javaguides.springboot.dto.BeneficiaireRequest;
import net.javaguides.springboot.model.Beneficiaire;
import net.javaguides.springboot.model.Fse;

public interface BeneficiaireService  {
	Beneficiaire saveBeneficiaire( BeneficiaireRequest beneficiaireRequest);
	List<Beneficiaire> getAllBeneficiaires();
	Beneficiaire getBeneficiaireById(long id);
	Beneficiaire updateBeneficiaire(Beneficiaire beneficiaire, long id);
	void deleteBeneficiaire(long id);
	List<Beneficiaire> findBeneficiaireByAdherent(long id_adherent);
	List<Beneficiaire> findBeneficiaireByAdherentCIN(String cin);


}
