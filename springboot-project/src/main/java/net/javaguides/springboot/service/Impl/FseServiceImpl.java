package net.javaguides.springboot.service.Impl;

import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.Random;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import net.javaguides.springboot.dto.FseRequest;
import net.javaguides.springboot.exception.ResourceNotFoundException;
import net.javaguides.springboot.model.ActeMed;
import net.javaguides.springboot.model.ActeRadio;
import net.javaguides.springboot.model.Fse;
import net.javaguides.springboot.model.Medecin;
import net.javaguides.springboot.repository.ActePharRepository;
import net.javaguides.springboot.repository.ActeRadioRepository;
import net.javaguides.springboot.repository.FseRepository;
import net.javaguides.springboot.repository.RadiologieRepository;
import net.javaguides.springboot.service.FseService;

@Service
public class FseServiceImpl implements FseService {
	@Autowired
	private FseRepository fseRepository;
	@Autowired
	private ActeRadioRepository acteRadioRepository;

	@Override
	public void saveFse(FseRequest fseRequest) {
	
		Fse fse =new Fse();
		Random rn = new Random();		
		fse.setNumeroDossier( rn.nextInt(1000000000));
		fse.setDateCreation(fseRequest.getDateCreation());
		fse.setActeMed(fseRequest.getActeMed());
		fse.setAdherent(fseRequest.getAdherent());
		//fse.setDate(fseRequest.getDate());
		//fse.setMedecin(fseRequest.getMedecin());
		fseRepository.save(fse);
	}

	@Override
	public List<Fse> findFseByMedAdherentActe( long id_med,long id_act_med, Date DateActe) {
		
		System.out.println(  DateActe);
		System.out.println( fseRepository.findFseByMedAdherentActe(id_act_med, id_med, DateActe));
		return fseRepository.findFseByMedAdherentActe(id_act_med, id_med, DateActe);
	}

	@Override
	public int AjouterActeLaboFSE(long IdActeLabo, long fseId) {
		
		return fseRepository.AddActeLaboToFSE(IdActeLabo, fseId);
	}

	@Override
	public int AjouterActeRadioToFSE(long IdActeRadio, long fseId) {
		return fseRepository.AddActeRadioToFSE(IdActeRadio, fseId);
	}

	@Override
	public int AjouterActePharToFSE(long IdActePharm, long fseId) {
		return fseRepository.AddActePharToFSE(IdActePharm, fseId);
	}

	@Override
	public Fse findFseWithAdhrentInfo(long id_adherent) {
		return fseRepository.findFseWithAdhrentInfo(id_adherent);
	}

	@Override
	public Fse findFseWithMedecintInfo(long id_adherent, long Id_fse) {
		return fseRepository.findFseWithMedecintInfo(id_adherent, Id_fse);
	}

	@Override
	public Fse findFseWithRadiotInfo(long id_adherent, long Id_fse) {
		return fseRepository.findFseWithRadiotInfo(id_adherent, Id_fse);
	}

	@Override
	public Fse findFseWithLaboInfo(long id_adherent, long Id_fse) {
		return fseRepository.findFseWithLaboInfo(id_adherent, Id_fse);
	}

	@Override
	public Fse findFseWithPharmaInfo(long id_adherent, long Id_fse) {
		return fseRepository.findFseWithPharmaInfo(id_adherent, Id_fse);
	}

	@Override
	public List<Fse> findFseByAdherent(long id_adherent) {
		return fseRepository.findFseByAdherent(id_adherent);
	}





}
