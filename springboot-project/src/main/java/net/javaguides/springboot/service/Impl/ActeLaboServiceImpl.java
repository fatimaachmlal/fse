package net.javaguides.springboot.service.Impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import net.javaguides.springboot.dto.ActeLaboRequest;
import net.javaguides.springboot.exception.ResourceNotFoundException;
import net.javaguides.springboot.model.ActeLabo;
import net.javaguides.springboot.model.ActeMed;
import net.javaguides.springboot.repository.ActeLaboRepository;
import net.javaguides.springboot.service.ActeLaboService;

@Service
public class ActeLaboServiceImpl implements ActeLaboService {

	@Autowired
	private ActeLaboRepository acteLaboRepository;
	
	
	public ActeLaboServiceImpl(ActeLaboRepository acteLaboRepository) {
		super();
		this.acteLaboRepository = acteLaboRepository;
	}

	@Override
	public ActeLabo saveActeLabo(ActeLaboRequest acteLaboRequest) {
		ActeLabo acteLabo=new ActeLabo();
		acteLabo.setPrix(acteLaboRequest.getPrix());
		acteLabo.setLaboratoire(acteLaboRequest.getLaboratoire());
		acteLabo.setLettreCle(acteLaboRequest.getLettreCle());
		acteLabo.setDateActe(acteLaboRequest.getDateActe());
		acteLabo.setCodeActe(acteLaboRequest.getCodeActe());	
		acteLabo.setAdherent(acteLaboRequest.getAdherent());
		acteLabo.setMedecin(acteLaboRequest.getMedecin());
		acteLabo.setNgap(acteLaboRequest.getNgap());
		
		
		return acteLaboRepository.save(acteLabo);
	}

	@Override
	public ActeLabo updateActeLabo(ActeLabo acteLabo, long id) {
		ActeLabo existingActeLabo= acteLaboRepository.findById(id).orElseThrow(
				() -> new ResourceNotFoundException("ActeLabo", "Id", id));
		existingActeLabo.setCodeActe(acteLabo.getCodeActe());
		existingActeLabo.setLettreCle(acteLabo.getLettreCle());
		existingActeLabo.setPrix(acteLabo.getPrix());
		existingActeLabo.setDateActe(acteLabo.getDateActe());
		existingActeLabo.setLaboratoire(acteLabo.getLaboratoire());
		
		acteLaboRepository.save(existingActeLabo);
		return existingActeLabo;
	}

	@Override
	public List<ActeLabo> getAllActesLabo() {
		return acteLaboRepository.findAll();
	}

	@Override
	public ActeLabo getActeLaboById(long id) {
		return acteLaboRepository.findById(id).orElseThrow(() -> 
		new ResourceNotFoundException("ActeLabo", "Id", id));
	}

	@Override
	public void deleteActeLabo(long id) {
		acteLaboRepository.findById(id).orElseThrow(() -> 
		new ResourceNotFoundException("ActeLabo", "Id", id));
		acteLaboRepository.deleteById(id);

	}

	@Override
	public List<ActeMed> findActeMedLaboByCinAdherent(String cin) {
		return acteLaboRepository.findActeMedLaboByCinAdherent(cin);
	}

	@Override
	public List<ActeLabo> findActeloboAdherentinfo(long IdLabo) {
		return acteLaboRepository.findActeloboAdherentinfo( IdLabo);
	}

	@Override
	public  List<ActeLabo> findActeloboMedinfoByIdActLabo(long IdActLabo) {
		
		return acteLaboRepository.findActeloboMedinfoByIdActLabo(IdActLabo);
	}
	

}
