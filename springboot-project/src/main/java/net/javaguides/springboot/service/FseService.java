package net.javaguides.springboot.service;

import java.time.LocalDateTime;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import net.javaguides.springboot.dto.FseRequest;
import net.javaguides.springboot.model.Fse;

public interface FseService {
	void saveFse(FseRequest fseRequest);
	List<Fse>  findFseByMedAdherentActe( long id_act_med,long id_med,Date DateActe);
	
   
    int AjouterActeLaboFSE (long IdActeLabo,  long fseId);
    

    int AjouterActeRadioToFSE( long IdActeRadio,  long fseId);
    

    int AjouterActePharToFSE( long IdActePharm,  long fseId);


    Fse findFseWithAdhrentInfo( long id_adherent);
	
	Fse findFseWithMedecintInfo(long id_adherent,long Id_fse);
	
	Fse findFseWithRadiotInfo(long id_adherent,long Id_fse);
	
	Fse findFseWithLaboInfo(long id_adherent,long Id_fse);
	
	Fse findFseWithPharmaInfo(long id_adherent,long Id_fse);

	List<Fse> findFseByAdherent(long id_adherent);










}



