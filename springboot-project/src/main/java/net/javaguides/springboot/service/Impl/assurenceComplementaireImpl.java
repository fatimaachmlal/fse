package net.javaguides.springboot.service.Impl;

import org.springframework.stereotype.Service;

import net.javaguides.springboot.dto.AssurenceComplementaireRequest;
import net.javaguides.springboot.model.Adherent;
import net.javaguides.springboot.model.AssurenceComplementaire;
import net.javaguides.springboot.repository.assurenceComplementaireRepository;
import net.javaguides.springboot.service.assurenceComplementaireService;

@Service
public class assurenceComplementaireImpl implements assurenceComplementaireService  {
	
	private assurenceComplementaireRepository AssurenceComplementaireRepository;
	public assurenceComplementaireImpl(assurenceComplementaireRepository AssurenceComplementaireRepository) {
		super();
		this.AssurenceComplementaireRepository=AssurenceComplementaireRepository;		
	}
	
	@Override
	public AssurenceComplementaire SaveAssurenceComplementaire(AssurenceComplementaireRequest assurenceComplementaireRequest) {
				
		AssurenceComplementaire assurenceComplementaire= new AssurenceComplementaire();		
		assurenceComplementaire.setNomAssurance(assurenceComplementaireRequest.getNomAssurance());		
		assurenceComplementaire.setNumPolice(assurenceComplementaireRequest.getNumPolice());	
		Adherent adherent = new Adherent();
		adherent.setId(assurenceComplementaireRequest.getAdherentId());		
		assurenceComplementaire.setAdherent(adherent);
		return AssurenceComplementaireRepository.save(assurenceComplementaire);
	}

}
