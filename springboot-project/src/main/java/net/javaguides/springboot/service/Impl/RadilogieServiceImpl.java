package net.javaguides.springboot.service.Impl;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import net.javaguides.springboot.dto.RadiologieRequest;
import net.javaguides.springboot.exception.ResourceNotFoundException;
import net.javaguides.springboot.model.Radiologie;
import net.javaguides.springboot.repository.RadiologieRepository;
import net.javaguides.springboot.service.RadiologieService;

@Service
public class RadilogieServiceImpl implements RadiologieService{
	
	@Autowired
	private RadiologieRepository radiologieRepository;
	

	public RadilogieServiceImpl(RadiologieRepository radiologieRepository) {
		super();
		this.radiologieRepository = radiologieRepository;
	}
	

	@Override
	public List<Radiologie> getAllRadiologie() {
		return radiologieRepository.findAll();
	}

	@Override
	public Radiologie rechercherRadiologieId(Long id) {
		return	radiologieRepository.findById(id).orElseThrow(() -> 
		new ResourceNotFoundException("Radiologie", "Id", id));
				
	}

	@Override
	public Radiologie modifierRadiologie(Radiologie radiologie, Long id) {
		Radiologie existingRadiologie = radiologieRepository.findById(id).orElseThrow(() -> 
		new ResourceNotFoundException("Radiologie", "Id", id)); 
		existingRadiologie.setPrenom(radiologie.getPrenom());
		existingRadiologie.setNom(radiologie.getNom());
		existingRadiologie.setEmail(radiologie.getEmail());
		existingRadiologie.setActes(radiologie.getActes());
		existingRadiologie.setAdresse(radiologie.getAdresse());
		existingRadiologie.setCachet(radiologie.getCachet());
		existingRadiologie.setCin(radiologie.getCin());
		existingRadiologie.setCodePostal(radiologie.getCodePostal());
		existingRadiologie.setDateNaissance(radiologie.getDateNaissance());
		existingRadiologie.setIce(radiologie.getIce());
		existingRadiologie.setInpe(radiologie.getInpe());
		existingRadiologie.setMotDePasse(radiologie.getMotDePasse());
		existingRadiologie.setSexe(radiologie.getSexe());
		existingRadiologie.setSignature(radiologie.getSignature());
		existingRadiologie.setSituationFamiliale(radiologie.getSituationFamiliale());
		existingRadiologie.setTelephone(radiologie.getTelephone());
		existingRadiologie.setVille(radiologie.getVille());
		
		radiologieRepository.save(existingRadiologie);
		
		return existingRadiologie;
	}

	@Override
	public void supprimerRadiologie(Long id) {
		
		radiologieRepository.findById(id).orElseThrow(
				()->new ResourceNotFoundException("Radiologie", "Id", id));
		radiologieRepository.deleteById(id);
		
		
	}


	@Override
	public Radiologie savesaveRadiologie(RadiologieRequest radiologieRequest) {
		
		Radiologie radiologie = new Radiologie();
		
		radiologie.setVille(radiologieRequest.getVille());
		radiologie.setTelephone(radiologieRequest.getTelephone());
		radiologie.setSituationFamiliale(radiologieRequest.getSituationFamiliale());
		radiologie.setSignature(radiologieRequest.getSignature());
		radiologie.setSexe(radiologieRequest.getSexe());
		radiologie.setPrenom(radiologieRequest.getPrenom());
		radiologie.setNom(radiologieRequest.getNom());
		radiologie.setMotDePasse(radiologieRequest.getMotDePasse());
		radiologie.setInpe(radiologieRequest.getInpe());
		radiologie.setIce(radiologieRequest.getIce());
		radiologie.setEmail(radiologieRequest.getEmail());
		radiologie.setDateNaissance(radiologieRequest.getDateNaissance());
		radiologie.setCodePostal(radiologieRequest.getCodePostal());
		radiologie.setCin(radiologieRequest.getCin());
		radiologie.setCachet(radiologieRequest.getCachet());
				
		return radiologieRepository.save(radiologie);
	}
	
	@Override
	public long countRadiologie(long IdAmin) {
		return radiologieRepository.countRadiologie(IdAmin);
	}

	
}
