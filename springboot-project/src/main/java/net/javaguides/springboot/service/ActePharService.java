package net.javaguides.springboot.service;

import java.util.List;

import org.springframework.data.repository.query.Param;

import net.javaguides.springboot.dto.ActePharRequest;
import net.javaguides.springboot.model.ActeMed;
import net.javaguides.springboot.model.ActePhar;

public interface ActePharService {
	
	ActePhar saveActePhar(ActePharRequest actePharRequest);
	ActePhar modifierActePhar(ActePhar actePhar, Long id);
	List<ActePhar> visualiserActesPhar();
	ActePhar rechercherActePharById(Long id);
	void supprimerActePhar(Long id);
	List<ActeMed> findActeMedPharmaByCinAdherent(String cin);

	List<ActePhar> findActePharmaAdherentinfo(long IdPharma);
	List<ActePhar> findActePharmaMedinfoByIdActPharma(long IdActPharma);



}
