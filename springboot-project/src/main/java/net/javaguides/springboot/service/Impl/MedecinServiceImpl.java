package net.javaguides.springboot.service.Impl;

import java.io.File;
import java.io.IOException;
import java.util.Base64;
import java.util.Date;
import java.util.List;
import java.util.stream.Stream;

import javax.persistence.Column;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import org.springframework.web.multipart.MultipartFile;


import net.javaguides.springboot.dto.MedecinRequest;
import net.javaguides.springboot.exception.ResourceNotFoundException;
import net.javaguides.springboot.model.Medecin;
import net.javaguides.springboot.model.Sexe;
import net.javaguides.springboot.model.SituationFamiliale;
import net.javaguides.springboot.repository.MedecinRepository;
import net.javaguides.springboot.service.MedecinService;
@Service
public class MedecinServiceImpl implements MedecinService {
	@Autowired
	private MedecinRepository medecinRepository;
	
	
	public MedecinServiceImpl(MedecinRepository medecinRepository) {
		super();
		this.medecinRepository = medecinRepository;
	}
	@Override
	public Medecin saveMedecin( MedecinRequest medecinRequest) {
		Medecin medecin= new Medecin();
		medecin.setVille(medecinRequest.getVille());
		medecin.setTelephone(medecinRequest.getTelephone());
		medecin.setSpecialite(medecinRequest.getSpecialite());
		medecin.setSituationFamiliale(medecinRequest.getSituationFamiliale());
		medecin.setSexe(medecinRequest.getSexe());
		medecin.setPrenom(medecinRequest.getPrenom());
		//medecin.setPhotoProfile(medecinRequest.getPhotoProfile());
		medecin.setNom(medecinRequest.getNom());
		medecin.setMotDePasse(medecinRequest.getMotDePasse());
		medecin.setInpe(medecinRequest.getInpe());
		medecin.setIce(medecinRequest.getIce());
		//medecin.setFses(medecinRequest.getFses());
	//	medecin.setFichierCin(medecinRequest.getFichierCin());
		medecin.setEmail(medecinRequest.getEmail());
		medecin.setDateNaissance(medecinRequest.getDateNaissance());
		medecin.setCodePostal(medecinRequest.getCodePostal());
		medecin.setCin(medecinRequest.getCin());
		medecin.setAdresse(medecinRequest.getAdresse());
	//	medecin.setActes(medecinRequest.getActes());
		//medecin.setTypeMutuelle(medecinRequest.getTypeMutuelle());
		/*String fileName = StringUtils.cleanPath(medecinRequest.getCachet().getOriginalFilename());
		if(fileName.contains(".."))
		{
			System.out.println("not a a valid file");
		}
		try {
			medecin.setCachet(Base64.getEncoder().encodeToString(medecinRequest.getCachet().getBytes()));
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		String fileName = StringUtils.cleanPath(file.getOriginalFilename());
		if(fileName.contains(".."))
		{
			System.out.println("not a a valid file");
		}
		try {
			medecin.setCachet(Base64.getEncoder().encodeToString(file.getBytes()));
		} catch (IOException e) {
			e.printStackTrace();
		}*/
		/*
		 * Medecin medecin=Medecin.build(medecinRequest.getSpecialite(),
		 * medecinRequest.getInpe(),medecinRequest.getIce(),
		 * medecinRequest.getSignature(), medecinRequest.getCachet(),
		 * medecinRequest.getCachet_pro(), medecinRequest.getActes(),
		 * medecinRequest.getFses());
		 */
		return medecinRepository.save(medecin);
	}
	@Override
	public Medecin updateMedecin(Medecin medecin, long id) {
		Medecin existingMedecin = medecinRepository.findById(id).orElseThrow(
				() -> new ResourceNotFoundException("Medecin", "Id", id)); 
		
		existingMedecin.setPrenom(medecin.getPrenom());
		existingMedecin.setNom(medecin.getNom());
		existingMedecin.setEmail(medecin.getEmail());
		existingMedecin.setActes(medecin.getActes());
		existingMedecin.setAdresse(medecin.getAdresse());
		//existingMedecin.setCartePro(medecin.getCartePro());
		existingMedecin.setCin(medecin.getCin());
		existingMedecin.setCodePostal(medecin.getCodePostal());
		existingMedecin.setDateNaissance(medecin.getDateNaissance());
		//existingMedecin.setFichierCin(medecin.getFichierCin());
		//existingMedecin.setFses(medecin.getFses());
		existingMedecin.setIce(medecin.getIce());
		existingMedecin.setInpe(medecin.getInpe());
		existingMedecin.setMotDePasse(medecin.getMotDePasse());
		existingMedecin.setSexe(medecin.getSexe());
		//existingMedecin.setPhotoProfile(medecin.getPhotoProfile());
		existingMedecin.setSituationFamiliale(medecin.getSituationFamiliale());
		existingMedecin.setSpecialite(medecin.getSpecialite());
		existingMedecin.setTelephone(medecin.getTelephone());
		existingMedecin.setVille(medecin.getVille());
		
		medecinRepository.save(existingMedecin);
		return existingMedecin;
	}
	@Override
	public void deleteMedecin(long id) {
		medecinRepository.findById(id).orElseThrow(() -> 
		new ResourceNotFoundException("Medecin", "Id", id));
		medecinRepository.deleteById(id);
		
	}
	@Override
	public List<Medecin> getAllMedecins() {
		return medecinRepository.findAll();
	}

	@Override
	public Medecin getMedecinById(long id) {
		return medecinRepository.findProfileMedecinById(id);
		
	}

	/*@Override
	  public Medecin store(MultipartFile File) throws IOException {
	    String fileName = StringUtils.cleanPath(File.getOriginalFilename());
	    Medecin FileDB = new Medecin(fileName, File.getContentType(), File.getBytes());
	    return medecinRepository.save(FileDB);
}*/
 /*	@Override
	 public Medecin store(MultipartFile File,String specialite, String inpe) throws IOException {
	    String fileName = StringUtils.cleanPath(File.getOriginalFilename());
	    Medecin medecin = new Medecin(fileName, File.getContentType(), File.getBytes(),specialite,inpe);
	   medecin.setVille(medecinRequest.getVille());
		medecin.setTelephone(medecinRequest.getTelephone());
		medecin.setSituationFamiliale(medecinRequest.getSituationFamiliale());
		medecin.setSexe(medecinRequest.getSexe());
		medecin.setPrenom(medecinRequest.getPrenom());
		medecin.setPhotoProfile(medecinRequest.getPhotoProfile());
		medecin.setNom(medecinRequest.getNom());
		medecin.setMotDePasse(medecinRequest.getMotDePasse());
		medecin.setIce(medecinRequest.getIce());
		medecin.setFses(medecinRequest.getFses());
		medecin.setFichierCin(medecinRequest.getFichierCin());
		medecin.setEmail(medecinRequest.getEmail());
		medecin.setDateNaissance(medecinRequest.getDateNaissance());
		medecin.setCodePostal(medecinRequest.getCodePostal());
		medecin.setCin(medecinRequest.getCin());
		medecin.setAdresse(medecinRequest.getAdresse());
		medecin.setActes(medecinRequest.getActes());
		medecin.setTypeMutuelle(medecinRequest.getTypeMutuelle());
	    return medecinRepository.save(medecin);
}*/
/*	@Override
	public void  saveMed(MultipartFile file,String nom,String prenom,Date dateNaissance,String cin,String email,
			String telephone,String motDePasse,String adresse,String ville, String codePostal, Sexe sexe,SituationFamiliale situationFamiliale, 
			File photoProfile,File fichierCin,String cachet,
			String specialite,String inpe,String ice, File cartePro, String typeMutuelle
	)
	{
		Medecin medecin=new Medecin();
		String fileName = StringUtils.cleanPath(file.getOriginalFilename());
		if(fileName.contains(".."))
		{
			System.out.println("not a a valid file");
		}
		try {
			medecin.setCachet(Base64.getEncoder().encodeToString(file.getBytes()));
		} catch (IOException e) {
			e.printStackTrace();
		}
		medecin.setVille(ville);
		medecin.setTelephone(telephone);
		medecin.setSpecialite(specialite);
		medecin.setSituationFamiliale(situationFamiliale);
		medecin.setSexe(sexe);
		medecin.setPrenom(prenom);
		medecin.setPhotoProfile(photoProfile);
		medecin.setNom(nom);
		medecin.setMotDePasse(motDePasse);
		medecin.setInpe(inpe);
		medecin.setIce(ice);
		medecin.setFichierCin(fichierCin);
		medecin.setEmail(email);
		medecin.setDateNaissance(dateNaissance);
		medecin.setCodePostal(codePostal);
		medecin.setCin(cin);
		medecin.setCartePro(cartePro);
		medecin.setAdresse(adresse);
		medecin.setTypeMutuelle(typeMutuelle);
		
		medecinRepository.save(medecin);
		
		
		
		
		}*/
	@Override
	public void saveMedToDB(MultipartFile file, String nom, String prenom, String inpe) {
		Medecin p = new Medecin();
		/*String fileName = StringUtils.cleanPath(file.getOriginalFilename());
		if(fileName.contains(".."))
		{
			System.out.println("not a a valid file");
		}
		try {
			p.setCachet(Base64.getEncoder().encodeToString(file.getBytes()));
		} catch (IOException e) {
			e.printStackTrace();
		}*/
		p.setNom(nom);
		
        p.setPrenom(prenom);
        p.setInpe(inpe);
        
        medecinRepository.save(p);
		
	}
	@Override
	public Medecin saveMedecinP(Medecin medecinRequest) {
		
		return medecinRepository.save(medecinRequest);
	}
	
	
	/*
	 * @Override public List<Medecin> findProfilPharmacie(Long id) { // TODO
	 * Auto-generated method stub return medecinRepository.findProfilPharmacie(id);
	 * }
	 * 
	 * @Override public List<Medecin> findProfilPharmacieComplet(Long id) { // TODO
	 * Auto-generated method stub
	 * 
	 * 
	 * return medecinRepository.findProfilPharmacieComplet(id); }
	
	@Override
	public List<Medecin> findProfilMedecinComplet(Long id) {
		// TODO Auto-generated method stub
		
		
		return medecinRepository.findProfilMedecinComplet(id);
	}*/
	@Override
	public Medecin RecherchMedecinById(Long id) {
		return medecinRepository.findProfileMedecinById(id);
	}
	
	@Override
	public long countMedecin(long IdAmin) {
		return medecinRepository.countMedecin(IdAmin);
	}
	
	
	
	

}
