package net.javaguides.springboot.service;


import java.io.File;
import java.io.IOException;
import java.util.Date;
import java.util.List;

import org.springframework.data.repository.query.Param;
import org.springframework.web.multipart.MultipartFile;

import net.javaguides.springboot.dto.MedecinRequest;
import net.javaguides.springboot.model.Medecin;
import net.javaguides.springboot.model.Sexe;
import net.javaguides.springboot.model.SituationFamiliale;

public interface MedecinService {
	Medecin saveMedecin(MedecinRequest medecinRequest);
	Medecin saveMedecinP(Medecin medecinRequest);
    List<Medecin> getAllMedecins();
    Medecin getMedecinById(long id);
	Medecin updateMedecin(Medecin medecin, long id);
	void deleteMedecin(long id);
	public void  saveMedToDB(MultipartFile file,String nom,String prenom,String inpe);			
	// List<Medecin> DetailMedecin();
    Medecin RecherchMedecinById(Long id);
    
    long countMedecin(long IdAmin);
   
}
