package net.javaguides.springboot.service;

import java.util.List;

import org.springframework.data.repository.query.Param;

import net.javaguides.springboot.dto.ActeRadioRequest;
import net.javaguides.springboot.model.ActeMed;
import net.javaguides.springboot.model.ActeRadio;

public interface ActeRadioService {
	
	ActeRadio creerActeRadion(ActeRadioRequest acteRadioRequest);
	ActeRadio modificationActeRadio(ActeRadio acteRadio,  Long id);
	List<ActeRadio> visualiserActeRadio();
	ActeRadio rechercherActeRadiById(Long id);
	void supprimerActeRadio(Long id);
	List<ActeMed>findActeMedRAdioByCinAdherent(String cin);
	
	List<ActeRadio> findActeRadioAdherentinfo(long IdRadio);
	 List<ActeRadio> findActeRadioMedinfoByIdActRadio(long IdActRadio);


	
	

}
