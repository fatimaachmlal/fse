package net.javaguides.springboot.service;


import java.util.List;



import net.javaguides.springboot.dto.AldRequest;
import net.javaguides.springboot.model.Ald;
import net.javaguides.springboot.model.Beneficiaire;

public interface AldService {
	Ald saveAld( AldRequest aldRequest);
	List<Ald> getAllAlds();
	Ald getAldById(long id);
	Ald updateAld(Ald ald, long id);
	void deleteAld(long id);
	List<Ald> findAldByAdherent(long id_adherent);


}

