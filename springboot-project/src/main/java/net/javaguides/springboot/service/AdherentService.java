package net.javaguides.springboot.service;




import java.util.List;

import net.javaguides.springboot.dto.AdherentRequest;
import net.javaguides.springboot.model.Adherent;
import net.javaguides.springboot.model.Ald;
import net.javaguides.springboot.model.Beneficiaire;

public interface AdherentService {
	
	Adherent saveAdherent(AdherentRequest adherentRequest);
	List<Adherent> getAllAdherents();
	Adherent getAdherentById(long id);
	Adherent updateAdherent(Adherent adherent, long id);
	void deleteAdherent(long id);
	Adherent getAdherentByCin(String cin);
	 long countAdherent(long IdAmin);




}

