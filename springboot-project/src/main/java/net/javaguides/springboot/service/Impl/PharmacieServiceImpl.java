package net.javaguides.springboot.service.Impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import net.javaguides.springboot.dto.PharmacieRequest;
import net.javaguides.springboot.exception.ResourceNotFoundException;
import net.javaguides.springboot.model.Pharmacie;
import net.javaguides.springboot.repository.PharmacieRepository;
import net.javaguides.springboot.service.PharmacieService;


@Service
public class PharmacieServiceImpl implements PharmacieService{
	
	@Autowired
	private PharmacieRepository pharmacieRepository;
	
	

	public PharmacieServiceImpl(PharmacieRepository pharmacieRepository) {
		super();
		this.pharmacieRepository = pharmacieRepository;
	}

	@Override
	public Pharmacie savePharmacie(PharmacieRequest pharmacieRequest) {
		
		Pharmacie pharmacie = new Pharmacie();
		pharmacie.setVille(pharmacieRequest.getVille());
		pharmacie.setTelephone(pharmacieRequest.getTelephone());
		pharmacie.setSituationFamiliale(pharmacieRequest.getSituationFamiliale());
		pharmacie.setSexe(pharmacieRequest.getSexe());
		pharmacie.setPrenom(pharmacieRequest.getPrenom());
		pharmacie.setNom(pharmacieRequest.getNom());
		pharmacie.setMotDePasse(pharmacieRequest.getMotDePasse());
		pharmacie.setInpe(pharmacieRequest.getInpe());
		pharmacie.setIce(pharmacieRequest.getIce());
		pharmacie.setEmail(pharmacieRequest.getEmail());
		pharmacie.setDateNaissance(pharmacieRequest.getDateNaissance());
		pharmacie.setCodePostal(pharmacieRequest.getCodePostal());
		pharmacie.setCin(pharmacieRequest.getCin());
		pharmacie.setRaisonSociale(pharmacie.getRaisonSociale());

		pharmacie.setAdresse(pharmacieRequest.getAdresse());
		pharmacie.setActes(pharmacieRequest.getActes());
		
		return pharmacieRepository.save(pharmacie);
	}

	@Override
	public List<Pharmacie> getAllPharmacie() {
		return pharmacieRepository.findAll();
	}

	@Override
	public Pharmacie rechercherPharmacieId(Long id) {
		return pharmacieRepository.findById(id).orElseThrow(() -> 
		new ResourceNotFoundException("Pharmacie", "Id", id));
		 
	}

	@Override
	public Pharmacie modifierPharmacie(Pharmacie pharmacie, Long id) {
		Pharmacie existingPharmacie = pharmacieRepository.findById(id).orElseThrow(() -> 
		new ResourceNotFoundException("Pharmacie", "Id", id)); 
		existingPharmacie.setPrenom(pharmacie.getPrenom());
		existingPharmacie.setNom(pharmacie.getNom());
		existingPharmacie.setEmail(pharmacie.getEmail());
		existingPharmacie.setActes(pharmacie.getActes());
		existingPharmacie.setAdresse(pharmacie.getAdresse());
		
		existingPharmacie.setCin(pharmacie.getCin());
		existingPharmacie.setCodePostal(pharmacie.getCodePostal());
		existingPharmacie.setDateNaissance(pharmacie.getDateNaissance());
		existingPharmacie.setIce(pharmacie.getIce());
		existingPharmacie.setInpe(pharmacie.getInpe());
		existingPharmacie.setMotDePasse(pharmacie.getMotDePasse());
		pharmacie.setRaisonSociale(pharmacie.getRaisonSociale());
		existingPharmacie.setSexe(pharmacie.getSexe());
		existingPharmacie.setSituationFamiliale(pharmacie.getSituationFamiliale());
		existingPharmacie.setTelephone(pharmacie.getTelephone());
		existingPharmacie.setVille(pharmacie.getVille());
		
		pharmacieRepository.save(existingPharmacie);
		return existingPharmacie;
	}

	@Override
	public void supprimerPharmacie(Long id) {
		
		pharmacieRepository.findById(id).orElseThrow(() -> 
		new ResourceNotFoundException("Pharmacie", "Id", id));
		pharmacieRepository.deleteById(id);
		
		
	}
	@Override
	public long countPharmacie(long IdAmin) {
		return pharmacieRepository.countPharmacie(IdAmin);
	}
	
	

}
