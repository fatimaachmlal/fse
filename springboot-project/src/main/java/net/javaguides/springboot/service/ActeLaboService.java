package net.javaguides.springboot.service;

import java.util.List;

import org.springframework.data.repository.query.Param;

import net.javaguides.springboot.dto.ActeLaboRequest;
import net.javaguides.springboot.model.ActeLabo;
import net.javaguides.springboot.model.ActeMed;

public interface ActeLaboService {
	ActeLabo saveActeLabo(ActeLaboRequest acteLaboRequest);
	ActeLabo updateActeLabo(ActeLabo acteLabo, long id);
    List<ActeLabo> getAllActesLabo();
    ActeLabo getActeLaboById(long id);
    void deleteActeLabo(long id);
    
	List<ActeMed> findActeMedLaboByCinAdherent(String cin);
	
	List<ActeLabo> findActeloboAdherentinfo(long IdLabo);
	
	 List<ActeLabo> findActeloboMedinfoByIdActLabo( long IdActLabo);


	

}
