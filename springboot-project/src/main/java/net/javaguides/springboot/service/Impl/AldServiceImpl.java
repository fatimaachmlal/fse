package net.javaguides.springboot.service.Impl;

import java.util.List;
//import java.util.Optional;

import org.springframework.stereotype.Service;

import net.javaguides.springboot.dto.AldRequest;
import net.javaguides.springboot.exception.ResourceNotFoundException;
import net.javaguides.springboot.model.Adherent;
import net.javaguides.springboot.model.Ald;
import net.javaguides.springboot.model.Beneficiaire;
import net.javaguides.springboot.repository.AldRepository;
import net.javaguides.springboot.service.AldService;

@Service
public class AldServiceImpl implements AldService{

	private AldRepository aldRepository;
	
	public AldServiceImpl(AldRepository aldRepository) {
		super();
		this.aldRepository = aldRepository;
	}

	@Override
	public Ald saveAld(AldRequest aldRequest) {
		Ald ald= new Ald();		
		ald.setCodeAld(aldRequest.getCodeAld());
		ald.setnDossier(aldRequest.getnDossier());	
		ald.setAdherent(aldRequest.getAdherent());
		//ald.setBeneficiaire(aldRequest.getBeneficiaire());
				
		return aldRepository.save(ald);
	}
	@Override
	public List<Ald> getAllAlds() {
		return aldRepository.findAll();
	}

	@Override
	public Ald getAldById(long id) {

//		}
		return aldRepository.findById(id).orElseThrow(() -> 
						new ResourceNotFoundException("Ald", "Id", id));
		
	}

	@Override
	public Ald updateAld(Ald ald, long id) {
		
		
		Ald existingAld = aldRepository.findById(id).orElseThrow(
				() -> new ResourceNotFoundException("Ald", "Id", id)); 
		
		existingAld.setCodeAld(ald.getCodeAld());
		existingAld.setnDossier(ald.getnDossier());
		
		
		
		
		
		aldRepository.save(existingAld);
		return existingAld;
	}

	@Override
	public void deleteAld(long id) {
		
		// check whether a ald exist in a DB or not
		aldRepository.findById(id).orElseThrow(() -> 
								new ResourceNotFoundException("Ald", "Id", id));
		aldRepository.deleteById(id);
	}
	@Override
	public List<Ald> findAldByAdherent(long id_adherent) {
		return aldRepository.findAldByAdherent(id_adherent);
	}
	
	
}


