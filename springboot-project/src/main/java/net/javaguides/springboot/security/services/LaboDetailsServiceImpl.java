package net.javaguides.springboot.security.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.transaction.annotation.Transactional;

import net.javaguides.springboot.model.Laboratoire;
import net.javaguides.springboot.model.Medecin;
import net.javaguides.springboot.repository.LaboratoireRepository;
import net.javaguides.springboot.repository.MedecinRepository;

public class LaboDetailsServiceImpl implements UserDetailsService {
	
	@Autowired
	LaboratoireRepository laboRepository;
	@Override
	@Transactional
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
		Laboratoire user = laboRepository.findByUsername(username)
				.orElseThrow(() -> new UsernameNotFoundException("User Not Found with username: " + username));
		return LaboDetailsImpl.build(user);
	}
}
