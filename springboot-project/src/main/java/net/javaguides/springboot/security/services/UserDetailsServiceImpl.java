package net.javaguides.springboot.security.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import net.javaguides.springboot.model.Medecin;
import net.javaguides.springboot.model.Laboratoire;
import net.javaguides.springboot.model.Radiologie;
import net.javaguides.springboot.model.Pharmacie;
import net.javaguides.springboot.model.Adherent;
import net.javaguides.springboot.model.Admin;
import net.javaguides.springboot.model.User;
import net.javaguides.springboot.repository.LaboratoireRepository;
import net.javaguides.springboot.repository.MedecinRepository;
import net.javaguides.springboot.repository.UserRepository;
import net.javaguides.springboot.repository.RadiologieRepository;
import net.javaguides.springboot.repository.PharmacieRepository;
import net.javaguides.springboot.repository.AdherentRepository;
import net.javaguides.springboot.repository.AdminRepository;

@Service
public class UserDetailsServiceImpl implements UserDetailsService {
	
	@Autowired
	LaboratoireRepository laboratoireRepository;
	
	@Autowired
	MedecinRepository medRepository;
	
	@Autowired
	RadiologieRepository radRepository;
	
	@Autowired
	PharmacieRepository PharRepository;
	@Autowired
	AdherentRepository adherentRepository;
	@Autowired
	AdminRepository adminRepository;
	
	
	
	@Override
	@Transactional
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
	
		Laboratoire laboratoire = new Laboratoire();
		Medecin	medecin=new Medecin();
		Radiologie	radiologie=new Radiologie();
		Pharmacie	pharmacie=new Pharmacie();
		Adherent	adherent=new Adherent();
		Admin	admin=new Admin();
if((laboratoireRepository.findByUsername(username)).isPresent() ) {
    System.out.println((laboratoireRepository.findByUsername(username)).isPresent());
    laboratoire = laboratoireRepository.findByUsername(username)
			.orElseThrow(() -> new UsernameNotFoundException("User Not Found with username: " + username));
	System.out.println(laboratoire);

    return UserDetailsImpl.build(laboratoire);
}else if( (medRepository.findByUsername(username)).isPresent()) {
	medecin = medRepository.findByUsername(username)
			.orElseThrow(() -> new UsernameNotFoundException("User Not Found with username: " + username));
	//System.out.println(medecin);
	return UserDetailsImpl.build(medecin);
}else if( (radRepository.findByUsername(username)).isPresent()) {
	radiologie = radRepository.findByUsername(username)
			.orElseThrow(() -> new UsernameNotFoundException("User Not Found with username: " + username));
	return UserDetailsImpl.build(radiologie);
}else if( (PharRepository.findByUsername(username)).isPresent()) {
	pharmacie = PharRepository.findByUsername(username)
			.orElseThrow(() -> new UsernameNotFoundException("User Not Found with username: " + username));
	return UserDetailsImpl.build(pharmacie);
}else if( (adherentRepository.findByUsername(username)).isPresent()) {
	adherent = adherentRepository.findByUsername(username)
			.orElseThrow(() -> new UsernameNotFoundException("User Not Found with username: " + username));
	return UserDetailsImpl.build(adherent);
}
else if( (adminRepository.findByUsername(username)).isPresent()) {
	admin = adminRepository.findByUsername(username)
			.orElseThrow(() -> new UsernameNotFoundException("User Not Found with username: " + username));
	return UserDetailsImpl.build(admin);
}
return null;






		
		 
		
	}

}
