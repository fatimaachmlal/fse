package net.javaguides.springboot.Controller;



import java.util.List;


import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RestController;

import net.javaguides.springboot.dto.PharmacieRequest;
import net.javaguides.springboot.model.Adherent;
import net.javaguides.springboot.model.Medecin;
import net.javaguides.springboot.model.Pharmacie;
import net.javaguides.springboot.payload.response.ProfilResponse;
import net.javaguides.springboot.repository.MedecinRepository;
import net.javaguides.springboot.repository.RoleRepository;
import net.javaguides.springboot.security.jwt.JwtUtils;
import net.javaguides.springboot.service.PharmacieService;


//@CrossOrigin(origins = "*", allowedHeaders = "*")
@CrossOrigin(origins = "*")
@RestController
@RequestMapping("/api/pharmacies")
public class PharmacieController {
	private byte[] bytes;
	@ExceptionHandler({Exception.class})
	public void resolveException(Exception e) {
	    e.printStackTrace();
	}
	
	 @Autowired
	  AuthenticationManager authenticationManager;
	  
	  @Autowired
	  MedecinRepository medecinRepository;
	  
	  @Autowired
	  RoleRepository roleRepository;

	  @Autowired
	  PasswordEncoder encoder;

	  @Autowired
	  JwtUtils jwtUtils;
	  
	private PharmacieService pharmacieService;
	
	public PharmacieController(PharmacieService pharmacieService) {
		super();
		this.pharmacieService = pharmacieService;
	}
	
	@PostMapping("inscription")
	 @PreAuthorize("hasRole('PHRMACIE')")
	public ResponseEntity<Pharmacie> saveRadiologie(@RequestBody @Valid PharmacieRequest pharmacieRequest){
		return new ResponseEntity<Pharmacie>(pharmacieService.savePharmacie(pharmacieRequest), HttpStatus.CREATED);

	}
   // @CrossOrigin(origins="http://localhost:4200/pharmacie")
	@GetMapping("/recherchepharmacie")
	public List<Pharmacie> getAllPharmacie(){
		return  pharmacieService.getAllPharmacie();
	}	
	
	@GetMapping("/CountPharmacie/{IdAmin}")
	public long getCountPhar(@PathVariable("IdAmin") long IdAmin){
		return pharmacieService.countPharmacie(IdAmin);
	}
	
	
	
	
	@PutMapping("modifierinfos/{id}")
	 @PreAuthorize("hasRole('PHRMACIE')")
	public ResponseEntity<Pharmacie> updatePharmacie(@PathVariable("id") long id,@RequestBody Pharmacie pharmacie){
		return new ResponseEntity<Pharmacie>(pharmacieService.modifierPharmacie(pharmacie, id), HttpStatus.OK);
	}
	
	@DeleteMapping("/supprimerPharmacie/{id}")
	 @PreAuthorize("hasRole('PHRMACIE')")
	public ResponseEntity<String> SupprimerPharmacie(@PathVariable("id") long id){
		
		pharmacieService.supprimerPharmacie(id);
		
		return new ResponseEntity<String>("Pharmacie deleted successfully!.", HttpStatus.OK);
	}
	
	@GetMapping("visualiserpharmacie")
	 @PreAuthorize("hasRole('PHRMACIE')")
	public List<Pharmacie> getAllPharmacies(){
		return pharmacieService.getAllPharmacie();
	}
	
	 @PreAuthorize("hasRole('PHRMACIE')")
	@GetMapping("rechercherPharmacie/{id}")
	public ResponseEntity<Pharmacie> recherchePharmacieParId(@PathVariable("id") long pharmacieId){
		return new ResponseEntity<Pharmacie>(pharmacieService.rechercherPharmacieId(pharmacieId), HttpStatus.OK);
	}
	
	@GetMapping("ProfilPharmacie/{id}")


	public ProfilResponse ProfilPharmacie(@PathVariable("id") long medecinId){
		Pharmacie pharmacie= new Pharmacie();
		ProfilResponse profilpharmacie=new ProfilResponse();
		pharmacie=pharmacieService.rechercherPharmacieId(medecinId);
		profilpharmacie.setId(pharmacie.getId());
		profilpharmacie.setNom(pharmacie.getNom());
		profilpharmacie.setInpe(pharmacie.getInpe());
		profilpharmacie.setId(pharmacie.getId());
		profilpharmacie.setNom(pharmacie.getNom());
		profilpharmacie.setInpe(pharmacie.getInpe());
		profilpharmacie.setPrenom(pharmacie.getPrenom());
		profilpharmacie.setSituationFamiliale(pharmacie.getSituationFamiliale());
		profilpharmacie.setSexe(pharmacie.getSexe());
		profilpharmacie.setVille(pharmacie.getVille());
		profilpharmacie.setAdresse(pharmacie.getAdresse());
		profilpharmacie.setTelephone(pharmacie.getTelephone());
		profilpharmacie.setEmail(pharmacie.getEmail());
		profilpharmacie.setCin(pharmacie.getCin());
		profilpharmacie.setDateNaissance(pharmacie.getDateNaissance());
		profilpharmacie.setRaisonSociale(pharmacie.getRaisonSociale());
		profilpharmacie.setIce(pharmacie.getIce());
		return profilpharmacie;
	}
	
	
	
}
