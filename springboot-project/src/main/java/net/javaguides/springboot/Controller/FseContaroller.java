package net.javaguides.springboot.Controller;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.repository.query.Param;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

import net.javaguides.springboot.model.ActeLabo;
import net.javaguides.springboot.model.ActeMed;
import net.javaguides.springboot.model.Fse;
import net.javaguides.springboot.model.Medecin;
import net.javaguides.springboot.model.Radiologie;
import net.javaguides.springboot.payload.request.FseRequest;
import net.javaguides.springboot.repository.FseRepository;
import net.javaguides.springboot.service.ActeMedService;
import net.javaguides.springboot.service.FseService;


@RestController
@RequestMapping("/api/FSE")
@CrossOrigin(origins = "*")
public class FseContaroller {
	@Autowired
	private FseService fseService;
	@Autowired
	private FseRepository fseRepository;
	

	public FseContaroller(FseService fseService) {
		super();
		this.fseService = fseService;
	}
	@PostMapping("/RecherchFse")
	public ResponseEntity<List<Fse>>  RecherchFse(@RequestBody FseRequest Fseinfo) throws ParseException{
		
			return new ResponseEntity<List<Fse>>(fseService.findFseByMedAdherentActe(Fseinfo.getId_med(),Fseinfo.getId_act_med(), Fseinfo.getDateActe()),HttpStatus.OK);
		
	}
	
	
		  
	@PutMapping("/AjouterActeLaboFSE/{IdActeLabo}/{fseId}")
	public ResponseEntity<String>  AjouterActeLaboFSE( @PathVariable("IdActeLabo") long IdActeLabo, @PathVariable("fseId")long fseId){
		return new ResponseEntity<String>(fseService.AjouterActeLaboFSE(IdActeLabo,fseId)+"upadte", HttpStatus.OK);
		
		  }
	
	@PutMapping("/AjouterActeRadioToFSE/{IdActeRadio}/{fseId}")
	public ResponseEntity<String>  AjouterActeRadioToFSE( @PathVariable("IdActeRadio") long IdActeRadio, @PathVariable("fseId")long fseId){
		return new ResponseEntity<String>(fseService.AjouterActeRadioToFSE(IdActeRadio,fseId)+"upadte", HttpStatus.OK);
		
		  }
	@PutMapping("/AjouterActePharToFSE/{IdActePharm}/{fseId}")
	public ResponseEntity<String>  AjouterActePharToFSE( @PathVariable("IdActePharm") long IdActePharm, @PathVariable("fseId")long fseId){
		return new ResponseEntity<String>(fseService.AjouterActePharToFSE(IdActePharm,fseId)+"upadte", HttpStatus.OK);
		
		  }
	
	
	@GetMapping("RechercherFseWithAdhrentInfo/{id_adherent}")
	public ResponseEntity<Fse> getFseWithAdhrentInfon( @PathVariable("id_adherent") long id_adherent){
		return new ResponseEntity<Fse>(fseService.findFseWithAdhrentInfo(id_adherent),HttpStatus.OK);
	}
	@GetMapping("RechercherdFseWithMedecintInfo/{id_adherent}/{Id_fse}")
	public ResponseEntity<Fse> getFseWithMedecintInfo(@PathVariable("id_adherent") long id_adherent,@PathVariable("Id_fse") long Id_fse ){
		return new ResponseEntity<Fse>(fseService.findFseWithMedecintInfo(id_adherent,Id_fse),HttpStatus.OK);
	}
	@GetMapping("RechercherFseWithRadiotInfo/{id_adherent}/{Id_fse}")
	public ResponseEntity<Fse> getFseWithRadiotInfo(@PathVariable("id_adherent") long id_adherent,@PathVariable("Id_fse") long Id_fse ){
		return new ResponseEntity<Fse>(fseService.findFseWithRadiotInfo(id_adherent,Id_fse),HttpStatus.OK);
	}
	@GetMapping("RechercherFseWithLaboInfo/{id_adherent}/{Id_fse}")
	public ResponseEntity<Fse> getFseWithLaboInfo(@PathVariable("id_adherent") long id_adherent,@PathVariable("Id_fse") long Id_fse ){
		return new ResponseEntity<Fse>(fseService.findFseWithLaboInfo(id_adherent,Id_fse),HttpStatus.OK);
	}
	@GetMapping("RechercherFseWithPharmaInfo/{id_adherent}/{Id_fse}")
	public ResponseEntity<Fse> getFseWithPharmaInfo(@PathVariable("id_adherent") long id_adherent,@PathVariable("Id_fse") long Id_fse ){
		return new ResponseEntity<Fse>(fseRepository.findFseWithPharmaInfo(id_adherent,Id_fse),HttpStatus.OK);
	}

	@GetMapping("RechercherFseByAdherent/{id_adherent}")
	public ResponseEntity<List<Fse>> getFseByAdherent(@PathVariable("id_adherent") long id_adherent ){
		return new ResponseEntity<List<Fse>>(fseRepository.findFseByAdherent(id_adherent),HttpStatus.OK);
	}


}
