package net.javaguides.springboot.Controller;

import java.io.IOException;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import javax.transaction.Transactional;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.fasterxml.jackson.databind.ObjectMapper;

import net.javaguides.springboot.dto.ActeLaboRequest;
import net.javaguides.springboot.dto.AdherentRequest;
import net.javaguides.springboot.dto.AdminRequest;
import net.javaguides.springboot.dto.LaboratoireRequest;
import net.javaguides.springboot.dto.MedecinRequest;
import net.javaguides.springboot.dto.PharmacieRequest;
import net.javaguides.springboot.dto.RadiologieRequest;
import net.javaguides.springboot.model.ActeLabo;
import net.javaguides.springboot.model.Adherent;
import net.javaguides.springboot.model.Admin;

import net.javaguides.springboot.model.ERole;
import net.javaguides.springboot.model.Laboratoire;
import net.javaguides.springboot.model.Medecin;
import net.javaguides.springboot.model.Pharmacie;
import net.javaguides.springboot.model.Radiologie;
import net.javaguides.springboot.model.Role;
import net.javaguides.springboot.payload.request.LoginRequest;
import net.javaguides.springboot.payload.request.SignupRequest;
import net.javaguides.springboot.payload.request.SignupRequestLabo;
import net.javaguides.springboot.payload.response.JwtResponse;
import net.javaguides.springboot.payload.response.MessageResponse;
import net.javaguides.springboot.repository.LaboratoireRepository;
import net.javaguides.springboot.repository.MedecinRepository;
import net.javaguides.springboot.repository.AdminRepository;
import net.javaguides.springboot.repository.PharmacieRepository;
import net.javaguides.springboot.repository.RadiologieRepository;
import net.javaguides.springboot.repository.RoleRepository;
import net.javaguides.springboot.repository.AdherentRepository;
import net.javaguides.springboot.security.jwt.JwtUtils;
import net.javaguides.springboot.security.services.UserDetailsImpl;

@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@RequestMapping("/api/auth")
public class AuthController {
  @Autowired
  AuthenticationManager authenticationManager;

  @Autowired
  MedecinRepository userRepository;
  @Autowired
  AdminRepository AdminRepository;
  @Autowired
  LaboratoireRepository laboRepository;
  
  @Autowired
  PharmacieRepository pharmacieRepository;
  @Autowired
  RadiologieRepository radiologieRepository;  
  
  @Autowired
  AdherentRepository adherentRepository;
  
  @Autowired
  RoleRepository roleRepository;

  @Autowired
  PasswordEncoder encoder;

  @Autowired
  JwtUtils jwtUtils;


  @PostMapping("/signin")
  public ResponseEntity<?> authenticateUser(@Valid @RequestBody LoginRequest loginRequest) {

    Authentication authentication = authenticationManager.authenticate(
        new UsernamePasswordAuthenticationToken(loginRequest.getUsername(), loginRequest.getPassword()));

    SecurityContextHolder.getContext().setAuthentication(authentication);
    String jwt = jwtUtils.generateJwtToken(authentication);
    
    UserDetailsImpl userDetails = (UserDetailsImpl) authentication.getPrincipal();  
    System.out.println(userDetails);
    List<String> roles = userDetails.getAuthorities().stream()
        .map(item -> item.getAuthority())
        .collect(Collectors.toList());
    System.out.println(roles);

    return ResponseEntity.ok(new JwtResponse(jwt, 
                         userDetails.getId(), 
                         userDetails.getUsername(), 
                         userDetails.getEmail(), 
                         roles));
  }


	
	  @PostMapping("/signupMedecin") 
	 // public ResponseEntity<?> registerUser(@Valid @RequestParam("medecinRequest") String signUp) throws IOException {
			public ResponseEntity<?> saveActeLabo(@RequestBody @Valid MedecinRequest signUp){

	/*	  ObjectMapper mapper = new ObjectMapper();
		  SignupRequest signUpRequest = mapper.readValue(signUp, SignupRequest.class);*/
		  if(userRepository.existsByUsername(signUp.getUsername())) { 
			  return ResponseEntity .badRequest() .body(new MessageResponse("Error: Username is already taken!")); }
		  if (userRepository.existsByEmail(signUp.getEmail())) {
			  return ResponseEntity .badRequest() .body(new MessageResponse("Error: Email is already in use!")); } // Create new user'saccount 
	  Medecin user = new Medecin();
	  
	  user.setUsername(signUp.getUsername());
	  user.setEmail(signUp.getEmail());
	  user.setMotDePasse(encoder.encode(signUp.getMotDePasse()));
	  user.setVille(signUp.getVille());
	  user.setTelephone(signUp.getTelephone());
	  user.setSpecialite(signUp.getSpecialite());
	  user.setTypeMutuelle(signUp.getTypeMutuelle());
	  user.setSexe(signUp.getSexe());
	  user.setPrenom(signUp.getPrenom());
	  user.setNom(signUp.getNom());
	  user.setInpe(signUp.getInpe());
	  user.setIce(signUp.getIce()); 
	  user.setDateNaissance(signUp.getDateNaissance());
	  user.setCodePostal(signUp.getCodePostal());
	  user.setCin(signUp.getCin()); //	
	  user.setAdresse(signUp.getAdresse());
	
		  Set<String> strRoles =signUp.getRole();
	  Set<Role> roles = new HashSet<>(); 
	           if (strRoles == null) 
	           	{ Role userRole = roleRepository.findByName(ERole.ROLE_USER).orElseThrow(() -> new RuntimeException("Error: Role is not found.")); 
	           	roles.add(userRole); }
	           else { strRoles.forEach(role -> { 
	        	   switch (role) { 
	        	   case "admin": Role adminRole = roleRepository.findByName(ERole.ROLE_ADMIN)
	        			   	.orElseThrow(() -> new RuntimeException("Error: Role is not found."));
	        	   			roles.add(adminRole);
	        	   			break; 
	        	   	case "medecin": Role modRole = roleRepository.findByName(ERole.ROLE_MEDCECIN) 
	        	   			.orElseThrow(() -> new RuntimeException("Error: Role is not found."));
	        	   			roles.add(modRole);
	        	   			break;
	        	   	default: Role userRole = roleRepository.findByName(ERole.ROLE_USER)
	        	   			.orElseThrow(() -> new RuntimeException("Error: Role is not found."));
	        	   			roles.add(userRole);
	        	   			} });
	           }
	           
	   user.setRoles(roles);
	   userRepository.save(user);
	//   imageService.sauvegardeImage(photoProfil, cachetSignature, CarteCin, carteProfessionnelle, user); 
	   return ResponseEntity.ok(new MessageResponse("User registered successfully!")); 
	   }
	 
	  
	  
	  
	  
	  
	  
	  @PostMapping("/signupPharmacie") 
	  
			public ResponseEntity<?> saveActeLabo(@RequestBody @Valid PharmacieRequest signUp){

		  
		  if(userRepository.existsByUsername(signUp.getUsername())) { 
			  return ResponseEntity .badRequest() .body(new MessageResponse("Error: Username is already taken!")); }
		  if (userRepository.existsByEmail(signUp.getEmail())) {
			  return ResponseEntity .badRequest() .body(new MessageResponse("Error: Email is already in use!")); } // Create new user'saccount 
		  Pharmacie user = new Pharmacie();
	  
	  user.setUsername(signUp.getUsername());
	  user.setEmail(signUp.getEmail());
	  user.setMotDePasse(encoder.encode(signUp.getMotDePasse()));
	  user.setVille(signUp.getVille());
	  user.setTelephone(signUp.getTelephone());
	  user.setSituationFamiliale(signUp.getSituationFamiliale());
	  user.setSexe(signUp.getSexe());
	  user.setPrenom(signUp.getPrenom());
	  user.setNom(signUp.getNom());
	  user.setInpe(signUp.getInpe());
	  user.setIce(signUp.getIce()); 
	  user.setDateNaissance(signUp.getDateNaissance());
	  user.setCodePostal(signUp.getCodePostal());
	  user.setCin(signUp.getCin());
	  user.setAdresse(signUp.getAdresse());
	  user.setRaisonSociale(signUp.getRaisonSociale());
	  Set<String> strRoles =signUp.getRole();
	  Set<Role> roles = new HashSet<>(); 
	           if (strRoles == null) 
	           	{ Role userRole = roleRepository.findByName(ERole.ROLE_USER).orElseThrow(() -> new RuntimeException("Error: Role is not found.")); 
	           	roles.add(userRole); }
	           else { strRoles.forEach(role -> { 
	        	   switch (role) { 
	        	       	   	case "pharmacie": Role modRole = roleRepository.findByName(ERole.ROLE_PHRMACIE) 
	        	   			.orElseThrow(() -> new RuntimeException("Error: Role is not found."));
	        	   			roles.add(modRole);
	        	   			break;
	        	   	default: Role userRole = roleRepository.findByName(ERole.ROLE_USER)
	        	   			.orElseThrow(() -> new RuntimeException("Error: Role is not found."));
	        	   			roles.add(userRole);
	        	   			} }); }
	           
	   user.setRoles(roles);
	   pharmacieRepository.save(user);
	   
	   return ResponseEntity.ok(new MessageResponse("Pharmacie registered successfully!")); 
	   }
	  
	  
	  @PostMapping("/signupRadiologie") 
		public ResponseEntity<?> saveActeLabo(@RequestBody @Valid RadiologieRequest signUp){
	
		  if(userRepository.existsByUsername(signUp.getUsername())) { 
			  return ResponseEntity .badRequest() .body(new MessageResponse("Error: Username is already taken!")); }
		  if (userRepository.existsByEmail(signUp.getEmail())) {
			  return ResponseEntity .badRequest() .body(new MessageResponse("Error: Email is already in use!")); } // Create new user'saccount 
		  Radiologie user = new Radiologie();
		  
	  user.setUsername(signUp.getUsername());
	  user.setEmail(signUp.getEmail());
	  user.setMotDePasse(encoder.encode(signUp.getMotDePasse()));
	  user.setVille(signUp.getVille());
	  user.setTelephone(signUp.getTelephone());
	  user.setSexe(signUp.getSexe());
	  user.setPrenom(signUp.getPrenom());
	  user.setNom(signUp.getNom());
	  user.setRaisonSociale(signUp.getRaisonSociale());
	  user.setInpe(signUp.getInpe());
	  user.setIce(signUp.getIce()); 
	  user.setDateNaissance(signUp.getDateNaissance());
	  user.setCodePostal(signUp.getCodePostal());
	  user.setCin(signUp.getCin());
	  user.setAdresse(signUp.getAdresse());
	  user.setSituationFamiliale(signUp.getSituationFamiliale());
	  Set<String> strRoles =signUp.getRole();
	  Set<Role> roles = new HashSet<>(); 
	           if (strRoles == null) 
	           	{ Role userRole = roleRepository.findByName(ERole.ROLE_USER).orElseThrow(() -> new RuntimeException("Error: Role is not found.")); 
	           	roles.add(userRole); }
	           else { strRoles.forEach(role -> { 
	        	   switch (role) { 
	      	        	   	case "radiologie": Role modRole = roleRepository.findByName(ERole.ROLE_RADIOLOGIE) 
	        	   			.orElseThrow(() -> new RuntimeException("Error: Role is not found."));
	        	   			roles.add(modRole);
	        	   			break;
	        	   	default: Role userRole = roleRepository.findByName(ERole.ROLE_USER)
	        	   			.orElseThrow(() -> new RuntimeException("Error: Role is not found."));
	        	   			roles.add(userRole);
	        	   			} }); }
	           
	   user.setRoles(roles);
	   radiologieRepository.save(user);
	   
	   return ResponseEntity.ok(new MessageResponse("Radiologie registered successfully!")); 
	   }
	  
	  
	  
	  
	  @PostMapping("/signupAdmin") 
		public ResponseEntity<?> saveActeLabo(@RequestBody @Valid AdminRequest signUp){
		  
			if (AdminRepository.existsByUsername(signUp.getUsername())) {
				return ResponseEntity
						.badRequest()
						.body(new MessageResponse("Error: Username is already taken!"));
			}
			if (AdminRepository.existsByEmail(signUp.getEmail())) {
				return ResponseEntity
						.badRequest()
						.body(new MessageResponse("Error: Email is already in use!"));
			}
			
			// Create new user's account
			Admin user = new Admin();
			
			user.setUsername(signUp.getUsername());
			user.setEmail(signUp.getEmail());
			user.setMotDePasse(encoder.encode(signUp.getMotDePasse()));
			user.setVille(signUp.getVille());
			user.setTelephone(signUp.getTelephone());
			user.setSituationFamiliale(signUp.getSituationFamiliale());			
			user.setSexe(signUp.getSexe());
			user.setPrenom(signUp.getPrenom());
			user.setNom(signUp.getNom());						
			user.setDateNaissance(signUp.getDateNaissance());
			user.setCodePostal(signUp.getCodePostal());
			user.setCin(signUp.getCin());
			user.setAdresse(signUp.getAdresse());				
			Set<String> strRoles = signUp.getRole();
			Set<Role> roles = new HashSet<>();
			if (strRoles == null) {
				Role userRole = roleRepository.findByName(ERole.ROLE_USER)
						.orElseThrow(() -> new RuntimeException("Error: Role is not found."));
				roles.add(userRole);
			} else {
				strRoles.forEach(role -> {
					switch (role) {
						case "admin":
						Role modRole = roleRepository.findByName(ERole. ROLE_ADMIN)
								.orElseThrow(() -> new RuntimeException("Error: Role is not found."));
						roles.add(modRole);
						break;
					default:
						Role userRole = roleRepository.findByName(ERole.ROLE_USER)
								.orElseThrow(() -> new RuntimeException("Error: Role is not found."));
						roles.add(userRole);
					}
				});
				}
			user.setRoles(roles);
			AdminRepository.save(user);
		   // imageService.sauvegardeImageLabo(photoProfil, cachetSignature, CarteCin, carteProfessionnelle, user);
			return ResponseEntity.ok(new MessageResponse("labo registered successfully!"));
	  }
	
	  
	  
	  
	 // , @ModelAttribute  MultipartFile photoProfil, @ModelAttribute  MultipartFile cachetSignature, @ModelAttribute  MultipartFile CarteCin, @ModelAttribute  MultipartFile carteProfessionnelle
	  
  @PostMapping("/signuplabo")
  
//public ResponseEntity<?> registerLabo(@Valid @RequestParam("laboRequest") String signUp ) throws IOException {
		public ResponseEntity<?> saveActeLabo(@RequestBody @Valid LaboratoireRequest signUp){

	/*  ObjectMapper mapper = new ObjectMapper();
	  SignupRequestLabo signUpRequest = mapper.readValue(signUp, SignupRequestLabo.class);*/
		if (laboRepository.existsByUsername(signUp.getUsername())) {
			return ResponseEntity
					.badRequest()
					.body(new MessageResponse("Error: Username is already taken!"));
		}
		if (laboRepository.existsByEmail(signUp.getEmail())) {
			return ResponseEntity
					.badRequest()
					.body(new MessageResponse("Error: Email is already in use!"));
		}
		// Create new user's account
		Laboratoire user = new Laboratoire();
		user.setUsername(signUp.getUsername());
		user.setEmail(signUp.getEmail());
		user.setMotDePasse(encoder.encode(signUp.getMotDePasse()));
		user.setVille(signUp.getVille());
		user.setTelephone(signUp.getTelephone());
		user.setRaisonSociale(signUp.getRaisonSociale());
		user.setSituationFamiliale(signUp.getSituationFamiliale());
		user.setTypeMutuelle(signUp.getTypeMutuelle());
		user.setSexe(signUp.getSexe());
		user.setPrenom(signUp.getPrenom());
		user.setNom(signUp.getNom());
		user.setInpe(signUp.getInpe());
		user.setIce(signUp.getIce());
		user.setDateNaissance(signUp.getDateNaissance());
		user.setCodePostal(signUp.getCodePostal());
		user.setCin(signUp.getCin());
		user.setAdresse(signUp.getAdresse());
		
		Set<String> strRoles = signUp.getRole();
		Set<Role> roles = new HashSet<>();
		if (strRoles == null) {
			Role userRole = roleRepository.findByName(ERole.ROLE_USER)
					.orElseThrow(() -> new RuntimeException("Error: Role is not found."));
			roles.add(userRole);
		} else {
			strRoles.forEach(role -> {
				switch (role) {
					case "laboratoire":
					Role modRole = roleRepository.findByName(ERole.ROLE_LABORATOIRE)
							.orElseThrow(() -> new RuntimeException("Error: Role is not found."));
					roles.add(modRole);
					break;
				default:
					Role userRole = roleRepository.findByName(ERole.ROLE_USER)
							.orElseThrow(() -> new RuntimeException("Error: Role is not found."));
					roles.add(userRole);
				}
			});
		}
		user.setRoles(roles);
		laboRepository.save(user);
	   // imageService.sauvegardeImageLabo(photoProfil, cachetSignature, CarteCin, carteProfessionnelle, user);
		return ResponseEntity.ok(new MessageResponse("labo registered successfully!"));
	}
  
  @PostMapping("/signupAdherent")
  
	public ResponseEntity<?> saveActeLabo(@RequestBody @Valid AdherentRequest signUp){
	
		if (adherentRepository.existsByUsername(signUp.getUsername())) {
			return ResponseEntity
					.badRequest()
					.body(new MessageResponse("Error: Username is already taken!"));
		}
		if (adherentRepository.existsByEmail(signUp.getEmail())) {
			return ResponseEntity
					.badRequest()
					.body(new MessageResponse("Error: Email is already in use!"));
		}
		// Create new user's account
		Adherent user = new Adherent();
		user.setUsername(signUp.getUsername());
		user.setEmail(signUp.getEmail());
		user.setMotDePasse(encoder.encode(signUp.getMotDePasse()));
		user.setVille(signUp.getVille());
		user.setTelephone(signUp.getTelephone());
		user.setSituationFamiliale(signUp.getSituationFamiliale());
		user.setTypeMutuelle(signUp.getTypeMutuelle());
		user.setSexe(signUp.getSexe());
		user.setPrenom(signUp.getPrenom());
		user.setNom(signUp.getNom());
		user.setImmatriculation(signUp.getImmatriculation());
		user.setDateNaissance(signUp.getDateNaissance());
		user.setCodePostal(signUp.getCodePostal());
		user.setCin(signUp.getCin());
		user.setAdresse(signUp.getAdresse());	
		Set<String> strRoles = signUp.getRole();
		Set<Role> roles = new HashSet<>();
		if (strRoles == null) {
			Role userRole = roleRepository.findByName(ERole.ROLE_USER)
					.orElseThrow(() -> new RuntimeException("Error: Role is not found."));
			roles.add(userRole);
		} else {
			strRoles.forEach(role -> {
				switch (role) {
					case "adherent":
					Role modRole = roleRepository.findByName(ERole.ROLE_ADHERENT)
							.orElseThrow(() -> new RuntimeException("Error: Role is not found."));
					roles.add(modRole);
					break;
				default:
					Role userRole = roleRepository.findByName(ERole.ROLE_USER)
							.orElseThrow(() -> new RuntimeException("Error: Role is not found."));
					roles.add(userRole);
				}
			});
		}
		user.setRoles(roles);
		adherentRepository.save(user);
	   // imageService.sauvegardeImageLabo(photoProfil, cachetSignature, CarteCin, carteProfessionnelle, user);
		return ResponseEntity.ok(new MessageResponse("Adherent registered successfully!"));
	
}
  }
