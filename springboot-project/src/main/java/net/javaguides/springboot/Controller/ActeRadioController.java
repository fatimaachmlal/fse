package net.javaguides.springboot.Controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import net.javaguides.springboot.dto.ActeRadioRequest;
import net.javaguides.springboot.model.ActeLabo;
import net.javaguides.springboot.model.ActeMed;
import net.javaguides.springboot.model.ActeRadio;
import net.javaguides.springboot.repository.ActeRadioRepository;
import net.javaguides.springboot.service.ActeRadioService;
@CrossOrigin(origins = "http://localhost:4200")
@RestController
@RequestMapping("/api/acteradiologies")
public class ActeRadioController {

	@Autowired
	private ActeRadioRepository acteRadioRepository;
	
	ActeRadioService acteRadioService;

	public ActeRadioController(ActeRadioService acteRadioService) {
		super();
		this.acteRadioService = acteRadioService;
	}
	
	
	@PostMapping("AjouteActe")
	public ResponseEntity<ActeRadio> creerActeRadio(@RequestBody @Valid  ActeRadioRequest acteRadioRequest){
		
		return new ResponseEntity<ActeRadio>(acteRadioService.creerActeRadion(acteRadioRequest), HttpStatus.CREATED);
	}
	
	@PutMapping("modifieracte/{id}")
	public ResponseEntity<ActeRadio> updateActeRadio(@PathVariable("id") long id,@RequestBody ActeRadio acteRadio){
		return new ResponseEntity<ActeRadio>(acteRadioService.modificationActeRadio(acteRadio, id), HttpStatus.OK);
	}
	@GetMapping("visualiseractes")
	public List<ActeRadio> visualiser(){
	  return acteRadioService.visualiserActeRadio();
	}
	
	@DeleteMapping("/supprimeracte/{id}")
	public ResponseEntity<String> SupprimerActer(@PathVariable("id") long id){
		acteRadioService.supprimerActeRadio(id);
		return new ResponseEntity<String>("acteRadiologie deleted successfully!.", HttpStatus.OK);
	}
	

	@GetMapping("rechercherActe/{id}")
	public ResponseEntity<ActeRadio> rechercheActeParId(@PathVariable("id") long acteRadioId){
		return new ResponseEntity<ActeRadio>(acteRadioService.rechercherActeRadiById(acteRadioId), HttpStatus.OK);
	}
	@GetMapping("/RechercherActeMedRadioByCin/{cin}")
	public List<ActeMed> getActeMedRadioByCin(@PathVariable("cin") String cin){
		return acteRadioService.findActeMedRAdioByCinAdherent(cin);
	}
	
	@GetMapping("/RechercherActeRadioWithInfoAdherent/{IdRadio}")
	public List<ActeRadio> getcteRadioWithInfoAdherent(@PathVariable("IdRadio") long IdRadio){
		return acteRadioService.findActeRadioAdherentinfo( IdRadio);
	}
	@GetMapping("/RechercheMedInfPourActRadio/{IdActRadio}")
	public  List<ActeRadio> getMedInfPourActRadio(@PathVariable("IdActRadio") long IdActRadio){
		return acteRadioService.findActeRadioMedinfoByIdActRadio(IdActRadio);
	}
}
