package net.javaguides.springboot.Controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import net.javaguides.springboot.dto.ActePharRequest;
import net.javaguides.springboot.model.ActeMed;
import net.javaguides.springboot.model.ActePhar;
import net.javaguides.springboot.service.ActePharService;



@CrossOrigin(origins = "*", allowedHeaders = "*")
@RestController
@RequestMapping("/api/actepharmacies")
public class ActePharController {
	private ActePharService actePharService;

	public ActePharController(ActePharService actePharService) {
		super();
		this.actePharService = actePharService;
	}
//	@PreAuthorize("hasRole('USER') or hasRole('PHRMACIE')")	

	@PostMapping("creeracte")
	public ResponseEntity<ActePhar> saveActePhar(@RequestBody  @Valid  ActePharRequest actePharRequest){
		return new ResponseEntity<ActePhar>(actePharService.saveActePhar(actePharRequest), HttpStatus.CREATED);
	}
	
	@CrossOrigin(origins = "http://localhost:4200")
	@PutMapping("modifieractephar/{id}")
	public ResponseEntity<ActePhar> updateActePhar(@PathVariable("id") long id,@RequestBody ActePhar actePhar){
		return new ResponseEntity<ActePhar>(actePharService.modifierActePhar(actePhar, id), HttpStatus.OK);
	}
	
	@CrossOrigin(origins = "http://localhost:4200")
	@GetMapping("visualiseractesphar")
	public List<ActePhar> visualiser(){
		return actePharService.visualiserActesPhar();
	}
	
	
	@DeleteMapping("/supprimeractesphar/{id}")
	public ResponseEntity<String> SupprimerActePhar(@PathVariable("id") long id){
		actePharService.supprimerActePhar(id);
		return new ResponseEntity<String>("actePharmacie deleted successfully!.", HttpStatus.OK);
	}
	
	@CrossOrigin(origins = "http://localhost:4200")
	@GetMapping("rechercherActe/{id}")
	public ResponseEntity<ActePhar> rechercheActeParId(@PathVariable("id") long actePharId){
		return new ResponseEntity<ActePhar>(actePharService.rechercherActePharById(actePharId), HttpStatus.OK);
	}
	
	@PreAuthorize("hasRole('PHRMACIE')")	
	@GetMapping("/RechercherActeMedPharmaByCin/{cin}")
	public List<ActeMed> getActeMedPharmaByCin(@PathVariable("cin") String cin){
		return actePharService.findActeMedPharmaByCinAdherent(cin);
	}
	@PreAuthorize("hasRole('PHRMACIE')")	
	@GetMapping("/RechercherActePharmaWithInfoAdherent/{IdPharma}")
	public List<ActePhar> getctePharmaWithInfoAdherent(@PathVariable("IdPharma") long IdPharma){
		return actePharService.findActePharmaAdherentinfo( IdPharma);
	}

	@PreAuthorize("hasRole('PHRMACIE')")	
	@GetMapping("/RechercheMedInfPourActPharma/{IdActPharma}")
	public  List<ActePhar> getMedInfPourActPharma(@PathVariable("IdActPharma") long IdActPharma){
		return actePharService.findActePharmaMedinfoByIdActPharma(IdActPharma);
	}
	

}
