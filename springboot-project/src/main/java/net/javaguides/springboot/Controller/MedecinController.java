package net.javaguides.springboot.Controller;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import com.fasterxml.jackson.databind.ObjectMapper;

import net.javaguides.springboot.dto.MedecinRequest;
import net.javaguides.springboot.message.ResponseFile;
import net.javaguides.springboot.model.Medecin;
import net.javaguides.springboot.model.Sexe;
import net.javaguides.springboot.model.SituationFamiliale;
import net.javaguides.springboot.payload.response.ProfilResponse;
import net.javaguides.springboot.repository.MedecinRepository;
import net.javaguides.springboot.repository.RoleRepository;
import net.javaguides.springboot.security.jwt.JwtUtils;
import net.javaguides.springboot.service.MedecinService;

import java.io.File;
import java.io.IOException;
import java.lang.annotation.Repeatable;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.crypto.password.PasswordEncoder;

/*import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
*/

@RestController
@RequestMapping("/api/medecins")
@CrossOrigin(origins = "*")
public class MedecinController {
	private byte[] bytes;
	@ExceptionHandler({Exception.class})
	public void resolveException(Exception e) {
	    e.printStackTrace();
	}

	 @Autowired
	  AuthenticationManager authenticationManager;
	  
	  @Autowired
	  MedecinRepository medecinRepository;
	  
	  @Autowired
	  RoleRepository roleRepository;

	  @Autowired
	  PasswordEncoder encoder;

	  @Autowired
	  JwtUtils jwtUtils;
	  
	@Autowired
	private MedecinService medecinService;
	
	
	public MedecinController(MedecinService medecinService) {
		super();
		this.medecinService = medecinService;
	}
	
	/*
	 * @Autowired private MedecinRepository medecinRepository;
	 */
	
//	@CrossOrigin(origins = "http://localhost:4200/inscriptionmedecin")
	@PostMapping(path="inscription", produces = "application/json")
	@PreAuthorize("hasRole('USER') or hasRole('MODERATOR') or hasRole('ADMIN')")	
	public ResponseEntity<String> saveMedecin(@RequestParam("medecinRequest") String medecinRequest, @ModelAttribute  MultipartFile photoProfil, @ModelAttribute  MultipartFile cachetSignature, @ModelAttribute  MultipartFile CarteCin, @ModelAttribute  MultipartFile carteProfessionnelle ) throws IOException{
		//medecinRequest.setInpe("123456789");
		ObjectMapper mapper = new ObjectMapper();
	    Medecin medecinobject = mapper.readValue(medecinRequest, Medecin.class);
		//System.out.println("INPe"+medecinobject.getInpe());
		medecinRepository.save(medecinobject);
		return new ResponseEntity<String>("les donnes sont la", HttpStatus.OK);
	}
/*	@CrossOrigin(origins = "*", allowedHeaders = "*")
	@GetMapping("recherchePharmacieProfilComplet/{id}")
	@PreAuthorize("hasRole('USER') or hasRole('MODERATOR') or hasRole('ADMIN')")
	public List<Medecin> recherchePharmacieProfilComplet(@PathVariable("id") long id){
		return medecinService.findProfilMedecinComplet(id);
		
		
	}
	
	@CrossOrigin(origins = "*")
	  @PostMapping(path="addmed" ,consumes = MediaType.MULTIPART_FORM_DATA_VALUE)
	    public void saveProduct( @RequestParam MultipartFile fileMultipartFile,@RequestParam String nom, @RequestParam String prenom,@RequestParam Date dateNaissance,@RequestParam String cin,@RequestParam String email,
	    		@RequestParam String telephone,@RequestParam String motDePasse,@RequestParam String adresse,@RequestParam String ville,@RequestParam String codePostal, @RequestParam Sexe sexe,@RequestParam SituationFamiliale situationFamiliale, 
	    		@RequestParam File photoProfile,@RequestParam File fichierCin,@RequestParam String cachet,
	    		@RequestParam String specialite,@RequestParam String inpe,@RequestParam String ice, @RequestParam File cartePro,@RequestParam String typeMutuelle)
	    {
	    	medecinService.saveMed(fileMultipartFile, nom, prenom, dateNaissance, cin, email, telephone, motDePasse, adresse, ville, codePostal, sexe, situationFamiliale, photoProfile, fichierCin, cachet, specialite, inpe, ice, cartePro, typeMutuelle);
	    }*/
	@PutMapping("/modifierinfos/{id}")
	public ResponseEntity<Medecin> updateMedecin(@PathVariable("id") long id,@RequestBody Medecin medecin){
		return new ResponseEntity<Medecin>(medecinService.updateMedecin(medecin, id), HttpStatus.OK);
	}
	
	@DeleteMapping("/supprimermedecin/{id}")
	public ResponseEntity<String> deleteMedecin(@PathVariable("id") long id){
		
		medecinService.deleteMedecin(id);
		
		return new ResponseEntity<String>("Medecin deleted successfully!.", HttpStatus.OK);
	}
	
	@GetMapping("/recherchermedecins")
	public List<Medecin> getAllMedecins(){
		return medecinService.getAllMedecins();
	}
	@GetMapping("/CountMedecin/{IdAmin}")
	public long getCountMedecin(@PathVariable("IdAmin") long IdAmin){
		return medecinService.countMedecin(IdAmin);
	}
	
	
	
	@GetMapping("/recherchermedecin/{id}")
	//@PreAuthorize("hasRole('USER') or hasRole('MODERATOR') or hasRole('ADMIN')")
	public ResponseEntity<Medecin> getMedecinById(@PathVariable("id") long medecinId){
		return new ResponseEntity<Medecin>(medecinService.RecherchMedecinById(medecinId), HttpStatus.OK);
	}
	
	// @CrossOrigin(origins = "*", allowedHeaders = "*")
	  @PostMapping("/upload")
	  public ResponseEntity<String> uploadFile(@RequestParam("file") MultipartFile file) {
	    String message = "";
	    try {
	    	
	     // medecinService.store(file);
	      message = "Uploaded the file successfully: " + file.getOriginalFilename();
	      return ResponseEntity.status(HttpStatus.OK).body("ok");
	    } catch (Exception e) {
	      message = "Could not upload the file: " + file.getOriginalFilename() + "!";
	      return ResponseEntity.status(HttpStatus.EXPECTATION_FAILED).body("ko");
	    }
	  }

	  
		@PostMapping(path="/add" ,consumes = MediaType.MULTIPART_FORM_DATA_VALUE)
	    public void saveMed(@RequestParam("file") MultipartFile file,
	    		@RequestParam String nom,
	    		
	    		@RequestParam String prenom,@RequestParam String inpe)
	    {
	    	medecinService.saveMedToDB(file, nom, prenom, inpe);
	    	
	    }
		@PostMapping("/uploadd")
		public void uploadImage(@RequestParam("imageFile") MultipartFile file) throws IOException {
			this.bytes = file.getBytes();
		}

		@PostMapping("/addd")
		public void createBook(@RequestBody Medecin medecin) throws IOException {
			medecinRepository.save(medecin);
			this.bytes = null;
		}
		/*@GetMapping("/test105/{id}")
		public ProfilResponse test(@PathVariable("id") long medecinId) {
			ProfilResponse profilmedecin=new ProfilResponse();
			return profilmedecin;
			
		}
		*/
		@GetMapping("/ProfilMedcin/{id}")
		public ProfilResponse ProfilMedcin(@PathVariable("id") long medecinId){
			Medecin medecin = new Medecin();
			ProfilResponse profilmedecin=new ProfilResponse();
			medecin=medecinService.getMedecinById(medecinId);
			profilmedecin.setId(medecin.getId());
			profilmedecin.setNom(medecin.getNom());
			profilmedecin.setInpe(medecin.getInpe());	
			profilmedecin.setPrenom(medecin.getPrenom());
			profilmedecin.setSituationFamiliale(medecin.getSituationFamiliale());
			profilmedecin.setSexe(medecin.getSexe());
			profilmedecin.setVille(medecin.getVille());
			profilmedecin.setAdresse(medecin.getAdresse());
			profilmedecin.setTelephone(medecin.getTelephone());
			profilmedecin.setEmail(medecin.getEmail());
			profilmedecin.setCin(medecin.getCin());
			profilmedecin.setDateNaissance(medecin.getDateNaissance());
			profilmedecin.setSpecialite(medecin.getSpecialite());
			profilmedecin.setTypeMutuelle(medecin.getTypeMutuelle());
			profilmedecin.setIce(medecin.getIce());
			profilmedecin.setCodePostal(medecin.getCodePostal());

			
			
			
			return profilmedecin;
		}
		
		
		
	//traiter l'inscription
	/*
	 * @PostMapping("/process_register") public void processRegister(Medecin
	 * medecin) { BCryptPasswordEncoder passwordEncoder = new
	 * BCryptPasswordEncoder(); String encodedPassword =
	 * passwordEncoder.encode(medecin.getMotDePasse());
	 * medecin.setMotDePasse(encodedPassword);
	 * 
	 * medecinRepository.save(medecin); }
	 */
	
}
