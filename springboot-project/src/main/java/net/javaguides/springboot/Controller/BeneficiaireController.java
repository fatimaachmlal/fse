package net.javaguides.springboot.Controller;


import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import net.javaguides.springboot.dto.BeneficiaireRequest;

import net.javaguides.springboot.model.Beneficiaire;
import net.javaguides.springboot.model.Fse;
import net.javaguides.springboot.repository.BeneficiaireRepository;
import net.javaguides.springboot.service.BeneficiaireService;
@CrossOrigin(origins="*")
@RestController
@RequestMapping("/api/beneficiaires")
public class BeneficiaireController {
	
	@Autowired
	private BeneficiaireService beneficiaireService;
	@Autowired
	private BeneficiaireRepository beneficiaireRepository;

    
	public BeneficiaireController(BeneficiaireService beneficiaireService) {
		super();
		this.beneficiaireService = beneficiaireService;
	}
	
	@GetMapping("/Ok")
	public int ok() {
		return 0;
	}
	// build create beneficiaire REST API
	@PostMapping("/ajouterAyantDroit")
	public ResponseEntity<Beneficiaire> ajouterBeneficiaire(@RequestBody @Valid BeneficiaireRequest beneficiaireRequest){
		return new ResponseEntity<Beneficiaire>(beneficiaireService.saveBeneficiaire(beneficiaireRequest), HttpStatus.CREATED);
	}
	
	// build get all beneficiaires REST API
	@GetMapping("/chercherAyantDroits")
	public List<Beneficiaire> chercherAllBeneficiaires(){
		return beneficiaireService.getAllBeneficiaires();
	}
	
	// build get beneficiaire by id REST API
	// http://localhost:8080/api/beneficiaires/1
	@GetMapping("/chercherAyantDroit/{id}")
	public ResponseEntity<Beneficiaire> getBeneficiaireById(@PathVariable("id") long beneficiaireId){
		return new ResponseEntity<Beneficiaire>(beneficiaireService.getBeneficiaireById(beneficiaireId), HttpStatus.OK);  
	}
	
	// build update beneficiaire REST API
	// http://localhost:8080/api/beneficiaires/1
	@PutMapping("/modifierAyantDroit/{id}")
	public ResponseEntity<Beneficiaire> modifierBeneficiaire(@PathVariable("id") long id
												  ,@RequestBody Beneficiaire beneficiaire){
		return new ResponseEntity<Beneficiaire>(beneficiaireService.updateBeneficiaire(beneficiaire, id), HttpStatus.OK);
	}
	
	// build delete beneficiaire REST API
	// http://localhost:8080/api/beneficiaires/1
	@DeleteMapping("/supprimerAyantDroit/{id}")
	public ResponseEntity<String> supprimerBeneficiaire(@PathVariable("id") long id){
		
		// delete beneficiaire from DB
		beneficiaireService.deleteBeneficiaire(id);
		
		return new ResponseEntity<String>("Beneficiaire deleted successfully!.", HttpStatus.OK);
	}
	@GetMapping("/RechercherBeneficiaireByAdherent/{id_adherent}")
	public ResponseEntity<List<Beneficiaire>> getBeneficiaireByAdherent(@PathVariable("id_adherent") long id_adherent ){
		return new ResponseEntity<List<Beneficiaire>>(beneficiaireService.findBeneficiaireByAdherent(id_adherent),HttpStatus.OK);
	}
	@GetMapping("/RechercherBeneficiaireByAdherentCIN/{cin}")
	public ResponseEntity<List<Beneficiaire>> getBeneficiaireByAdherentCIN(@PathVariable("cin") String cin ){
		return new ResponseEntity<List<Beneficiaire>>(beneficiaireService.findBeneficiaireByAdherentCIN(cin),HttpStatus.OK);
	}
	
	
	
	
}

