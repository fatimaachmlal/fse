package net.javaguides.springboot.Controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import net.javaguides.springboot.dto.LaboratoireRequest;
import net.javaguides.springboot.model.Laboratoire;
import net.javaguides.springboot.model.Radiologie;
import net.javaguides.springboot.payload.response.ProfilResponse;
import net.javaguides.springboot.repository.MedecinRepository;
import net.javaguides.springboot.repository.RoleRepository;
import net.javaguides.springboot.security.jwt.JwtUtils;
import net.javaguides.springboot.service.LaboratoireService;

@RestController
@RequestMapping("/api/laboratoires")
@CrossOrigin(origins = "http://localhost:4200")
public class LaboratoireController {
	private LaboratoireService laboratoireService;
	
	public LaboratoireController(LaboratoireService laboratoireService) {
		super();
		this.laboratoireService = laboratoireService;
	}
	
	@PostMapping("inscription")
	//@Valid
	public ResponseEntity<Laboratoire> saveLaboratoire(@RequestBody  LaboratoireRequest laboratoireRequest){
		
		return new ResponseEntity<Laboratoire>(laboratoireService.saveLaboratoire(laboratoireRequest), HttpStatus.CREATED);

	}
	
	@PutMapping("modifierinfos/{id}")
	public ResponseEntity<Laboratoire> updateLaboratoire(@PathVariable("id") long id,@RequestBody Laboratoire laboratoire){
		return new ResponseEntity<Laboratoire>(laboratoireService.updateLaboratoire(laboratoire, id), HttpStatus.OK);
	}
	@DeleteMapping("supprimerLaboratoire/{id}")
	public ResponseEntity<String> deleteLaboratoire(@PathVariable("id") long id){
		
		laboratoireService.deleteLaboratoire(id);
		
		return new ResponseEntity<String>("Laboratoire deleted successfully!.", HttpStatus.OK);
	}
	
	@GetMapping("/rechercherlaboratoires")
	public List<Laboratoire> getAllLaboratoires(){
		return laboratoireService.getAllLaboratoires();
		
	}
	
	@GetMapping("/CountLaboratoire")
	public long getCountLabo(){
		return laboratoireService.countLaboratoire();
	}
	
	
	@GetMapping("rechercherlaboratoire/{id}")
	public ResponseEntity<Laboratoire> getLaboratoireById(@PathVariable("id") long laboratoireId){
		return new ResponseEntity<Laboratoire>(laboratoireService.getLaboratoireById(laboratoireId), HttpStatus.OK);
	}
	
	@GetMapping("Profillaboratoire/{id}")
	public ProfilResponse Profillaboratoire(@PathVariable("id") long laboratoireId){
		Laboratoire laboratoire= new Laboratoire();
		ProfilResponse profilLaboratoire=new ProfilResponse();
		laboratoire=laboratoireService.getLaboratoireById(laboratoireId);
		profilLaboratoire.setId(laboratoire.getId());
		profilLaboratoire.setNom(laboratoire.getNom());
		profilLaboratoire.setInpe(laboratoire.getInpe());
		
		
		
		profilLaboratoire.setId(laboratoire.getId());
		profilLaboratoire.setNom(laboratoire.getNom());
		profilLaboratoire.setInpe(laboratoire.getInpe());
		profilLaboratoire.setPrenom(laboratoire.getPrenom());
		profilLaboratoire.setSituationFamiliale(laboratoire.getSituationFamiliale());
		profilLaboratoire.setSexe(laboratoire.getSexe());
		profilLaboratoire.setVille(laboratoire.getVille());
		profilLaboratoire.setAdresse(laboratoire.getAdresse());
		profilLaboratoire.setTelephone(laboratoire.getTelephone());
		profilLaboratoire.setEmail(laboratoire.getEmail());
		profilLaboratoire.setCin(laboratoire.getCin());
		profilLaboratoire.setDateNaissance(laboratoire.getDateNaissance());
		profilLaboratoire.setRaisonSociale(laboratoire.getRaisonSociale());
		profilLaboratoire.setIce(laboratoire.getIce());
		
		
		return profilLaboratoire;
	}
}
