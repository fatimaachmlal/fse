package net.javaguides.springboot.Controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import net.javaguides.springboot.dto.ActeLaboRequest;
import net.javaguides.springboot.model.ActeLabo;
import net.javaguides.springboot.model.ActeMed;
import net.javaguides.springboot.service.ActeLaboService;
@CrossOrigin(origins = "http://localhost:4200")
@RestController
@RequestMapping("/api/acteslaboratoires")
public class ActeLaboController {

	private ActeLaboService acteLaboService;

	public ActeLaboController(ActeLaboService acteLaboService) {
		super();
		this.acteLaboService = acteLaboService;
	}
	@PostMapping("/ajouteractelabo")
	public ResponseEntity<ActeLabo> saveActeLabo(@RequestBody @Valid ActeLaboRequest acteLaboRequest){
		return new ResponseEntity<ActeLabo>(acteLaboService.saveActeLabo(acteLaboRequest),HttpStatus.OK);
	}
	@PutMapping("modifieractelabo/{id}")
	public ResponseEntity<ActeLabo> updateActeLabo(@PathVariable("id") long id,@RequestBody ActeLabo acteLabo){
		return new ResponseEntity<ActeLabo>(acteLaboService.updateActeLabo(acteLabo, id),HttpStatus.OK);
	}
	@GetMapping("rechercheracteslabo")
	public List<ActeLabo> getAllActesLabo(){
		
		return acteLaboService.getAllActesLabo();
	}
	@GetMapping("rechercheractelabo/{id}")
	public ResponseEntity<ActeLabo> getActeLaboById(@PathVariable("id") long acteLaboId){
		return new ResponseEntity<ActeLabo>(acteLaboService.getActeLaboById(acteLaboId),HttpStatus.OK);
	}
	@DeleteMapping("supprimeractelabo/{id}")
	public ResponseEntity<String> deleteActeLabo(@PathVariable("id") long id) {
		acteLaboService.deleteActeLabo(id);
		return new ResponseEntity<String>("Acte Laboratoire deleted successfully!.", HttpStatus.OK);
	}
	@GetMapping("/RechercherActeMedLaboByCin/{cin}")
	public List<ActeMed> getActeMedLaboByCin(@PathVariable("cin") String cin){
		return acteLaboService.findActeMedLaboByCinAdherent(cin);
	}
	@GetMapping("/RechercherActeLaboWithInfoAdherent/{IdLabo}")
	public List<ActeLabo> getcteLaboWithInfoAdherent(@PathVariable("IdLabo") long IdLabo){
		return acteLaboService.findActeloboAdherentinfo( IdLabo);
	}
	@GetMapping("/RechercheMedInfPourActLabo/{IdActLabo}")
	public  List<ActeLabo> getMedInfPourActLabo(@PathVariable("IdActLabo") long IdActLabo){
		return acteLaboService.findActeloboMedinfoByIdActLabo( IdActLabo);
	}
}
