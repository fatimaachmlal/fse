package net.javaguides.springboot.Controller;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import net.javaguides.springboot.dto.AssurenceComplementaireRequest;
import net.javaguides.springboot.model.AssurenceComplementaire;
import net.javaguides.springboot.service.assurenceComplementaireService;

@CrossOrigin(origins="*")
@RestController
@RequestMapping("/api/assurenceComplementaire")
public class AssurenceComplementaireController {
	
	@Autowired
	private assurenceComplementaireService AssurenceComplementaireService;
	
	@PostMapping("/ajouterassurenceComplementaire")
	public ResponseEntity<AssurenceComplementaire> ajouterAssurenceComplementaire(@RequestBody @Valid AssurenceComplementaireRequest assurenceComplementaireRequest){		
		return new ResponseEntity<AssurenceComplementaire>(AssurenceComplementaireService.SaveAssurenceComplementaire(assurenceComplementaireRequest), HttpStatus.CREATED);
	}

}
