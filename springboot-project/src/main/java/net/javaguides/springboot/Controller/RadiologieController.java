package net.javaguides.springboot.Controller;

import java.util.List;
import java.util.Optional;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

import net.javaguides.springboot.dto.RadiologieRequest;
import net.javaguides.springboot.model.Adherent;
import net.javaguides.springboot.model.Pharmacie;
import net.javaguides.springboot.model.Radiologie;
import net.javaguides.springboot.payload.response.ProfilResponse;
import net.javaguides.springboot.repository.RadiologieRepository;
import net.javaguides.springboot.service.RadiologieService;

@RestController
@RequestMapping("/api/radiologies")
@CrossOrigin(origins = "*")

public class RadiologieController {
	@Autowired
	private RadiologieService radiologieService;
	
	@Autowired
	private RadiologieRepository radiologieRepository;

	public RadiologieController(RadiologieService radiologieService) {
		super();
		this.radiologieService = radiologieService;
	}
	
	
	
	@PostMapping("/inscription")
	public ResponseEntity<Radiologie> saveRadiologie(@RequestBody  @Valid RadiologieRequest radiologieRequest){
		return new ResponseEntity<Radiologie>(radiologieService.savesaveRadiologie(radiologieRequest), HttpStatus.CREATED);
	}
	
	@PutMapping("/modifierinfos/{id}")
	public ResponseEntity<Radiologie> updateRadiologie(@PathVariable("id") long id,@RequestBody Radiologie radiologie){
		return new ResponseEntity<Radiologie>(radiologieService.modifierRadiologie(radiologie, id), HttpStatus.OK);
	}
	@GetMapping("/visualiserradiologie")
	public List<Radiologie> visualiser(){
		return radiologieService.getAllRadiologie();
		
	}
	@GetMapping("/CountRadiologies/{IdAmin}")
	public long getCountRadio(@PathVariable("IdAmin") long IdAmin){
		return radiologieService.countRadiologie(IdAmin);
	}
	
	
	
	@DeleteMapping("/supprimerRadiologie/{id}")
	public ResponseEntity<String> SupprimerRadiologie(@PathVariable("id") long id){
		radiologieService.supprimerRadiologie(id);
		return new ResponseEntity<String>("Radiologie deleted successfully!.", HttpStatus.OK);
	}
	
	@GetMapping("/rechercheRadiologies")
	public List<Radiologie> getAllRadiologie(){
		return radiologieService.getAllRadiologie();
	}	
	@GetMapping("/rechercherRadiologie/{id}")
	public Optional<Radiologie> rechercheRadiologieeParId(@PathVariable("id") long radiologieId){
	//	return new ResponseEntity<Optional<Radiologie> >(radiologieService.rechercherRadiologieId(radiologieId), HttpStatus.OK);
		 Optional<Radiologie> radiologie = radiologieRepository.findById(radiologieId);
		    if (radiologie.isEmpty()) {
		        throw new ResponseStatusException(HttpStatus.NOT_FOUND, String.format("User with id %d not found", radiologieId));
		    }

		return radiologie;
	}
	
	@GetMapping("/ProfilRadiologie/{id}")
	public ProfilResponse ProfilRadiologie(@PathVariable("id") long Radiologie){
		Radiologie radiologie= new Radiologie();
		ProfilResponse profilRadiologiee=new ProfilResponse();
		radiologie=radiologieService.rechercherRadiologieId(Radiologie);

		profilRadiologiee.setId(radiologie.getId());
		profilRadiologiee.setNom(radiologie.getNom());
		profilRadiologiee.setInpe(radiologie.getInpe());
		profilRadiologiee.setPrenom(radiologie.getPrenom());
		profilRadiologiee.setSituationFamiliale(radiologie.getSituationFamiliale());
		profilRadiologiee.setSexe(radiologie.getSexe());
		profilRadiologiee.setVille(radiologie.getVille());
		profilRadiologiee.setAdresse(radiologie.getAdresse());
		profilRadiologiee.setTelephone(radiologie.getTelephone());
		profilRadiologiee.setEmail(radiologie.getEmail());
		profilRadiologiee.setCin(radiologie.getCin());
		profilRadiologiee.setDateNaissance(radiologie.getDateNaissance());
		profilRadiologiee.setRaisonSociale(radiologie.getRaisonSociale());
		profilRadiologiee.setIce(radiologie.getIce());
		return profilRadiologiee;
	}

}
