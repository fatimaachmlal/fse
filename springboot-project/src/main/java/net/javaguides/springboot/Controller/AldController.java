package net.javaguides.springboot.Controller;



import java.util.List;

import javax.validation.Valid;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


import net.javaguides.springboot.dto.AldRequest;

import net.javaguides.springboot.model.Ald;
import net.javaguides.springboot.model.Beneficiaire;
import net.javaguides.springboot.repository.AldRepository;
import net.javaguides.springboot.service.AldService;

@RestController
@RequestMapping("/api/alds")
@CrossOrigin(origins="*")
public class AldController {
	
    
	
	private AldService aldService;
    
	public AldController(AldService aldService) {
		super();
		this.aldService = aldService;
	}

	
	// build create ald REST API
		@PostMapping("/ajouterAld")
		public ResponseEntity<Ald> ajouterAld(@RequestBody @Valid AldRequest aldRequest){
			
			return new ResponseEntity<Ald>(aldService.saveAld(aldRequest), HttpStatus.CREATED);
		}
	
	// build get all ald REST API
	@GetMapping("/rechercheAllAlds")
	public List<Ald> rechercheAllAlds(){
		return aldService.getAllAlds();
	}
	
	// build get ald by id REST API
	// http://localhost:8080/api/alds/1
	@GetMapping("/rechercheAldById/{id}")
	public ResponseEntity<Ald> rechercheAldById(@PathVariable("id") long aldId){
		return new ResponseEntity<Ald>(aldService.getAldById(aldId), HttpStatus.OK);
	}
	
	// build update ald REST API
	// http://localhost:8080/api/alds/1
	@PutMapping("/modifierAld/{id}")
	public ResponseEntity<Ald> modifierAld(@PathVariable("id") long id ,@RequestBody Ald ald){
												  
		return new ResponseEntity<Ald>(aldService.updateAld(ald, id), HttpStatus.OK);
	}
	
	// build delete ald REST API
	// http://localhost:8080/api/alds/1
	@DeleteMapping("/supprimerAld/{id}")
	public ResponseEntity<String> supprimerAld(@PathVariable("id") long id){
		
		// delete ald from DB
		aldService.deleteAld(id);
		
		return new ResponseEntity<String>("Ald deleted successfully!.", HttpStatus.OK);
	}
	@GetMapping("/RechercherAldByAdherent/{id_adherent}")
	public ResponseEntity<List<Ald>> getBeneficiaireByAdherent(@PathVariable("id_adherent") long id_adherent ){
		return new ResponseEntity<List<Ald>>(aldService.findAldByAdherent(id_adherent),HttpStatus.OK);
	}
	
	
}


