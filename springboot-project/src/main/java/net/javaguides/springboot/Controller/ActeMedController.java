package net.javaguides.springboot.Controller;

import java.util.Date;
import java.util.List;

import javax.validation.Valid;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import net.javaguides.springboot.dto.ActeMedRequest;
import net.javaguides.springboot.model.ActeMed;
import net.javaguides.springboot.model.ActeRadio;
import net.javaguides.springboot.service.ActeMedService;
import net.javaguides.springboot.service.MedecinService;

@RestController
@RequestMapping("/api/actesmedecins")
@CrossOrigin(origins = "*", allowedHeaders = "*")

public class ActeMedController {

	private ActeMedService acteMedService;

	public ActeMedController(ActeMedService acteMedService) {
		super();
		this.acteMedService = acteMedService;
	}
	
	
	@PostMapping("/ajouteractemed")
	public ResponseEntity<ActeMed> saveActeMed(@RequestBody @Valid ActeMedRequest acteMedRequest){
		return new ResponseEntity<ActeMed>(acteMedService.saveActeMed(acteMedRequest),HttpStatus.OK);
		
	}
	@PutMapping("/modifieractemed/{id}")
	public ResponseEntity<ActeMed> updateActeMed(@PathVariable("id") long id,@RequestBody ActeMed acteMed){
		return new ResponseEntity<ActeMed>(acteMedService.updateActeMed(acteMed, id),HttpStatus.OK);
	}
	@GetMapping("/rechercheractesmed")
	public List<ActeMed> getAllActesMed(){
		return acteMedService.getAllActesMed();
	}
	@GetMapping("/rechercheracteByMed/{id}")
	public List<ActeMed> getActeMedByMedecin(@PathVariable("id") long id){
		
		return acteMedService.findActeMedByMedecin(id);
	}
	@GetMapping("/rechercheracteByCin/{cin}")
	public List<ActeMed> getActeMedByMedecin(@PathVariable("cin") String cin){
		return acteMedService.findActeMedByCinAdherent(cin);
	}
	@GetMapping("/RechercherActeMedecinWithInfoAdherent/{IdMedecin}")
	public List<ActeMed> getcteMedecinWithInfoAdherent(@PathVariable("IdMedecin") long IdMedecin){
		return acteMedService.findActeMedecinAdherentinfo( IdMedecin);
	}
	
	@GetMapping("/RechercheMedInfPourActMed/{IdMedecin}")
	public List<ActeMed> getMedInfPourActMed(@PathVariable("IdMedecin") long IdMedecin){
		return acteMedService.findActeMedecinMedinfoByIdActMed( IdMedecin);
	}
	@DeleteMapping("/supprimeractemed/{id}")
	public ResponseEntity<String> deleteActeMed(@PathVariable("id") long id) {
		acteMedService.deleteActeMed(id);
		return new ResponseEntity<String>("Acte Medecin deleted successfully!.", HttpStatus.OK);
	}

}

