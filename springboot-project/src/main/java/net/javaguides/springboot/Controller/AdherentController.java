package net.javaguides.springboot.Controller;

import java.util.List;

//import javax.validation.Valid;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
//import org.springframework.validation.annotation.Validated ;
//import net.javaguides.springboot.Controller.Valid;
import net.javaguides.springboot.dto.AdherentRequest;
import net.javaguides.springboot.model.Adherent;
import net.javaguides.springboot.model.Ald;
import net.javaguides.springboot.model.Beneficiaire;
import net.javaguides.springboot.model.Radiologie;
import net.javaguides.springboot.payload.response.AdherentResponse;
import net.javaguides.springboot.payload.response.ProfilResponse;
import net.javaguides.springboot.repository.AdherentRepository;
import net.javaguides.springboot.service.AdherentService;

import javax.validation.Valid;
@CrossOrigin(origins="*")
@RestController
@RequestMapping("/api/adherents")
public class AdherentController {
	
    
	private AdherentService adherentService;
	private AdherentRepository adherentRepository;

    
	public AdherentController(AdherentService adherentService) {
		super();
		this.adherentService = adherentService;
	}
	//@CrossOrigin(origins="http://localhost:4200/Adherant")
	@PostMapping("inscription")
	public ResponseEntity<Adherent> ajouterAyantDroit(@RequestBody @Valid AdherentRequest adherentRequest){
		return new ResponseEntity<Adherent>(adherentService.saveAdherent(adherentRequest), HttpStatus.CREATED);

	}
	
	// build create adherent REST API
	//@PostMapping("/inscription")
	//public ResponseEntity<Adherent> ajouterAyantDroit(@RequestBody Adherent adherent){
	//	return new ResponseEntity<Adherent>(adherentService.saveAdherent(adherent), HttpStatus.CREATED);
	//}
	
	// build get all adherents REST API
	@GetMapping("/rechercheAdherents")
	public List<Adherent> getAllAdherents(){
		return adherentService.getAllAdherents();
	}	
	
	@GetMapping("/CountAdherent/{IdAmin}")
	public long getCountAdherent(@PathVariable("IdAmin") long IdAmin){
		return adherentService.countAdherent(IdAmin);
	}
	
	
	
	// build get adherent by id REST API
	// http://localhost:8080/api/adherents/1
	@GetMapping("/rechercheAdherentById/{id}")
	public ResponseEntity<Adherent> rechercheAdherentById(@PathVariable("id") long adherentId){
		return new ResponseEntity<Adherent>(adherentService.getAdherentById(adherentId), HttpStatus.OK);
	}
	
	// build update adherent REST API
	// http://localhost:8080/api/adherents/1
	@PutMapping("/modifierAdherent/{id}")
	public ResponseEntity<Adherent> modifierAdherent(@PathVariable("id") long id
												  ,@RequestBody Adherent adherent){
		return new ResponseEntity<Adherent>(adherentService.updateAdherent(adherent, id), HttpStatus.OK);
	}
	
	// build delete adherent REST API
	// http://localhost:8080/api/adherents/1
	@DeleteMapping("/supprimerAdherent/{id}")
	public ResponseEntity<String> supprimerAdherent(@PathVariable("id") long id){
		
		// delete adherent from DB
		adherentService.deleteAdherent(id);
		
		return new ResponseEntity<String>("Adherent deleted successfully!.", HttpStatus.OK);
	}
	@GetMapping("/rechercheAdherentByCin/{Cin}")
	public AdherentResponse rechercheAdherentByCin( @PathVariable("Cin") String Cin){
		Adherent adhrent = new Adherent() ;
		adhrent =adherentService.getAdherentByCin(Cin);		
		adhrent.getCin();
		AdherentResponse adherentResponse =new AdherentResponse();
		adherentResponse.setId(adhrent.getId());
		adherentResponse.setNon(adhrent.getNom());
		adherentResponse.setPrenom(adhrent.getPrenom());
		adherentResponse.setTelphone(adhrent.getTelephone());
		adherentResponse.setCin(adhrent.getCin());
		adherentResponse.setVille(adhrent.getVille());
		adherentResponse.setDateNaissance(adhrent.getDateNaissance());
		adherentResponse.setImmatriculation(adhrent.getImmatriculation());
		adherentResponse.setTypeMutuelle(adhrent.getTypeMutuelle());

		
		 
	
		return adherentResponse;
	}
	@GetMapping("ProfilAdherent/{id}")
	public ProfilResponse ProfilAdherent(@PathVariable("id") long adhrentId){
		Adherent adhrent = new Adherent() ;
		ProfilResponse profilAdhrent=new ProfilResponse();
		adhrent=adherentService.getAdherentById(adhrentId);
		profilAdhrent.setId(adhrent.getId());
		profilAdhrent.setNom(adhrent.getNom());		
		profilAdhrent.setPrenom(adhrent.getPrenom());
		profilAdhrent.setSituationFamiliale(adhrent.getSituationFamiliale());
		profilAdhrent.setSexe(adhrent.getSexe());
		profilAdhrent.setVille(adhrent.getVille());
		profilAdhrent.setAdresse(adhrent.getAdresse());
		profilAdhrent.setTelephone(adhrent.getTelephone());
		profilAdhrent.setEmail(adhrent.getEmail());
		profilAdhrent.setCin(adhrent.getCin());
		profilAdhrent.setDateNaissance(adhrent.getDateNaissance());
		profilAdhrent.setTypeMutuelle(adhrent.getTypeMutuelle());
		profilAdhrent.setImmatriculation(adhrent.getImmatriculation());
		profilAdhrent.setCodePostal(adhrent.getCodePostal());
		return profilAdhrent;
	}
	
	
	
	
}

