package net.javaguides.springboot.message;
public class ResponseFile {
	  private String name;
	  private String url;
	  private String url1;
	  private String url2;
	  private String type;
	  private long size;
	  public ResponseFile(String name, String url,  String url1, String url2,String type, long size) {
	    this.name = name;
	    this.url = url;
	    this.url1 = url1;
	    this.url2 = url2;
	    this.type = type;
	    this.size = size;
	  }
	  public String getName() {
	    return name;
	  }
	  public void setName(String name) {
	    this.name = name;
	  }
	  public String getUrl() {
	    return url;
	  }
	  public void setUrl(String url) {
	    this.url = url;
	  }
	  public String getType() {
	    return type;
	  }
	  public void setType(String type) {
	    this.type = type;
	  }
	  public long getSize() {
	    return size;
	  }
	  public void setSize(long size) {
	    this.size = size;
	  }
	public String getUrl2() {
		return url2;
	}
	public void setUrl2(String url2) {
		this.url2 = url2;
	}
	public String getUrl1() {
		return url1;
	}
	public void setUrl1(String url1) {
		this.url1 = url1;
	}
	}