package net.javaguides.springboot.model;

import java.util.Date;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Table(name="acte_labo")
@Data
@NoArgsConstructor 
@AllArgsConstructor
public class ActeLabo {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long id;
	
	@Column(name = "code_acte")
	private String codeActe;
	
	@Column(name = "lettre_cle")
	private String lettreCle;
	
	@Column(name = "prix")
	private double prix;
	
	@Column(name = "date_acte")
	@JsonFormat(pattern="yyyy-MM-dd")
	private Date dateActe;
	
	@Column(name = "ngap")
	private String ngap; 

	
	@ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "laboratoire_id", referencedColumnName = "id")
    @JsonIgnoreProperties("actes")
	private Laboratoire laboratoire;
	
	@ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "Adherent_id", referencedColumnName = "id")
    @JsonIgnoreProperties("actes")
	private Adherent adherent;
	
	@ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "Medecin_id", referencedColumnName = "id")
    @JsonIgnoreProperties("actes")
	private Medecin medecin;

    @Transient 
	protected String nom;

    @Transient 
	protected String prenom;
    @Transient 
	protected String telephone;
    @Transient 
   	protected String cin; 
    @Transient 
   	protected long numeroDossier;

	


	public ActeLabo(long id, String codeActe, String lettreCle, double prix, Date dateActe,  String nom,String prenom, String telephone,String cin,long numeroDossier,String ngap) {
		super();
		this.id = id;
		this.codeActe = codeActe;
		this.lettreCle = lettreCle;
		this.prix = prix;
		this.dateActe = dateActe;
		this.nom = nom;
		this.prenom = prenom;
		this.telephone = telephone;
		this.cin = cin;
		this.numeroDossier=numeroDossier; 
		this.ngap =ngap;
	}




	public ActeLabo(String nom, String prenom, String telephone) {
		super();
		this.nom = nom;
		this.prenom = prenom;
		this.telephone = telephone;
	}




	public ActeLabo() {
		// TODO Auto-generated constructor stub
	}




	public long getId() {
		return id;
	}




	public void setId(long id) {
		this.id = id;
	}




	public String getCodeActe() {
		return codeActe;
	}




	public void setCodeActe(String codeActe) {
		this.codeActe = codeActe;
	}




	public String getLettreCle() {
		return lettreCle;
	}




	public void setLettreCle(String lettreCle) {
		this.lettreCle = lettreCle;
	}




	public double getPrix() {
		return prix;
	}




	public void setPrix(double prix) {
		this.prix = prix;
	}




	public Date getDateActe() {
		return dateActe;
	}




	public void setDateActe(Date dateActe) {
		this.dateActe = dateActe;
	}




	public String getNgap() {
		return ngap;
	}




	public void setNgap(String ngap) {
		this.ngap = ngap;
	}




	public Laboratoire getLaboratoire() {
		return laboratoire;
	}




	public void setLaboratoire(Laboratoire laboratoire) {
		this.laboratoire = laboratoire;
	}




	public Adherent getAdherent() {
		return adherent;
	}




	public void setAdherent(Adherent adherent) {
		this.adherent = adherent;
	}




	public Medecin getMedecin() {
		return medecin;
	}




	public void setMedecin(Medecin medecin) {
		this.medecin = medecin;
	}




	public String getNom() {
		return nom;
	}




	public void setNom(String nom) {
		this.nom = nom;
	}




	public String getPrenom() {
		return prenom;
	}




	public void setPrenom(String prenom) {
		this.prenom = prenom;
	}




	public String getTelephone() {
		return telephone;
	}




	public void setTelephone(String telephone) {
		this.telephone = telephone;
	}




	public String getCin() {
		return cin;
	}




	public void setCin(String cin) {
		this.cin = cin;
	}




	public long getNumeroDossier() {
		return numeroDossier;
	}




	public void setNumeroDossier(long numeroDossier) {
		this.numeroDossier = numeroDossier;
	}




	
	
}