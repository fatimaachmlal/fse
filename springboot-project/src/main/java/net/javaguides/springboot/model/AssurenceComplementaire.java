package net.javaguides.springboot.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import lombok.Data;

@Entity
@Table(name="assurance_complementaire")
@Data
public class AssurenceComplementaire {
	
	public AssurenceComplementaire() {
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getNomAssurance() {
		return NomAssurance;
	}

	public void setNomAssurance(String nomAssurance) {
		NomAssurance = nomAssurance;
	}

	public String getNumPolice() {
		return NumPolice;
	}

	public void setNumPolice(String numPolice) {
		NumPolice = numPolice;
	}

	public Adherent getAdherent() {
		return adherent;
	}

	public void setAdherent(Adherent adherent) {
		this.adherent = adherent;
	}

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long id;
	
	@Column(name ="Nom_assurance")
	private String NomAssurance;	
	
	@Column(name ="Num_police")
	private String NumPolice;

	
	@ManyToOne(fetch = FetchType.EAGER)
	private Adherent adherent;
		
}
