package net.javaguides.springboot.model;


import java.io.File;
import java.sql.Date;
import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.springframework.security.core.GrantedAuthority;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;


@Entity
@Table(name="radiologie")
@Data @NoArgsConstructor @AllArgsConstructor
@EqualsAndHashCode(callSuper = true)
public class Radiologie extends Personne{



	
	@Column(name = "raison_sociale")
	private String raisonSociale;
	
	@Column(name = "inpe")
	private String inpe;
	
	@Column(name = "ice")
	private String ice;
	
	public String getRaisonSociale() {
		return raisonSociale;
	}

	public void setRaisonSociale(String raisonSociale) {
		this.raisonSociale = raisonSociale;
	}

	public String getInpe() {
		return inpe;
	}

	public void setInpe(String inpe) {
		this.inpe = inpe;
	}

	public String getIce() {
		return ice;
	}

	public void setIce(String ice) {
		this.ice = ice;
	}

	public File getSignature() {
		return signature;
	}

	public void setSignature(File signature) {
		this.signature = signature;
	}

	public File getCachet() {
		return cachet;
	}

	public void setCachet(File cachet) {
		this.cachet = cachet;
	}

	public File getCachet_pro() {
		return cachet_pro;
	}

	public void setCachet_pro(File cachet_pro) {
		this.cachet_pro = cachet_pro;
	}

	public Set<ActeRadio> getActes() {
		return actes;
	}

	public void setActes(Set<ActeRadio> actes) {
		this.actes = actes;
	}

	public Set<Role> getRoles() {
		return roles;
	}

	public void setRoles(Set<Role> roles) {
		this.roles = roles;
	}

	@Column(name = "signature")
	private File signature;
	
	@Column(name = "cachet")
	private File cachet;
	
	@Column(name = "cachet_pro")
	private File cachet_pro;
	
	@OneToMany(cascade = CascadeType.ALL,mappedBy = "radiologie")
	private Set<ActeRadio> actes=new HashSet<>();
	/*
	 	@OneToMany(cascade = CascadeType.ALL,mappedBy = "radiologie")
	private Set<Fse> fses=new HashSet<>();
	
	 */

	@ManyToMany(fetch = FetchType.LAZY)
    @JoinTable( name = "Radio_roles", joinColumns = @JoinColumn(name = "radio_id"),inverseJoinColumns = @JoinColumn(name = "role_id")) 
	private Set<Role> roles = new HashSet<>();

	/*
	public void setRaisonSociale(String raisonSociale2) {
		// TODO Auto-generated method stub
		this.raisonSociale=raisonSociale2;
		
	}

	public void setInpe(String inpe2) {
		// TODO Auto-generated method stub
		this.inpe=inpe2;
	}

	public void setIce(String ice2) {
		// TODO Auto-generated method stub
		this.ice=ice2;
		
	}

	public void setRoles(Set<Role> roles2) {
		// TODO Auto-generated method stub
		this.roles=roles2;
		
	}

	public Long getId() {
		// TODO Auto-generated method stub
		return id;
	}

	public String getNom() {
		// TODO Auto-generated method stub
		return nom;
	}

	public String getInpe() {
		// TODO Auto-generated method stub
		return inpe;
	}

	public String getPrenom() {
		// TODO Auto-generated method stub
		return prenom;
	}

	public SituationFamiliale getSituationFamiliale() {
		// TODO Auto-generated method stub
		return situationFamiliale;
	}

	public Sexe getSexe() {
		// TODO Auto-generated method stub
		return sexe;
	}

	public String getVille() {
		// TODO Auto-generated method stub
		return ville;
	}

	public String getAdresse() {
		// TODO Auto-generated method stub
		return adresse;
	}

	public String getTelephone() {
		// TODO Auto-generated method stub
		return telephone;
	}

	public String getEmail() {
		// TODO Auto-generated method stub
		return email;
	}

	public String getCin() {
		// TODO Auto-generated method stub
		return cin;
	}

	public String getRaisonSociale() {
		// TODO Auto-generated method stub
		return raisonSociale;
	}

	public String getIce() {
		// TODO Auto-generated method stub
		return ice;
	}

	public Set<Role> getRoles() {
		// TODO Auto-generated method stub
		return roles;
	}

	public String getUsername() {
		// TODO Auto-generated method stub
		return username;
	}

	public String getMotDePasse() {
		// TODO Auto-generated method stub
		return motDePasse;
	}

	public Set<ActeRadio> getActes() {
		// TODO Auto-generated method stub
		return actes;
	}

	public void setActes(Set<ActeRadio> actes2) {
		// TODO Auto-generated method stub
		this.actes=actes2;
		
	}

	public File getCachet() {
		// TODO Auto-generated method stub
		return cachet;
	}

	public void setCachet(File cachet2) {
		// TODO Auto-generated method stub
		this.cachet=cachet2;
		
	}

	public String getCodePostal() {
		// TODO Auto-generated method stub
		return codePostal;
	}

	public File getSignature() {
		// TODO Auto-generated method stub
		return signature;
	}

	public void setSignature(File signature2) {
		// TODO Auto-generated method stub
		this.signature=signature2;
		
	}
*/
	

	

	

	

	
}
