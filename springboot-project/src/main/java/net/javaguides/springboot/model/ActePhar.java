package net.javaguides.springboot.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Table(name="acte_phar")
@Data
@NoArgsConstructor 
@AllArgsConstructor
public class ActePhar {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long id;
	
	@Column(name = "prix")
	private double prix;
	
	@Column(name = "date_acte")
	@JsonFormat(pattern="yyyy-MM-dd")
	private Date dateActe;
	
	@ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "pharmacie_id", referencedColumnName = "id")
    @JsonIgnoreProperties("actes")
	private Pharmacie pharmacie;
	
	
	@ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "Adherent_id", referencedColumnName = "id")
    @JsonIgnoreProperties("actes")
	private Adherent adherent;
	
	@ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "Medecin_id", referencedColumnName = "id")
    @JsonIgnoreProperties("actes")
	private Medecin medecin;
	
	
    @Transient 
	protected String nom;

    @Transient 
	protected String prenom;
    @Transient 
	protected String telephone;
    @Transient 
   	protected String cin;
    
    @Transient 
   	protected long numeroDossier;

    
    
	public ActePhar(long id, double prix, Date dateActe,  String nom,
			String prenom, String telephone,String cin,long numeroDossier) {
		super();
		this.id = id;
		this.prix = prix;
		this.dateActe = dateActe;
		this.nom = nom;
		this.prenom = prenom;
		this.telephone = telephone;
		this.cin = cin;
		this.numeroDossier= numeroDossier;
	}




	public ActePhar(String nom, String prenom, String telephone) {
		super();
		this.nom = nom;
		this.prenom = prenom;
		this.telephone = telephone;
	}




	public ActePhar() {
		// TODO Auto-generated constructor stub
	}




	public void setDateActe(Date dateActe2) {
		// TODO Auto-generated method stub
		this.dateActe=dateActe2;
		
	}




	public void setPrix(double prix2) {
		// TODO Auto-generated method stub
		this.prix=prix2;
		
	}




	public void setPharmacie(Pharmacie pharmacie2) {
		// TODO Auto-generated method stub
		this.pharmacie=pharmacie2;
		
	}




	public void setAdherent(Adherent adherent2) {
		// TODO Auto-generated method stub
		this.adherent=adherent2;
		
	}




	public void setMedecin(Medecin medecin2) {
		// TODO Auto-generated method stub
		this.medecin=medecin2;
		
	}




	public double getPrix() {
		// TODO Auto-generated method stub
		return prix;
	}




	public Date getDateActe() {
		// TODO Auto-generated method stub
		return dateActe;
	}




	public Pharmacie getPharmacie() {
		// TODO Auto-generated method stub
		return pharmacie;
	}
	
}
