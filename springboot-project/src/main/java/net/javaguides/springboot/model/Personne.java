package net.javaguides.springboot.model;


import java.util.Date;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.Lob;
import javax.persistence.Table;

import org.springframework.web.multipart.MultipartFile;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data 
@NoArgsConstructor 
@AllArgsConstructor
@Inheritance(strategy = InheritanceType.JOINED)
@Entity
@Table(name="personne")
public abstract class Personne {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	protected long id;
	
	@Column(name = "username")
	protected String username;
	
	@Column(name = "nom")
	protected String nom;

	@Column(name = "prenom")
	protected String prenom;
	
	// @Lob
	 //protected byte[] phtotProfil;



	@Column(name = "date_naissance")
	protected Date dateNaissance;

	@Column(name = "cin")
	protected String cin;

	@Column(name = "email")
	protected String email;

	@Column(name = "telephone")
	protected String telephone;
	
	@Column(name = "mot_de_passe")
	protected String motDePasse;

	@Column(name = "adresse")
	protected String adresse;
	@Column(name = "ville")
	protected String ville;
	@Column(name = "code_postal")
	protected String codePostal;

	
	@Enumerated(EnumType.STRING)
	@Column(name = "sexe")
	protected Sexe sexe;

	
	@Enumerated(EnumType.STRING)
	@Column(name = "situation_familiale")
	protected SituationFamiliale situationFamiliale;
/*
	public void setMotDePasse(String encode) {
		// TODO Auto-generated method stub
		this.motDePasse=encode;
		
	}
	public void setVille(String ville) {
		// TODO Auto-generated method stub
		this.ville=ville;
	}
	public void setTelephone(String telephone) {
		// TODO Auto-generated method stub
		this.telephone=telephone;
		
	}
	public void setSituationFamiliale(SituationFamiliale situationFamiliale) {
		// TODO Auto-generated method stub
		this.situationFamiliale=situationFamiliale;
	}
	
	public void setSexe(Sexe sexe) {
		// TODO Auto-generated method stub
		this.sexe=sexe;
	}
	public void setPrenom(String prenom) {
		// TODO Auto-generated method stub
		this.prenom=prenom;
	}
	public void setNom(String nom) {
		// TODO Auto-generated method stub
		this.nom=nom;
		
	}
	public void setDateNaissance(Date dateNaissance) {
		// TODO Auto-generated method stub
		this.dateNaissance=dateNaissance;
		
		
	}
	public void setCodePostal(String codePostal) {
		// TODO Auto-generated method stub
		this.codePostal=codePostal;
		
	}
	public void setCin(String cin) {
		// TODO Auto-generated method stub
		this.cin=cin;
	}

	public void setAdresse(String adresse) {
		// TODO Auto-generated method stub
		this.adresse=adresse;
		
	}
	public void setUsername(String username) {
		// TODO Auto-generated method stub
		this.username=username;
		
	}
	public void setEmail(String email) {
		// TODO Auto-generated method stub
		this.email=email;
		
	}
	public Date getDateNaissance() {
		// TODO Auto-generated method stub
		return dateNaissance;
	}
	
	*/


	public long getId() {
		return id;
	}


	public void setId(long id) {
		this.id = id;
	}


	public String getUsername() {
		return username;
	}


	public void setUsername(String username) {
		this.username = username;
	}


	public String getNom() {
		return nom;
	}


	public void setNom(String nom) {
		this.nom = nom;
	}


	public String getPrenom() {
		return prenom;
	}


	public void setPrenom(String prenom) {
		this.prenom = prenom;
	}


	public Date getDateNaissance() {
		return dateNaissance;
	}


	public void setDateNaissance(Date dateNaissance) {
		this.dateNaissance = dateNaissance;
	}


	public String getCin() {
		return cin;
	}


	public void setCin(String cin) {
		this.cin = cin;
	}


	public String getEmail() {
		return email;
	}


	public void setEmail(String email) {
		this.email = email;
	}


	public String getTelephone() {
		return telephone;
	}


	public void setTelephone(String telephone) {
		this.telephone = telephone;
	}


	public String getMotDePasse() {
		return motDePasse;
	}


	public void setMotDePasse(String motDePasse) {
		this.motDePasse = motDePasse;
	}


	public String getAdresse() {
		return adresse;
	}


	public void setAdresse(String adresse) {
		this.adresse = adresse;
	}


	public String getVille() {
		return ville;
	}


	public void setVille(String ville) {
		this.ville = ville;
	}


	public String getCodePostal() {
		return codePostal;
	}


	public void setCodePostal(String codePostal) {
		this.codePostal = codePostal;
	}


	public Sexe getSexe() {
		return sexe;
	}


	public void setSexe(Sexe sexe) {
		this.sexe = sexe;
	}


	public SituationFamiliale getSituationFamiliale() {
		return situationFamiliale;
	}


	public void setSituationFamiliale(SituationFamiliale situationFamiliale) {
		this.situationFamiliale = situationFamiliale;
	}
	
	
	
	
	
}

