package net.javaguides.springboot.model;


import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import lombok.Data;

@Entity
@Table(name="ald")
@Data
public class Ald {
	
	public Adherent getAdherent() {
		return adherent;
	}

	public void setAdherent(Adherent adherent) {
		this.adherent = adherent;
	}

	public Ald(long id, String codeAld,String nDossier) {		
		this.id=id;
		this.codeAld=codeAld;
		this.nDossier=nDossier;		
	}
	
	public Ald() {		
	             
	}
	
	

	public String getCodeAld() {
		return codeAld;
	}

	public void setCodeAld(String codeAld) {
		this.codeAld = codeAld;
	}

	public String getnDossier() {
		return nDossier;
	}

	public void setnDossier(String nDossier) {
		this.nDossier = nDossier;
	}

	

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long id;
	
	@Column(name = "code_ald")
	private String codeAld;
	
	@Column(name = "n_dossier")
	private String nDossier;

	
	@ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "beneficiaire_id", referencedColumnName = "id")
    @JsonIgnoreProperties("beneficiaires")
	private Beneficiaire beneficiaire; 
	
	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}



	@ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "adherent_id", referencedColumnName = "id")
    @JsonIgnoreProperties("alds")
	private Adherent adherent;

	

	
	
	
}
