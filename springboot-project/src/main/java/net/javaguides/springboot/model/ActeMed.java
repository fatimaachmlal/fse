package net.javaguides.springboot.model;


import java.util.Date;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
@NoArgsConstructor 
@AllArgsConstructor
@Entity
@Table(name="acte_med")
@Data
public class ActeMed {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long id;
	
	@Column(name = "code_acte")
	private String codeActe;
	
	@Column(name = "lettre_cle")
	private String lettreCle;
	
	@Column(name = "prix")
	private double prix;
	
	@Column(name = "date_acte")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss")
	private Date dateActe;
	

	@Column(name = "Ordonnance_Biologie")
	private String Ordonnance_Biologie;	
	
	@Column(name = "Ordonnance_Radiologie")
	private String Ordonnance_Radiologie;

	@Column(name = "Ordonnance_Pharmacie")
	private String Ordonnance_Pharmacie;
	
	@Column(name = "Type_de_soins")
	private String Type_de_soins;
	
	@Column(name = "Ald")
	private String Ald;
	
	@ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "medecin_id", referencedColumnName = "id")
    @JsonIgnoreProperties("actes")
	private Medecin medecin;
	
	@ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "adherent_id", referencedColumnName = "id")
    @JsonIgnoreProperties("actes")
	private Adherent adherent;
	
	@ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "beneficiaire_id", referencedColumnName = "id")
    @JsonIgnoreProperties("actes")
	private Beneficiaire beneficiaire;
		
	@Transient
	protected String nom;
	@Transient
	protected String prenom;
	@Transient
	protected String cin;
	@Transient
	protected String telephone;
	@Transient
	protected long medecin_id;
	@Transient
	protected long adherent_id;
	@Transient
	protected long beneficiaire_id;
	
    public long getBeneficiaire_id() {
		return beneficiaire_id;
	}

	public void setBeneficiaire_id(long beneficiaire_id) {
		this.beneficiaire_id = beneficiaire_id;
	}

	@Transient 
   	protected long numeroDossier;
	
	
	
	
	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public String getPrenom() {
		return prenom;
	}

	public void setPrenom(String prenom) {
		this.prenom = prenom;
	}

	public String getCin() {
		return cin;
	}

	public void setCin(String cin) {
		this.cin = cin;
	}

	public String getTelephone() {
		return telephone;
	}

	public void setTelephone(String telephone) {
		this.telephone = telephone;
	}

	public long getMedecin_id() {
		return medecin_id;
	}

	public void setMedecin_id(long medecin_id) {
		this.medecin_id = medecin_id;
	}

	public long getAdherent_id() {
		return adherent_id;
	}

	public void setAdherent_id(long adherent_id) {
		this.adherent_id = adherent_id;
	}

	public long getNumeroDossier() {
		return numeroDossier;
	}

	public void setNumeroDossier(long numeroDossier) {
		this.numeroDossier = numeroDossier;
	}

	public String getOrdonnance_Biologie() {
		return Ordonnance_Biologie;
	}

	public String getOrdonnance_Radiologie() {
		return Ordonnance_Radiologie;
	}

	public String getOrdonnance_Pharmacie() {
		return Ordonnance_Pharmacie;
	}

	public String getType_de_soins() {
		return Type_de_soins;
	}

	public String getAld() {
		return Ald;
	}

	public Adherent getAdherent() {
		return adherent;
	}

	
	
	public Beneficiaire getBeneficiaire() {
		return beneficiaire;
	}

	public void setBeneficiaire(Beneficiaire beneficiaire) {
		this.beneficiaire = beneficiaire;
	}

	
	
	public ActeMed(long id, String nom, String prenom, String cin, String telephone, Date dateActe ,long medecin_id ,long adherent_id) {
		super();
		this.id = id;
		this.dateActe = dateActe;
		this.nom = nom;
		this.prenom = prenom;
		this.cin = cin;
		this.telephone = telephone;
		this.medecin_id = medecin_id;
		this.adherent_id=adherent_id;
		
	}
	
	public ActeMed(long id, String nom, String prenom, String cin, String telephone, Date dateActe ) {
		super();
		this.id = id;
		this.dateActe = dateActe;
		this.nom = nom;
		this.prenom = prenom;
		this.cin = cin;
		this.telephone = telephone;
		
		
	}
	
    
	public ActeMed(long id, String codeActe, String lettreCle, double prix, Date dateActe,  String nom,
			String prenom, String telephone,String cin,String Ordonnance_Biologie,String Ordonnance_Radiologie, String Ordonnance_Pharmacie,String Type_de_soins,String Ald,long numeroDossier
  ) {
		super();
		this.id = id;
		this.codeActe = codeActe;
		this.lettreCle = lettreCle;
		this.prix = prix;
		this.dateActe = dateActe;
		this.nom = nom;
		this.prenom = prenom;
		this.telephone = telephone;
		this.cin = cin;
		this.Ordonnance_Biologie=Ordonnance_Biologie;
		this.Ordonnance_Radiologie=Ordonnance_Radiologie;
		this.Ordonnance_Pharmacie=Ordonnance_Pharmacie;
		this.Type_de_soins=Type_de_soins;
		this.Ald=Ald;
		this.numeroDossier=numeroDossier;
	}
	public ActeMed(String nom, String prenom, String telephone) {
		super();
		this.nom = nom;
		this.prenom = prenom;
		this.telephone = telephone;
	}

	public ActeMed() {
		// TODO Auto-generated constructor stub
	}

	public void setPrix(double prix2) {
		// TODO Auto-generated method stub
		this.prix=prix2;
		
	}

	public void setMedecin(Medecin medecin2) {
		// TODO Auto-generated method stub
		this.medecin=medecin2;
		
	}

	public void setAdherent(Adherent adherent2) {
		// TODO Auto-generated method stub
		this.adherent=adherent2;
		
	}

	public void setLettreCle(String lettreCle2) {
		// TODO Auto-generated method stub
		this.lettreCle=lettreCle2;
		
	}

	

	public void setOrdonnance_Biologie(String ordonnance_Biologie2) {
		// TODO Auto-generated method stub
		this.Ordonnance_Biologie=ordonnance_Biologie2;
		
	}

	public void setOrdonnance_Radiologie(String ordonnance_Radiologie2) {
		// TODO Auto-generated method stub
		this.Ordonnance_Radiologie=ordonnance_Radiologie2;
		
	}

	public void setOrdonnance_Pharmacie(String ordonnance_Pharmacie2) {
		// TODO Auto-generated method stub
		this.Ordonnance_Pharmacie=ordonnance_Pharmacie2;
		
	}

	public void setType_de_soins(String type_de_soins2) {
		// TODO Auto-generated method stub
		this.Type_de_soins=type_de_soins2;
		
	}

	public void setAld(String ald2) {
		// TODO Auto-generated method stub
		this.Ald=ald2;
		
	}

	public void setDateActe(Date dateActe2) {
		// TODO Auto-generated method stub
		this.dateActe=dateActe2;
		
	}

	public void setCodeActe(Date codeActe2) {
		// TODO Auto-generated method stub
		
	}

	public void setCodeActe(String codeActe2) {
		// TODO Auto-generated method stub
		this.codeActe=codeActe2;
		
	}

	public String getCodeActe() {
		// TODO Auto-generated method stub
		return codeActe;
	}

	public String getLettreCle() {
		// TODO Auto-generated method stub
		return lettreCle;
	}

	public double getPrix() {
		// TODO Auto-generated method stub
		return prix;
	}

	public Date getDateActe() {
		// TODO Auto-generated method stub
		return dateActe;
	}

	public Medecin getMedecin() {
		// TODO Auto-generated method stub
		return medecin;
	}

	

	

	
	

	
}
