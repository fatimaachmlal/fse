package net.javaguides.springboot.model;

import java.io.File;
import java.util.Collection;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

import org.springframework.security.core.GrantedAuthority;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Data 
@Entity
@Table(name="medecin")

@NoArgsConstructor 
@AllArgsConstructor
@EqualsAndHashCode(callSuper = true)
public class Medecin extends Personne {

	@Column(name = "specialite")
	private String specialite;
	
	@Column(name = "inpe")
	private String inpe;
	
	@Column(name = "ice")
	private String ice;
	
	@Column(name = "type_mutuelle")
	private String typeMutuelle;
	

	@Transient
	private byte[] photoCarteCin;
	
	@Transient
	private byte[] carteProfessionnelle;
	
	@Transient
	private byte[] cachetSignature;
	
	@Transient
	private byte[] PhotoProfile;


    //@Transient 
   // private long MedecinId;
    
 
   /*	@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
	@EqualsAndHashCode.Exclude
	@ToString.Exclude*/
	
	//,fetch = FetchType.EAGER,
	@OneToMany(  cascade = CascadeType.ALL ,mappedBy = "medecin")
	private Set<ActeMed> actes=new HashSet<>();
	
	
/*	@OneToMany(fetch = FetchType.EAGER, mappedBy = "medecin", cascade = CascadeType.ALL)
	@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
	@EqualsAndHashCode.Exclude
	@ToString.Exclude
	private Set<Image> img=new HashSet<>();
	
	*/
	
	
	
	@ManyToMany(fetch = FetchType.LAZY)
    @JoinTable( name = "Med_roles", joinColumns = @JoinColumn(name = "Med_id"),inverseJoinColumns = @JoinColumn(name = "role_id")) 
	private Set<Role> roles = new HashSet<>();
	
	public Medecin(Long id, String nom,String prenom,Date dateNaissance, String cin, String email, String telephone,String motDePasse,String adresse, String ville, String codePostal, Sexe sexe, String specialite, String inpe,String ice, String typeMutuelle, String username,byte[] cachetSignature,byte[] PhotoProfile, byte[] carteProfessionnelle, byte [] photoCarteCin) {
		
		super();
		
		this.id =id;
		this.nom =nom;
		this.prenom = prenom;
		this.dateNaissance= dateNaissance;
		this.cin= cin;
		this.email = email;
		this.telephone = telephone;
		this.motDePasse=motDePasse;
		this.adresse = adresse;
		this.ville=ville;
		this.codePostal = codePostal;
		this.sexe= sexe;
		this.specialite = specialite;
		this.inpe = inpe;
		this.ice = ice;
		this.typeMutuelle = typeMutuelle;
		this.username=username;
		this.cachetSignature=cachetSignature;
		this.PhotoProfile=PhotoProfile;
		this.carteProfessionnelle = carteProfessionnelle;
		this.photoCarteCin = photoCarteCin;

		
	}

public Medecin( String nom,String prenom,String cin, String telephone,String inpe,String ice, long id,String Email,Sexe sexe,String ville,String adresse,String codePostal,String typeMutuelle,String specialite,Date dateNaissance,String motDePasse ) {
	super();
	this.nom =nom;
	this.prenom = prenom;
	this.cin= cin;
	this.telephone = telephone;
	this.inpe = inpe;
	this.ice = ice;
    this.id=id;
    email=Email;
    this.sexe=sexe;
    this.ville=ville;
    this.adresse=adresse;
    this.codePostal=codePostal;
    this.typeMutuelle=typeMutuelle;
    this.specialite=specialite;
    this.dateNaissance=dateNaissance;
    this.motDePasse=motDePasse;
}



public Medecin() {
	// TODO Auto-generated constructor stub
}
/*

public void setUsername(String username) {
	// TODO Auto-generated method stub
	this.username=username;
	
}

public void setEmail(String email) {
	// TODO Auto-generated method stub
	this.email=email;
	
}



public void setVille(String ville) {
	// TODO Auto-generated method stub
	this.ville=ville;
	
}

public void setTelephone(String telephone) {
	// TODO Auto-generated method stub
	this.telephone=telephone;
}

public void setSpecialite(String specialite2) {
	// TODO Auto-generated method stub
	this.specialite=specialite2;
	
}

public void setSituationFamiliale(SituationFamiliale situationFamiliale) {
	// TODO Auto-generated method stub
	this.situationFamiliale=situationFamiliale;
	
}

public void setTypeMutuelle(String typeMutuelle2) {
	// TODO Auto-generated method stub
	this.typeMutuelle=typeMutuelle2;
	
}

public void setSexe(Sexe sexe) {
	// TODO Auto-generated method stub
	this.sexe=sexe;
	
}

public void setPrenom(String prenom) {
	// TODO Auto-generated method stub
	this.prenom=prenom;
	
}

public void setNom(String nom) {
	// TODO Auto-generated method stub
	this.nom=nom;
	
}

public void setInpe(String inpe2) {
	// TODO Auto-generated method stub
	this.inpe=inpe2;
	
}

public void setIce(String ice2) {
	// TODO Auto-generated method stub
	this.ice=ice2;
	
}

public void setDateNaissance(java.sql.Date dateNaissance) {
	// TODO Auto-generated method stub
	this.dateNaissance=dateNaissance;
	
}

public void setCodePostal(String codePostal) {
	// TODO Auto-generated method stub
	this.codePostal=codePostal;
			
	
}

public void setCin(String cin) {
	// TODO Auto-generated method stub
	this.cin=cin;
	
}

public void setAdresse(String adresse) {
	// TODO Auto-generated method stub
	this.adresse=adresse;
	
}

public void setRoles(Set<Role> roles2) {
	// TODO Auto-generated method stub
	this.roles=roles2;
	
}

public Long getId() {
	// TODO Auto-generated method stub
	return id;
}

public String getNom() {
	// TODO Auto-generated method stub
	return nom;
}

public String getInpe() {
	// TODO Auto-generated method stub
	return inpe;
}

public String getPrenom() {
	// TODO Auto-generated method stub
	return prenom;
}

public SituationFamiliale getSituationFamiliale() {
	// TODO Auto-generated method stub
	return situationFamiliale;
}

public Sexe getSexe() {
	// TODO Auto-generated method stub
	return sexe;
}

public String getVille() {
	// TODO Auto-generated method stub
	return ville;
}

public String getAdresse() {
	// TODO Auto-generated method stub
	return adresse;
}

public String getTelephone() {
	// TODO Auto-generated method stub
	return telephone;
}

public String getEmail() {
	// TODO Auto-generated method stub
	return email;
}

public String getCin() {
	// TODO Auto-generated method stub
	return cin;
}

public Date getDateNaissance() {
	// TODO Auto-generated method stub
	return dateNaissance;
}

public String getSpecialite() {
	// TODO Auto-generated method stub
	return specialite;
}

public String getTypeMutuelle() {
	// TODO Auto-generated method stub
	return typeMutuelle;
}

public String getIce() {
	// TODO Auto-generated method stub
	return ice;
}

public Set<Role> getRoles() {
	// TODO Auto-generated method stub
	return roles;
}

public String getUsername() {
	// TODO Auto-generated method stub
	return username;
}

public String getMotDePasse() {
	// TODO Auto-generated method stub
	return motDePasse;
}

public Set<ActeMed> getActes() {
	// TODO Auto-generated method stub
	return actes;
}

public void setActes(Set<ActeMed> actes2) {
	// TODO Auto-generated method stub
	this.actes=actes2;
	
}

public String getCodePostal() {
	// TODO Auto-generated method stub
	return codePostal;
}


*/

public String getSpecialite() {
	return specialite;
}

public void setSpecialite(String specialite) {
	this.specialite = specialite;
}

public String getInpe() {
	return inpe;
}

public void setInpe(String inpe) {
	this.inpe = inpe;
}

public String getIce() {
	return ice;
}

public void setIce(String ice) {
	this.ice = ice;
}

public String getTypeMutuelle() {
	return typeMutuelle;
}

public void setTypeMutuelle(String typeMutuelle) {
	this.typeMutuelle = typeMutuelle;
}

public byte[] getPhotoCarteCin() {
	return photoCarteCin;
}

public void setPhotoCarteCin(byte[] photoCarteCin) {
	this.photoCarteCin = photoCarteCin;
}

public byte[] getCarteProfessionnelle() {
	return carteProfessionnelle;
}

public void setCarteProfessionnelle(byte[] carteProfessionnelle) {
	this.carteProfessionnelle = carteProfessionnelle;
}

public byte[] getCachetSignature() {
	return cachetSignature;
}

public void setCachetSignature(byte[] cachetSignature) {
	this.cachetSignature = cachetSignature;
}

public byte[] getPhotoProfile() {
	return PhotoProfile;
}

public void setPhotoProfile(byte[] photoProfile) {
	PhotoProfile = photoProfile;
}


public Set<ActeMed> getActes() {
	return actes;
}

public void setActes(Set<ActeMed> actes) {
	this.actes = actes;
}

public Set<Role> getRoles() {
	return roles;
}

public void setRoles(Set<Role> roles) {
	this.roles = roles;
}





	
	
	
	

}
