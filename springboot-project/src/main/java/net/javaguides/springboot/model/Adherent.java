package net.javaguides.springboot.model;

import java.io.File;
import java.sql.Date;
import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.springframework.security.core.GrantedAuthority;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;



@Entity
@Table(name="adherent")
@Data @NoArgsConstructor @AllArgsConstructor
@EqualsAndHashCode(callSuper = true)
public class Adherent extends Personne{
	

/*	@Column(name = "id_Adherent ", unique = true, nullable = false)
	@GeneratedValue(strategy = GenerationType.AUTO)
	private int id_Adherent;*/
	
	@Column(name = "type_mutuelle")
	private String typeMutuelle;
	
	@Column(name = "Immatriculation")
	private String immatriculation;	
	
	@Column(name = "carte_mutuelle")
	private File carteMutuelle;
	
	@OneToMany(cascade = CascadeType.ALL,mappedBy = "adherent")
	private Set<Beneficiaire> beneficiaires=new HashSet<>();
	
	@OneToMany(cascade = CascadeType.ALL,mappedBy = "adherent")
	private Set<Fse> fses=new HashSet<>();
	
	@OneToMany(cascade = CascadeType.ALL,mappedBy = "adherent")
	private Set<Ald> alds=new HashSet<>();
	
	
	@ManyToMany(fetch = FetchType.LAZY)
    @JoinTable( name = "user_roles", joinColumns = @JoinColumn(name = "id"),inverseJoinColumns = @JoinColumn(name = "role_id")) 
	private Set<Role> roles = new HashSet<>();
	
	public Adherent() {
		
	}
    public Adherent(Set<Ald> alds) {
    	this.alds=alds;
		
	}
	public String getTypeMutuelle() {
		return typeMutuelle;
	}

	public void setTypeMutuelle(String typeMutuelle) {
		this.typeMutuelle = typeMutuelle;
	}

	public String getImmatriculation() {
		return immatriculation;
	}

	public void setImmatriculation(String immatriculation) {
		this.immatriculation = immatriculation;
	}

	public File getCarteMutuelle() {
		return carteMutuelle;
	}

	public void setCarteMutuelle(File carteMutuelle) {
		this.carteMutuelle = carteMutuelle;
	}
/*
	public Set<Beneficiaire> getBeneficiaires() {
		return beneficiaires;
	}
*/

	public void setBeneficiaires(Set<Beneficiaire> beneficiaires) {
		this.beneficiaires = beneficiaires;
	}

	public Set<Fse> getFses() {
		return fses;
	}

	public void setFses(Set<Fse> fses) {
		this.fses = fses;
	}

	public Set<Ald> getAlds() {
		return alds;
	}

	public void setAlds(Set<Ald> alds) {
		this.alds = alds;
	}

	public Set<Role> getRoles() {
		return roles;
	}

	public void setRoles(Set<Role> roles) {
		this.roles = roles;
	}
	public String getMotdePasse() {
		// TODO Auto-generated method stub
		return motDePasse;
	}

	
	

	/*
	public String getCin() {
		// TODO Auto-generated method stub
		return cin;
		
	}

	public Long getId() {
		// TODO Auto-generated method stub
		return id;
	}

	public String getNom() {
		// TODO Auto-generated method stub
		return nom;
	}

	public String getPrenom() {
		// TODO Auto-generated method stub
		return prenom;
	}

	public String getTelephone() {
		// TODO Auto-generated method stub
		return telephone;
	}

	public String getVille() {
		// TODO Auto-generated method stub
		return ville;
	}

	public Date getDateNaissance() {
		// TODO Auto-generated method stub
		return getDateNaissance();
	}

	public SituationFamiliale getSituationFamiliale() {
		// TODO Auto-generated method stub
		return situationFamiliale;
				
	}

	public Sexe getSexe() {
		// TODO Auto-generated method stub
		return sexe;
	}

	public String getAdresse() {
		// TODO Auto-generated method stub
		return adresse;
	}

	public String getEmail() {
		// TODO Auto-generated method stub
		return email;
	}

	public String getTypeMutuelle() {
		// TODO Auto-generated method stub
		return typeMutuelle;
	}

	public String getImmatriculation() {
		// TODO Auto-generated method stub
		return immatriculation;
	}

	public void setTypeMutuelle(String typeMutuelle2) {
		// TODO Auto-generated method stub
		this.typeMutuelle=typeMutuelle2;
		
	}

	public void setImmatriculation(String immatriculation2) {
		// TODO Auto-generated method stub
		this.immatriculation=immatriculation2;
		
	}

	public void setVille(String ville) {
		// TODO Auto-generated method stub
		this.ville=ville;
		
	}

	public void setSituationFamiliale(SituationFamiliale situationFamiliale) {
		// TODO Auto-generated method stub
		this.situationFamiliale=situationFamiliale;
		
	}

	public void setSexe(Sexe sexe) {
		// TODO Auto-generated method stub
		this.sexe=sexe;
		
	}

	public void setPrenom(String prenom) {
		// TODO Auto-generated method stub
		this.prenom=prenom;
		
	}

	public void setNom(String nom) {
		// TODO Auto-generated method stub
		this.nom=nom;
		
	}

	public void setMotDePasse(String motDePasse) {
		// TODO Auto-generated method stub
		this.motDePasse=motDePasse;
		
	}

	public void setEmail(String email) {
		// TODO Auto-generated method stub
		this.email=email;
		
	}

	public void setDateNaissance(Date dateNaissance) {
		// TODO Auto-generated method stub
		this.dateNaissance=dateNaissance;
		
	}

	public void setCodePostal(String codePostal) {
		// TODO Auto-generated method stub
		this.codePostal=codePostal;
		
	}

	public void setCin(String cin) {
		// TODO Auto-generated method stub
		this.cin=cin;
		
	}

	public void setAdresse(String adresse) {
		// TODO Auto-generated method stub
		this.adresse=adresse;
		
	}

	public void setUsername(String username) {
		// TODO Auto-generated method stub
		this.username=username;
		
	}

	public void setTelephone(String telephone) {
		// TODO Auto-generated method stub
		this.telephone=telephone;
		
	}

	public void setRoles(Set<Role> roles2) {
		// TODO Auto-generated method stub
		this.roles=roles2;
		
	}

	public Set<Role> getRoles() {
		// TODO Auto-generated method stub
		return roles;
	}

	public String getUsername() {
		// TODO Auto-generated method stub
		return username;
	}

	public String getMotDePasse() {
		// TODO Auto-generated method stub
		return motDePasse;
	}
	*/
}
