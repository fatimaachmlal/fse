package net.javaguides.springboot.model;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

@Entity
@Table(name="beneficiaire")
@Data
public class Beneficiaire {

	public Beneficiaire(
			long id,
			String nom,
			String prenom,
			String cin,
			Date dateNaissance,
			String sexe,
			String lieuNaissance,
			String nDossier,
			String codeAld,
			String relationFamiliale,
			Adherent adherent
			){
		this.nom=nom;
		this.prenom=prenom;
		this.id=id;
		this.cin=cin;
		this.dateNaissance=dateNaissance;
		this.sexe=sexe;
		this.nDossier=nDossier;
		this.codeAld=codeAld;
		this.relationFamiliale=relationFamiliale;
		this.adherent=adherent;
		
	}
	
	public Beneficiaire(
			Adherent adherent,
			long id,
			String nom,
			String prenom,
			String cin,
			String lieuNaissance,
			String relationFamiliale
			
			){
		this.adherent=adherent;
		this.nom=nom;
		this.prenom=prenom;
		this.id=id;
		this.cin=cin;
		this.relationFamiliale=relationFamiliale;
		
	}
	public Beneficiaire(){
		
	}
	
	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getnDossier() {
		return nDossier;
	}

	public void setnDossier(String nDossier) {
		this.nDossier = nDossier;
	}

	public String getLieuNaissance() {
		return lieuNaissance;
	}

	public String getCin() {
		return cin;
	}

	public String getCodeAld() {
		return codeAld;
	}

	public String getRelationFamiliale() {
		return relationFamiliale;
	}

	public String getSexe() {
		return sexe;
	}

	public Adherent getAdherent() {
		return adherent;
	}

	public Set<Ald> getAlds() {
		return alds;
	}

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long id;
	
	@Column(name = "nom")
	private String nom;

	@Column(name = "prenom")
	private String prenom;

	@Column(name = "date_naissance")
	private Date dateNaissance;
	
	@Column(name="lieu_naissance")
	private String lieuNaissance;
	
	@Column(name="nDossier")
	private String nDossier;
	
	@Column(name="cin")
	private String cin;
	
	@Column(name="codeAld")
	private String codeAld;
	
	@Column(name="relationFamiliale")
	private String relationFamiliale;
	
	@Column(name="sexe")
	private String sexe;
	
	@ManyToOne(fetch = FetchType.EAGER)
	private Adherent adherent;
	
	@OneToMany(cascade = CascadeType.ALL,mappedBy = "beneficiaire")
	@JsonIgnoreProperties({"hibernateLazyInitializer","handler"})
	@EqualsAndHashCode.Exclude
	@ToString.Exclude
	private Set<Ald> alds=new HashSet<>();

	public void setPrenom(String prenom2) {
		// TODO Auto-generated method stub
		this.prenom=prenom2;
		
	}

	public void setNom(String nom2) {
		// TODO Auto-generated method stub
		this.nom=nom2;
		
	}

	public void setDateNaissance(Date dateNaissance2) {
		// TODO Auto-generated method stub
		this.dateNaissance=dateNaissance2;
		
	}

	public void setLieuNaissance(String lieuNaissance2) {
		// TODO Auto-generated method stub
		this.lieuNaissance=lieuNaissance2;
		
	}

	public void setNDossier(String nDossier2) {
		// TODO Auto-generated method stub
		this.nDossier=nDossier2;
		
	}

	public void setCodeAld(String codeAld2) {
		// TODO Auto-generated method stub
		this.codeAld=codeAld2;
		
	}

	public void setAlds(Set<Ald> alds2) {
		// TODO Auto-generated method stub
		this.alds=alds2;
		
	}

	public void setAdherent(Adherent adherent2) {
		// TODO Auto-generated method stub
		this.adherent=adherent2;
		
	}

	public void setCin(String cin2) {
		// TODO Auto-generated method stub
		this.cin=cin2;
		
	}

	public void setSexe(String sexe2) {
		// TODO Auto-generated method stub
		this.sexe=sexe2;
		
	}

	public void setRelationFamiliale(String relationFamiliale2) {
		// TODO Auto-generated method stub
		this.relationFamiliale=relationFamiliale2;
		
	}

	public String getNom() {
		// TODO Auto-generated method stub
		return nom;
	}

	public String getPrenom() {
		// TODO Auto-generated method stub
		return prenom;
	}

	public Date getDateNaissance() {
		// TODO Auto-generated method stub
		return dateNaissance;
	}


	
}
