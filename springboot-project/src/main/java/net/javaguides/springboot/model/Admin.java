package net.javaguides.springboot.model;

import java.io.File;
import java.sql.Date;
import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.springframework.security.core.GrantedAuthority;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;



@Entity
@Table(name="admin")
@Data @NoArgsConstructor @AllArgsConstructor
@EqualsAndHashCode(callSuper = true)
public class Admin extends Personne{
	



	@ManyToMany(fetch = FetchType.LAZY)
    @JoinTable( name = "admin_roles", joinColumns = @JoinColumn(name = "id"),inverseJoinColumns = @JoinColumn(name = "role_id")) 
	private Set<Role> roles = new HashSet<>();

	
	
	
	
	
	public String getMotdePasse() {
		// TODO Auto-generated method stub
		return motDePasse;
	}

	
	
    public Date getDateNaissance() {
		// TODO Auto-generated method stub
		return getDateNaissance();
	}

	
    public Sexe getSexe() {
		// TODO Auto-generated method stub
		return sexe;
	}

	public String getAdresse() {
		// TODO Auto-generated method stub
		return adresse;
	}

	public String getEmail() {
		// TODO Auto-generated method stub
		return email;
	}

	
	
	
	public void setVille(String ville) {
		// TODO Auto-generated method stub
		this.ville=ville;
		
	}

	public void setSituationFamiliale(SituationFamiliale situationFamiliale) {
		// TODO Auto-generated method stub
		this.situationFamiliale=situationFamiliale;
		
	}

	public void setSexe(Sexe sexe) {
		// TODO Auto-generated method stub
		this.sexe=sexe;
		
	}

	public void setPrenom(String prenom) {
		// TODO Auto-generated method stub
		this.prenom=prenom;
		
	}

	public void setNom(String nom) {
		// TODO Auto-generated method stub
		this.nom=nom;
		
	}

	public void setMotDePasse(String motDePasse) {
		// TODO Auto-generated method stub
		this.motDePasse=motDePasse;
		
	}

	public void setEmail(String email) {
		// TODO Auto-generated method stub
		this.email=email;
		
	}

	public void setDateNaissance(Date dateNaissance) {
		// TODO Auto-generated method stub
		this.dateNaissance=dateNaissance;
		
	}

	public void setCodePostal(String codePostal) {
		// TODO Auto-generated method stub
		this.codePostal=codePostal;
		
	}

	public void setCin(String cin) {
		// TODO Auto-generated method stub
		this.cin=cin;
		
	}

	public void setAdresse(String adresse) {
		// TODO Auto-generated method stub
		this.adresse=adresse;
		
	}

	public void setUsername(String username) {
		// TODO Auto-generated method stub
		this.username=username;
		
	}

	public void setTelephone(String telephone) {
		// TODO Auto-generated method stub
		this.telephone=telephone;
		
	}

	public void setRoles(Set<Role> roles2) {
		// TODO Auto-generated method stub
		this.roles=roles2;
		
	}

	public Set<Role> getRoles() {
		// TODO Auto-generated method stub
		return roles;
	}

	public String getUsername() {
		// TODO Auto-generated method stub
		return username;
	}

	public String getMotDePasse() {
		// TODO Auto-generated method stub
		return motDePasse;
	}
	
}
