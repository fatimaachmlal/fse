package net.javaguides.springboot.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Table(name="acte_radio")
@Data
@NoArgsConstructor 
@AllArgsConstructor
public class ActeRadio {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long id;
	
	@Column(name = "code_acte")
	private String codeActe;
	
	@Column(name = "lettre_cle")
	private String lettreCle;
	
	@Column(name = "prix")
	private double prix;
	
	@Column(name = "date_acte")
	@JsonFormat(pattern="yyyy-MM-dd")
	private Date dateActe;
	
	@Column(name = "ngap")
	private String ngap;
	
	@ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "radiologie_id", referencedColumnName = "id")
    @JsonIgnoreProperties("actes")
	private Radiologie radiologie;
	
	@ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "Adherent_id", referencedColumnName = "id")
    @JsonIgnoreProperties("actes")
	private Adherent adherent;
	
	@ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "Medecin_id", referencedColumnName = "id")
    @JsonIgnoreProperties("actes")
	private Medecin medecin;
	
	
    @Transient 
	protected String nom;

    @Transient 
	protected String prenom;
    @Transient 
	protected String telephone;
    @Transient 
   	protected String cin;
    
    @Transient 
   	protected long numeroDossier;
    
	public ActeRadio(long id, String codeActe, String lettreCle, double prix, Date dateActe,  String nom,
			String prenom, String telephone,String cin,long numeroDossier,String ngap) {
		super();
		this.id = id;
		this.codeActe = codeActe;
		this.lettreCle = lettreCle;
		this.prix = prix;
		this.dateActe = dateActe;
		this.nom = nom;
		this.prenom = prenom;
		this.telephone = telephone;
		this.cin = cin;
		this.numeroDossier=numeroDossier;
		this.ngap=ngap;
	}




	public ActeRadio(String nom, String prenom, String telephone) {
		super();
		this.nom = nom;
		this.prenom = prenom;
		this.telephone = telephone;
	}




	public ActeRadio() {
		// TODO Auto-generated constructor stub
	}




	public void setCodeActe(String codeActe2) {
		// TODO Auto-generated method stub
		this.codeActe=codeActe2;
		
	}




	public void setNgap(String ngap2) {
		// TODO Auto-generated method stub
		this.ngap=ngap2;
		
	}




	public void setLettreCle(String lettreCle2) {
		// TODO Auto-generated method stub
		this.lettreCle=lettreCle2;
		
	}




	public void setPrix(double prix2) {
		// TODO Auto-generated method stub
		this.prix=prix2;
		
	}




	public void setDateActe(Date dateActe2) {
		// TODO Auto-generated method stub
		this.dateActe=dateActe2;
		
	}




	public void setRadiologie(Radiologie radiologie2) {
		// TODO Auto-generated method stub
		this.radiologie=radiologie2;
		
	}




	public void setAdherent(Adherent adherent2) {
		// TODO Auto-generated method stub
		this.adherent=adherent2;
		
	}




	public void setMedecin(Medecin medecin2) {
		// TODO Auto-generated method stub
		this.medecin=medecin2;
		
	}




	public String getCodeActe() {
		// TODO Auto-generated method stub
		return codeActe;
	}




	public Date getDateActe() {
		// TODO Auto-generated method stub
		return dateActe;
	}




	public String getLettreCle() {
		// TODO Auto-generated method stub
		return lettreCle;
	}




	public double getPrix() {
		// TODO Auto-generated method stub
		return prix;
	}




	public Radiologie getRadiologie() {
		// TODO Auto-generated method stub
		return radiologie;
	}
	
    
}
