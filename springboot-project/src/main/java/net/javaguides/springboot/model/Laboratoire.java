package net.javaguides.springboot.model;


import java.io.File;
import java.util.Collection;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.Lob;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.springframework.security.core.GrantedAuthority;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;


@Data 
@Entity
@Table(name="laboratoire")

@NoArgsConstructor 
@AllArgsConstructor
@EqualsAndHashCode(callSuper = true)
public class Laboratoire extends Personne {

/*
	@Column(name = "id_laboratoire ", unique = true, nullable = false)
	@GeneratedValue(strategy = GenerationType.AUTO)
	private int id_laboratoire;*/
	@Column(name = "raisonSociale")
	private String raisonSociale;
	
	@Column(name = "inpe")
	private String inpe;
	
	@Column(name = "ice")
	private String ice;
	
	@Column(name = "type_mutuelle")
	private String typeMutuelle;
	

	@Transient
	private byte[] photoCarteCin;
	
	@Transient
	private byte[] carteProfessionnelle;
	
	@Transient
	private byte[] cachetSignature;
	
	@Transient
	private byte[] PhotoProfile;


    @Transient 
    private long LaboratoireId;
    
 
    
	
	
	@OneToMany(cascade = CascadeType.ALL, mappedBy = "laboratoire")
	@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
	@EqualsAndHashCode.Exclude
	@ToString.Exclude
	private Set<ActeLabo> actes=new HashSet<>();
	
	/*
	@OneToMany(fetch = FetchType.EAGER, mappedBy = "laboratoire", cascade = CascadeType.ALL)
	@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
	@EqualsAndHashCode.Exclude
	@ToString.Exclude
	private Set<Image> img=new HashSet<>();
	
	
	*/
	
	
	@ManyToMany(fetch = FetchType.LAZY)
    @JoinTable( name = "labo_roles", joinColumns = @JoinColumn(name = "labo_id"),inverseJoinColumns = @JoinColumn(name = "role_id")) 
//    @JoinTable( name = "user_roles", joinColumns = @JoinColumn(name = "id"),inverseJoinColumns = @JoinColumn(name = "role_id")) 
	private Set<Role> roles = new HashSet<>();
	/*
	public Laboratoire(Long id, String nom,String prenom,Date dateNaissance, String cin, String email, String telephone,String motDePasse,String adresse, String ville, String codePostal, Sexe sexe, SituationFamiliale situationFamiliale, String raisonSociale, String inpe,String ice, String typeMutuelle, String username,byte[] cachetSignature,byte[] PhotoProfile, byte[] carteProfessionnelle, byte [] photoCarteCin) {
		
		super();
		
		this.id =id;
		this.nom =nom;
		this.prenom = prenom;
		this.dateNaissance= dateNaissance;
		this.cin= cin;
		this.email = email;
		this.telephone = telephone;
		this.motDePasse=motDePasse;
		this.adresse = adresse;
		this.ville=ville;
		this.codePostal = codePostal;
		this.sexe= sexe;
		this.situationFamiliale = situationFamiliale;
		this.raisonSociale = raisonSociale;
		this.inpe = inpe;
		this.ice = ice;
		this.typeMutuelle = typeMutuelle;
		
		this.username=username;
		this.cachetSignature=cachetSignature;
		this.PhotoProfile=PhotoProfile;
		this.carteProfessionnelle = carteProfessionnelle;
		this.photoCarteCin = photoCarteCin;

		
	}
	*/

	public Laboratoire() {
		// TODO Auto-generated constructor stub
	}
/*
	public Long getId() {
		// TODO Auto-generated method stub
		return id;
	}

	public String getUsername() {
		// TODO Auto-generated method stub
		return username;
	}

	public String getEmail() {
		// TODO Auto-generated method stub
		return email;
	}

	public String getMotDePasse() {
		// TODO Auto-generated method stub
		return motDePasse;
	}

	public void setRaisonSociale(String raisonSociale2) {
		// TODO Auto-generated method stub
		this.raisonSociale=raisonSociale2;
		
	}

	public void setTypeMutuelle(String typeMutuelle2) {
		// TODO Auto-generated method stub
		this.typeMutuelle=typeMutuelle2;
		
	}

	public void setInpe(String inpe2) {
		// TODO Auto-generated method stub
		this.inpe=inpe2;
		
	}

	public void setIce(String ice2) {
		// TODO Auto-generated method stub
		this.ice=ice2;
	}

	public void setRoles(Set<Role> roles2) {
		// TODO Auto-generated method stub
		this.roles=roles2;
		
	}

	public String getNom() {
		// TODO Auto-generated method stub
		return nom;
	}

	public String getInpe() {
		// TODO Auto-generated method stub
		return inpe;
	}

	public String getPrenom() {
		// TODO Auto-generated method stub
		return prenom;
	}

	public SituationFamiliale getSituationFamiliale() {
		// TODO Auto-generated method stub
		return situationFamiliale;
	}

	public Sexe getSexe() {
		// TODO Auto-generated method stub
		return sexe;
	}

	public String getVille() {
		// TODO Auto-generated method stub
		return ville;
	}

	public String getAdresse() {
		// TODO Auto-generated method stub
		return adresse;
	}

	public String getTelephone() {
		// TODO Auto-generated method stub
		return telephone;
	}

	public String getCin() {
		// TODO Auto-generated method stub
		return cin;
	}

	public Date getDateNaissance() {
		// TODO Auto-generated method stub
		return dateNaissance;
	}

	public String getRaisonSociale() {
		// TODO Auto-generated method stub
		return raisonSociale;
	}

	public String getIce() {
		// TODO Auto-generated method stub
		return cin;
	}

	public Set<Role> getRoles() {
		// TODO Auto-generated method stub
		return roles;
	}

	public void setMotDePasse(CharSequence motDePasse) {
		// TODO Auto-generated method stub
		this.motDePasse=this.motDePasse;
		
	}

	public void setActes(Set<ActeLabo> actes2) {
		// TODO Auto-generated method stub
		this.actes=actes2;
		
	}

	public Set<ActeLabo> getActes() {
		// TODO Auto-generated method stub
		return actes;
	}

	public String getCodePostal() {
		// TODO Auto-generated method stub
		return codePostal;
	}

	*/

	public String getRaisonSociale() {
		return raisonSociale;
	}

	public void setRaisonSociale(String raisonSociale) {
		this.raisonSociale = raisonSociale;
	}

	public String getInpe() {
		return inpe;
	}

	public void setInpe(String inpe) {
		this.inpe = inpe;
	}

	public String getIce() {
		return ice;
	}

	public void setIce(String ice) {
		this.ice = ice;
	}

	public String getTypeMutuelle() {
		return typeMutuelle;
	}

	public void setTypeMutuelle(String typeMutuelle) {
		this.typeMutuelle = typeMutuelle;
	}

	public byte[] getPhotoCarteCin() {
		return photoCarteCin;
	}

	public void setPhotoCarteCin(byte[] photoCarteCin) {
		this.photoCarteCin = photoCarteCin;
	}

	public byte[] getCarteProfessionnelle() {
		return carteProfessionnelle;
	}

	public void setCarteProfessionnelle(byte[] carteProfessionnelle) {
		this.carteProfessionnelle = carteProfessionnelle;
	}

	public byte[] getCachetSignature() {
		return cachetSignature;
	}

	public void setCachetSignature(byte[] cachetSignature) {
		this.cachetSignature = cachetSignature;
	}

	public byte[] getPhotoProfile() {
		return PhotoProfile;
	}

	public void setPhotoProfile(byte[] photoProfile) {
		PhotoProfile = photoProfile;
	}

	public long getLaboratoireId() {
		return LaboratoireId;
	}

	public void setLaboratoireId(long laboratoireId) {
		LaboratoireId = laboratoireId;
	}

	public Set<ActeLabo> getActes() {
		return actes;
	}

	public void setActes(Set<ActeLabo> actes) {
		this.actes = actes;
	}

	public Set<Role> getRoles() {
		return roles;
	}

	public void setRoles(Set<Role> roles) {
		this.roles = roles;
	}

	

	

	
	

}
