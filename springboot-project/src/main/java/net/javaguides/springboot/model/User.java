package net.javaguides.springboot.model;

import java.io.File;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Entity
@Table(name = "users"/*
						 * , uniqueConstraints = {
						 * 
						 * @UniqueConstraint(columnNames = "username"),
						 * 
						 * @UniqueConstraint(columnNames = "email") }
						 */)

@Data @NoArgsConstructor 
@AllArgsConstructor(staticName = "build")
@EqualsAndHashCode(callSuper = true)
public class User extends Personne{
	
	@NotBlank
	@Size(max = 20)
	private String username;
	
	@Column(name = "specialite")
	private String specialite;
	
	@Column(name = "inpe")
	private String inpe;
	
	@Column(name = "ice")
	private String ice;
	
	@Column(name = "signature")
	private File signature;
	
	@Column(name = "cachet")
	private File cachet;

	
	@Column(name = "cachet_pro")
	private File cachet_pro;
	
	@OneToMany(fetch = FetchType.EAGER, mappedBy = "medecin", cascade = CascadeType.ALL)
	@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
	@EqualsAndHashCode.Exclude
	@ToString.Exclude
	private Set<ActeMed> actes=new HashSet<>();
/*	
	@OneToMany(cascade = CascadeType.ALL,mappedBy = "medecin")
	private Set<Fse> fses=new HashSet<>();

	*/
	/*@ManyToMany(fetch = FetchType.LAZY)
    @JoinTable( name = "user_roles", joinColumns = @JoinColumn(name = "user_id"),
	  inverseJoinColumns = @JoinColumn(name = "role_id")) 
	private Set<Role> roles = new HashSet<>();*/
	 
	 
}
