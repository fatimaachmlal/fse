package net.javaguides.springboot.model;


import java.time.LocalDateTime;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

import com.fasterxml.jackson.annotation.JsonFormat;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
@NoArgsConstructor 
@AllArgsConstructor
@Entity
@Table(name="fse")
@Data
public class Fse {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long id;
	
	@Column(name = "numero_dossier")
	private long numeroDossier ;
	
	@Column(name = "date_creation")
	private Date dateCreation;
	
	//@Column(name = "date")
	//private LocalDateTime date;
	@ManyToOne
	private Adherent adherent;
	
	@ManyToOne
	private ActeLabo acteLabo;
	
	@ManyToOne
	private ActeMed acteMed;
	
	@ManyToOne
	private ActePhar actePhar;
	
	@ManyToOne
	private ActeRadio acteRadio;
	
	
    @Transient 
	protected String nom;
    
    @Transient 
	protected String prenom;
    
    @Transient 
	protected String cin;
    
    @Transient 
	protected String adresse;
    
    @Transient 
	protected String ville;
    @Transient 
    protected Sexe sexe;
    @Transient
	protected Date dateNaissance;

    
    @Transient
	private String inpe;
    @Transient
	private String lettreCle;
    @Transient
	private double prix;
    @Transient
	@JsonFormat(pattern="yyyy-MM-dd")
	private Date dateActe;
    
    @Transient  
	private String specialite;
    
    @Transient
	private String raisonSociale;
    @Transient
	protected String telephone;
    @Transient
	private String ngap; 
    @Transient
	private String immatriculation;



	public Fse(long id) {
		super();
		this.id = id;
	}


	public Fse(String immatriculation, String nom, String prenom, String cin, String adresse, String ville ,Date dateNaissance,Sexe sexe ) {
		super();
		this.immatriculation=immatriculation;
		this.nom = nom;
		this.prenom = prenom;
		this.cin = cin;
		this.adresse = adresse;
		this.ville = ville;
		this.dateNaissance=dateNaissance;
		this.sexe=sexe;
		
	}

	// constructeur med
	public Fse(String nom, String prenom, String adresse, String ville, String telephone, String inpe, String lettreCle, Date dateActe ,double prix,String specialite ) {
		super();
		this.nom = nom;
		this.prenom = prenom;
		this.adresse = adresse;
		this.ville = ville;
		this.inpe = inpe;
		this.lettreCle = lettreCle;
		this.dateActe = dateActe;
		this.prix=prix;
		this.specialite=specialite;
		this.telephone=telephone;
	}
// constructeur radio labo 
	public Fse( String adresse, String ville, String inpe, String lettreCle, Date dateActe ,String ngap,String raisonSociale,double prix ) {
		super();

		this.adresse = adresse;
		this.ville = ville;
		this.inpe = inpe;
		this.lettreCle = lettreCle;
		this.dateActe = dateActe;
		this.prix=prix;
		this.raisonSociale=raisonSociale;
		this.ngap=ngap;
	}

	public Fse(String inpe, String lettreCle, double prix, Date dateActe) {
		super();
		this.inpe = inpe;
		this.lettreCle = lettreCle;
		this.prix = prix;
		this.dateActe = dateActe;
	}

	// constructeur pharma
	public Fse(String adresse, String ville,String inpe, double prix, Date dateActe ,String raisonSociale) {
		super();

		this.adresse = adresse;
		this.ville = ville;
		this.inpe = inpe;
		this.prix = prix;
		this.dateActe = dateActe;
		this.raisonSociale=raisonSociale;
	}


	public Fse(long id, long numeroDossier, String nom, String prenom, Date dateActe) {
		super();
		this.id = id;
		this.numeroDossier = numeroDossier;
		this.nom = nom;
		this.prenom = prenom;
		this.dateActe = dateActe;
	}


	public Fse() {
		// TODO Auto-generated constructor stub
	}


	public void setNumeroDossier(int nextInt) {
		// TODO Auto-generated method stub
		this.numeroDossier=nextInt;
	}


	public void setDateCreation(Date dateCreation2) {
		// TODO Auto-generated method stub
		this.dateCreation=dateCreation2;
		
	}


	public void setActeMed(ActeMed acteMed2) {
		// TODO Auto-generated method stub
		this.acteMed=acteMed2;
		
	}


	public void setAdherent(Adherent adherent2) {
		// TODO Auto-generated method stub
		this.adherent=adherent2;
		
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
}