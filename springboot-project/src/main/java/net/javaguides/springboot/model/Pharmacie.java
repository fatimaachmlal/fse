package net.javaguides.springboot.model;

import java.io.File;
import java.util.Collection;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.springframework.security.core.GrantedAuthority;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;


@Entity
@Table(name="pharmacie")
@Data 
@NoArgsConstructor 
@AllArgsConstructor
@EqualsAndHashCode(callSuper = true)
public class Pharmacie extends Personne{



	
	@Column(name = "raison_sociale")
	private String raisonSociale;
	
	@Column(name = "inpe")
	private String inpe;
	
	@Column(name = "ice")
	private String ice;
	
	@Transient
	private byte[] photoCarteCin;
	
	@Transient
	private byte[] carteProfessionnelle;
	
	@Transient
	private byte[] cachetSignature;
	
	@Transient
	private byte[] PhotoProfile;

	@Transient 
    private long PharmacieId;
	
	@OneToMany(cascade = CascadeType.ALL,mappedBy = "pharmacie")
	private Set<ActePhar> actes=new HashSet<>();
	
	
	@ManyToMany(fetch = FetchType.LAZY)
    @JoinTable( name = "Pharm_roles", joinColumns = @JoinColumn(name = "Pharm_id"),inverseJoinColumns = @JoinColumn(name = "role_id")) 
	private Set<Role> roles = new HashSet<>();


	public String getRaisonSociale() {
		return raisonSociale;
	}


	public void setRaisonSociale(String raisonSociale) {
		this.raisonSociale = raisonSociale;
	}


	public String getInpe() {
		return inpe;
	}


	public void setInpe(String inpe) {
		this.inpe = inpe;
	}


	public String getIce() {
		return ice;
	}


	public void setIce(String ice) {
		this.ice = ice;
	}


	

	

	
	public byte[] getPhotoCarteCin() {
		return photoCarteCin;
	}


	public void setPhotoCarteCin(byte[] photoCarteCin) {
		this.photoCarteCin = photoCarteCin;
	}


	public byte[] getCarteProfessionnelle() {
		return carteProfessionnelle;
	}


	public void setCarteProfessionnelle(byte[] carteProfessionnelle) {
		this.carteProfessionnelle = carteProfessionnelle;
	}


	public byte[] getCachetSignature() {
		return cachetSignature;
	}


	public void setCachetSignature(byte[] cachetSignature) {
		this.cachetSignature = cachetSignature;
	}


	public byte[] getPhotoProfile() {
		return PhotoProfile;
	}


	public void setPhotoProfile(byte[] photoProfile) {
		PhotoProfile = photoProfile;
	}


	public long getPharmacieId() {
		return PharmacieId;
	}


	public void setPharmacieId(long pharmacieId) {
		PharmacieId = pharmacieId;
	}


	public Set<ActePhar> getActes() {
		return actes;
	}


	public void setActes(Set<ActePhar> actes) {
		this.actes = actes;
	}


	public Set<Role> getRoles() {
		return roles;
	}


	public void setRoles(Set<Role> roles) {
		this.roles = roles;
	}

/*
	public void setUsername(String username) {
		// TODO Auto-generated method stub
		this.username=username;
		
	}


	public void setEmail(String email) {
		// TODO Auto-generated method stub
		this.email=email;
		
	}


	public void setInpe(String inpe2) {
		// TODO Auto-generated method stub
		this.inpe=inpe2;
		
	}


	public void setIce(String ice2) {
		// TODO Auto-generated method stub
		this.ice=ice2;
	}


	public void setRaisonSociale(String raisonSociale2) {
    this.raisonSociale=raisonSociale2;		
	}


	public void setRoles(Set<Role> roles2) {
		// TODO Auto-generated method stub
		this.roles=roles2;
		
	}


	public Long getId() {
		// TODO Auto-generated method stub
		return id;
	}


	public String getNom() {
		// TODO Auto-generated method stub
		return nom;
	}


	public String getInpe() {
		// TODO Auto-generated method stub
		return inpe;
	}


	public String getPrenom() {
		// TODO Auto-generated method stub
		return prenom;
	}


	public SituationFamiliale getSituationFamiliale() {
		// TODO Auto-generated method stub
		return situationFamiliale;
	}


	public Sexe getSexe() {
		// TODO Auto-generated method stub
		return sexe;
	}


	public String getVille() {
		// TODO Auto-generated method stub
		return ville;
	}


	public String getAdresse() {
		// TODO Auto-generated method stub
		return adresse;
	}


	public String getTelephone() {
		// TODO Auto-generated method stub
		return telephone;
	}


	public String getEmail() {
		// TODO Auto-generated method stub
		return email;
	}


	public String getCin() {
		// TODO Auto-generated method stub
		return cin;
	}


	public Date getDateNaissance() {
		// TODO Auto-generated method stub
		return dateNaissance;
	}


	public String getRaisonSociale() {
		// TODO Auto-generated method stub
		return raisonSociale;
	}


	public String getIce() {
		// TODO Auto-generated method stub
		return ice;
	}


	public String getUsername() {
		// TODO Auto-generated method stub
		return username;
	}


	public String getMotDePasse() {
		// TODO Auto-generated method stub
		return motDePasse;
	}


	public Set<Role> getRoles() {
		// TODO Auto-generated method stub
		return roles;
	}


	public Set<ActePhar> getActes() {
		// TODO Auto-generated method stub
		return actes;
	}


	public void setActes(Set<ActePhar> actes2) {
		// TODO Auto-generated method stub
		this.actes=actes2;
	}


	public File getCachet() {
		// TODO Auto-generated method stub
		return cachet;
	}


	public void setCachet(File cachet2) {
		// TODO Auto-generated method stub
		this.cachet=cachet2;
		
	}


	public String getCodePostal() {
		// TODO Auto-generated method stub
		return codePostal;
	}

*/
	




	


	


	


	


	


	


	


	


	


	
	

	
}
