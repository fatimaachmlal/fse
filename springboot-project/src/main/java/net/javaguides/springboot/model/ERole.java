package net.javaguides.springboot.model;

public enum ERole {
	ROLE_USER,
    ROLE_MODERATOR,
    ROLE_ADMIN,
    
    ROLE_ADHERENT,
    ROLE_MEDCECIN,
    ROLE_PHRMACIE,
    ROLE_LABORATOIRE,
    ROLE_RADIOLOGIE,
    
}
