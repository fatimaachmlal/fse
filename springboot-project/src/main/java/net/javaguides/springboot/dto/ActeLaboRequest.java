package net.javaguides.springboot.dto;

import java.util.Date;

import javax.validation.constraints.NotNull;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import net.javaguides.springboot.model.Adherent;
import net.javaguides.springboot.model.CodeActe;
import net.javaguides.springboot.model.Laboratoire;
import net.javaguides.springboot.model.Medecin;
@Data
@AllArgsConstructor
@NoArgsConstructor
public class ActeLaboRequest {
	@NotNull
	private String codeActe;
	@NotNull
	private String lettreCle;
	@NotNull
	private double prix;
	@NotNull
	private String ngap; 
	@NotNull
	private Date dateActe;
	private Laboratoire laboratoire;
	private Adherent adherent;
	private Medecin medecin;
	public double getPrix() {
		// TODO Auto-generated method stub
		return prix;
	}
	public Laboratoire getLaboratoire() {
		// TODO Auto-generated method stub
		return laboratoire;
	}
	public String getLettreCle() {
		// TODO Auto-generated method stub
		return lettreCle;
	}
	public Date getDateActe() {
		// TODO Auto-generated method stub
		return dateActe;
	}
	public String getCodeActe() {
		// TODO Auto-generated method stub
		return codeActe;
	}
	public Adherent getAdherent() {
		// TODO Auto-generated method stub
		return adherent;
	}
	public Medecin getMedecin() {
		// TODO Auto-generated method stub
		return medecin;
	}
	public String getNgap() {
		// TODO Auto-generated method stub
		return ngap;
	}

}
