package net.javaguides.springboot.dto;

import java.io.File;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Lob;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;

import org.springframework.web.multipart.MultipartFile;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import net.javaguides.springboot.model.ActeLabo;
import net.javaguides.springboot.model.Fse;
@Data
@AllArgsConstructor(staticName = "build")
@NoArgsConstructor
@EqualsAndHashCode(callSuper = true)
public class LaboratoireRequest extends PersonneRequest {
	@NotNull(message = "la raisonSociale ne doit pas être null")
	private String raisonSociale;
	
	@Pattern(regexp = "^\\d{9}$",message = "L'inpe est invalide ")
	private String inpe;
		
	@Pattern(regexp = "^\\d{15}$",message = "L'ice est invalide ")
	private String ice;
	
	
	@NotNull(message = "le typeMutuelle ne doit pas être null")

    private String typeMutuelle;
	@NotNull(message = "le role ne doit pas être null")
	private Set<String> role;
	private String cachet;
	private MultipartFile file;
	private File cartePro;
	

	private Set<ActeLabo> actes=new HashSet<>();
	//private Set<Fse> fses=new HashSet<>();


	public String getRaisonSociale() {
		// TODO Auto-generated method stub
		return raisonSociale;
	}


	public String getTypeMutuelle() {
		// TODO Auto-generated method stub
		return typeMutuelle;
	}


	public String getInpe() {
		// TODO Auto-generated method stub
		return inpe;
	}


	public String getIce() {
		// TODO Auto-generated method stub
		return ice;
	}


	public Set<String> getRole() {
		// TODO Auto-generated method stub
		return role;
	}


	public Set<ActeLabo> getActes() {
		// TODO Auto-generated method stub
		return actes;
	}


	public MultipartFile getFile() {
		// TODO Auto-generated method stub
		return file;
	}
}
