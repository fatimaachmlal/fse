package net.javaguides.springboot.dto;

import java.io.File;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.FetchType;
import javax.persistence.OneToMany;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonManagedReference;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;
import net.javaguides.springboot.model.ActePhar;
import net.javaguides.springboot.model.ActeRadio;
import net.javaguides.springboot.model.Fse;

@Data
@AllArgsConstructor(staticName = "build")
@NoArgsConstructor
@EqualsAndHashCode(callSuper = true)
public class RadiologieRequest extends PersonneRequest{
	
	@NotNull(message = "la raison sociale ne doit pas être null")
	private String raisonSociale;
	
	@Pattern(regexp = "^\\d{9}$",message = "L'inpe est invalide ")
	private String inpe;
	
	@Pattern(regexp = "^\\d{15}$",message = "L'ice est invalide ")
	private String ice;
	
	@NotNull(message = "le role ne doit pas être null")
	private Set<String> role;

	private File signature;
	
	private File cachet;
	
	private Set<ActePhar> actes=new HashSet<>();

	public String getRaisonSociale() {
		// TODO Auto-generated method stub
		return raisonSociale;
	}

	public String getInpe() {
		// TODO Auto-generated method stub
		return inpe;
	}

	public String getIce() {
		// TODO Auto-generated method stub
		return ice;
	}

	public Set<String> getRole() {
		// TODO Auto-generated method stub
		return role;
	}

	public File getSignature() {
		// TODO Auto-generated method stub
		return signature;
	}

	public File getCachet() {
		// TODO Auto-generated method stub
		return cachet;
	}

/*	
	@OneToMany(fetch = FetchType.EAGER, mappedBy = "radiologie", cascade = CascadeType.ALL)
	@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
	@EqualsAndHashCode.Exclude
	@ToString.Exclude
	@JsonManagedReference
	private Set<ActeRadio> actes=new HashSet<>();
	
	@OneToMany(cascade = CascadeType.ALL,mappedBy = "radiologie")
	@JsonManagedReference
	private Set<Fse> fses=new HashSet<>();
*/
}
