package net.javaguides.springboot.dto;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.validation.constraints.NotNull;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import net.javaguides.springboot.model.Adherent;
import net.javaguides.springboot.model.CodeActe;
import net.javaguides.springboot.model.Medecin;
import net.javaguides.springboot.model.Radiologie;




@Data
@AllArgsConstructor
@NoArgsConstructor
public class ActeRadioRequest {
	
	@NotNull(message = "SVP renseigner le code acte")
	private String codeActe;
	
	@NotNull(message = "SVP renseigner la lettre cle")
	private String lettreCle;
	
	@NotNull(message = "SVP renseigner le prix")
	private double prix;
	
	@NotNull(message = "SVP renseigner la date")
	private Date dateActe;
	
	@NotNull(message = "SVP renseigner NGAP")
	private String ngap;
	
	@NotNull(message = "SVP renseigner radiologie")
	private Radiologie radiologie;
	
	
	@NotNull(message = "SVP renseigner adherent")
	private Adherent adherent;
	
	@NotNull(message = "SVP renseigner medecin")
	private Medecin medecin;

	public String getCodeActe() {
		// TODO Auto-generated method stub
		return codeActe;
	}

	public String getNgap() {
		// TODO Auto-generated method stub
		return ngap;
	}

	public String getLettreCle() {
		// TODO Auto-generated method stub
		return lettreCle;
	}

	public double getPrix() {
		// TODO Auto-generated method stub
		return prix;
	}

	public Date getDateActe() {
		// TODO Auto-generated method stub
		return dateActe;
	}

	public Radiologie getRadiologie() {
		// TODO Auto-generated method stub
		return radiologie;
	}

	public Adherent getAdherent() {
		// TODO Auto-generated method stub
		return adherent;
	}

	public Medecin getMedecin() {
		// TODO Auto-generated method stub
		return medecin;
	}



}
