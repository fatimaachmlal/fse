package net.javaguides.springboot.dto;

import java.util.Date;


import javax.validation.constraints.NotNull;



import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import net.javaguides.springboot.model.Adherent;
import net.javaguides.springboot.model.Medecin;
import net.javaguides.springboot.model.Pharmacie;


@Data
@AllArgsConstructor
@NoArgsConstructor
public class ActePharRequest {
	
	@NotNull
	private double prix;
	@NotNull
	private Date dateActe;
	
	
	@NotNull
	private Pharmacie pharmacie;
	
	@NotNull(message = "SVP renseigner adherent")
	private Adherent adherent;
	
	@NotNull(message = "SVP renseigner medecin")
	private Medecin medecin;

	public Date getDateActe() {
		// TODO Auto-generated method stub
		return dateActe;
	}

	public double getPrix() {
		// TODO Auto-generated method stub
		return prix;
	}

	public Pharmacie getPharmacie() {
		// TODO Auto-generated method stub
		return pharmacie;
	}

	public Adherent getAdherent() {
		// TODO Auto-generated method stub
		return adherent;
	}

	public Medecin getMedecin() {
		// TODO Auto-generated method stub
		return medecin;
	}
	
	
	

}
