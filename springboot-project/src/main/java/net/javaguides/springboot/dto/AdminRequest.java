package net.javaguides.springboot.dto;

import java.util.Set;
import java.io.File;

import javax.validation.constraints.NotNull;

import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
@AllArgsConstructor(staticName = "build")
@NoArgsConstructor
@EqualsAndHashCode(callSuper = true)

public class AdminRequest extends PersonneRequest{
	@NotNull
	private Set<String> role;

	public Set<String> getRole() {
		return role;
	}

	
}
