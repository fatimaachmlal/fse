package net.javaguides.springboot.dto;


import javax.validation.constraints.NotNull;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
@Data
@AllArgsConstructor(staticName = "build")
@NoArgsConstructor
public class AssurenceComplementaireRequest {
	
	@NotNull
	private String NomAssurance;
	
	@NotNull
	private String NumPolice;
	
	private long adherentId;
	
	public String getNomAssurance() {
		return NomAssurance;
	}
	public void setNomAssurance(String nomAssurance) {
		NomAssurance = nomAssurance;
	}
	public String getNumPolice() {
		return NumPolice;
	}
	public void setNumPolice(String numPolice) {
		NumPolice = numPolice;
	}
	public long getAdherentId() {
		return adherentId;
	}
	public void setAdherentId(long adherentId) {
		this.adherentId = adherentId;
	}
	
	
	

	

}
