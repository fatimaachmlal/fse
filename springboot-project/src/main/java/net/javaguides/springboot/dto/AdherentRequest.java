package net.javaguides.springboot.dto;



import java.io.File;
import java.sql.Date;
import java.util.HashSet;
import java.util.Set;

import javax.validation.constraints.NotNull;

//import javax.validation.constraints.*;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import net.javaguides.springboot.model.Ald;
import net.javaguides.springboot.model.Beneficiaire;
import net.javaguides.springboot.model.Fse;
import net.javaguides.springboot.model.Sexe;
import net.javaguides.springboot.model.SituationFamiliale;
@Data
@AllArgsConstructor(staticName = "build")
@NoArgsConstructor
@EqualsAndHashCode(callSuper = true)
public class AdherentRequest extends PersonneRequest{

	@NotNull
	private String typeMutuelle;
	
	@NotNull
    private String immatriculation;
	private File carteMutuelle;
	
	@NotNull
	private Set<String> role;
	private Set<Beneficiaire> beneficiaires=new HashSet<>();
    private Set<Fse> fses=new HashSet<>();
	private Set<Ald> alds=new HashSet<>();
	
    public File getCarteMutuelle() {
		return carteMutuelle;
	}
	public void setCarteMutuelle(File carteMutuelle) {
		this.carteMutuelle = carteMutuelle;
	}
	
    public Set<Beneficiaire> getBeneficiaires() {
		return beneficiaires;
	}
	public void setBeneficiaires(Set<Beneficiaire> beneficiaires) {
		this.beneficiaires = beneficiaires;
	}
	
	public Set<Fse> getFses() {
		return fses;
	}
	public void setFses(Set<Fse> fses) {
		this.fses = fses;
	}
	public Set<Ald> getAlds() {
		return alds;
	}
	public void setAlds(Set<Ald> alds) {
		this.alds = alds;
	}
	
	public String getTypeMutuelle() {
		// TODO Auto-generated method stub
		return typeMutuelle;
	}
	public void setTypeMutuelle(String typeMutuelle) {
		// TODO Auto-generated method stub
		this.typeMutuelle=typeMutuelle;
	}
	public String getImmatriculation() {
		// TODO Auto-generated method stub
		return immatriculation;
	}
	public void setImmatriculation(String immatriculation) {
		// TODO Auto-generated method stub
		this.immatriculation=immatriculation;
	}
	
	public Set<String> getRole() {
		// TODO Auto-generated method stub
		return role;
	}
	
}
