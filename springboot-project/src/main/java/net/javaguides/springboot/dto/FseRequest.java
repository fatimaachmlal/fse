package net.javaguides.springboot.dto;

import java.io.File;
import java.time.LocalDateTime;
import java.util.Date;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.ManyToOne;
import javax.validation.constraints.NotNull;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import net.javaguides.springboot.model.ActeLabo;
import net.javaguides.springboot.model.ActeMed;
import net.javaguides.springboot.model.ActePhar;
import net.javaguides.springboot.model.ActeRadio;
import net.javaguides.springboot.model.Adherent;
import net.javaguides.springboot.model.Ald;
import net.javaguides.springboot.model.Beneficiaire;
import net.javaguides.springboot.model.Fse;
import net.javaguides.springboot.model.Laboratoire;
import net.javaguides.springboot.model.Medecin;
import net.javaguides.springboot.model.Pharmacie;
import net.javaguides.springboot.model.Radiologie;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class FseRequest {
	private Medecin medecin;
	
	private long numeroDossier ;
	
	private Date dateCreation;
	private LocalDateTime date;
	
	private Adherent adherent;
	
	private ActeLabo acteLabo;
	
	private ActeMed acteMed;
	
	private ActePhar actePhar;
	
	private ActeRadio ActeRadio;

	public void setAdherent(Adherent adherent2) {
		// TODO Auto-generated method stub
		this.adherent=adherent2;
		
	}

	public void setDateCreation(Date datecreation) {
		// TODO Auto-generated method stub
		this.dateCreation=datecreation;
		
	}

	public void setActeMed(ActeMed newacteMed) {
		// TODO Auto-generated method stub
		this.acteMed=newacteMed;
		
	}

	public Date getDateCreation() {
		// TODO Auto-generated method stub
		return dateCreation;
	}

	public ActeMed getActeMed() {
		// TODO Auto-generated method stub
		return acteMed;
	}

	public Adherent getAdherent() {
		// TODO Auto-generated method stub
		return adherent;
	}

}
