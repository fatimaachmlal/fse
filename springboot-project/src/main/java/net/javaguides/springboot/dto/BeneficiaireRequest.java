package net.javaguides.springboot.dto;






import java.io.File;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonFormat;

//import javax.validation.constraints.*;
import lombok.NonNull;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import net.javaguides.springboot.model.Adherent;
import net.javaguides.springboot.model.Ald;
import net.javaguides.springboot.model.Beneficiaire;
import net.javaguides.springboot.model.Fse;
@Data
@AllArgsConstructor(staticName = "build")
@NoArgsConstructor

public class BeneficiaireRequest {

	@NotNull
	private String nom;
	@NotNull
    private String prenom;
	
  @JsonFormat(pattern = "dd/MM/yyyy")
    private Date dateNaissance;
	@NotNull
    private String lieuNaissance;
	
	@NotNull(message = "nDossier ne doit pas être null111111111")
	private String nDossier;
	
	@NotNull
	private String codeAld;
	@NotNull
	private String cin;
	
	
	@NotNull
	private String relationFamiliale;
	@NotNull
	private String sexe;
	
	private long adhrentId;
	

  
    //private Adherent adherent;
   
	//private Set<Ald> alds=new HashSet<>();

	public String getnDossier() {
		return nDossier;
	}

	public void setnDossier(String nDossier) {
		this.nDossier = nDossier;
	}

	public long getAdhrentId() {
		return adhrentId;
	}

	public void setAdhrentId(long adhrentId) {
		this.adhrentId = adhrentId;
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public String getPrenom() {
		return prenom;
	}

	public void setPrenom(String prenom) {
		this.prenom = prenom;
	}

	public Date getDateNaissance() {
		return dateNaissance;
	}

	public void setDateNaissance(Date dateNaissance) {
		this.dateNaissance = dateNaissance;
	}

	public String getLieuNaissance() {
		return lieuNaissance;
	}

	public void setLieuNaissance(String lieuNaissance) {
		this.lieuNaissance = lieuNaissance;
	}

	public String getNDossier() {
		return nDossier;
	}

	public void setNDossier(String nDossier) {
		this.nDossier = nDossier;
	}

	public String getCodeAld() {
		return codeAld;
	}

	public void setCodeAld(String codeAld) {
		this.codeAld = codeAld;
	}

	public String getCin() {
		return cin;
	}

	public void setCin(String cin) {
		this.cin = cin;
	}

	public String getRelationFamiliale() {
		return relationFamiliale;
	}

	public void setRelationFamiliale(String relationFamiliale) {
		this.relationFamiliale = relationFamiliale;
	}

	public String getSexe() {
		return sexe;
	}

	public void setSexe(String sexe) {
		this.sexe = sexe;
	}


	
	
}
