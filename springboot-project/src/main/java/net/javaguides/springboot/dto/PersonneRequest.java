package net.javaguides.springboot.dto;

import java.io.File;
import java.util.Date;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

import lombok.*;
import net.javaguides.springboot.model.Sexe;
import net.javaguides.springboot.model.SituationFamiliale;
@Data
@AllArgsConstructor(staticName = "build")
@NoArgsConstructor
public class PersonneRequest {
	@NotNull(message = "Le username ne doit pas être null")
	private String username;
	@NotNull(message = "Le nom ne doit pas être null")
	private String nom;
	@NotNull(message = "Le prénom ne doit pas être null")
	private String prenom;
	@NotNull(message = "Le dateNaissance ne doit pas être null")
	private Date dateNaissance;
	
	@NotNull(message = "Le Cin ne doit pas être null")
	private String cin;
	
	@Email
	private String email;
	
	@Pattern(regexp = "^\\d{10}$",message = "le numéro de téléphone est invalide ")
	private String telephone;
	@NotEmpty
	@Size(min = 8, message = "password should have at least 8 characters")
	private String motDePasse;
	@NotNull(message = "La adresse ne doit pas être null")
	private String adresse;
	@NotNull(message = "La ville ne doit pas être null")
	private String ville;
	@NotNull(message = "Le code postal ne doit pas être null")
	private String codePostal;
	@NotNull(message = "Le sexe postal ne doit pas être null")
	private Sexe sexe;
	@NotNull(message = "Le sexe postal ne doit pas être null")
	private SituationFamiliale situationFamiliale;
	
	public String getUsername() {
		return username;
		
	}
	public String getEmail() {
		// TODO Auto-generated method stub
		return email;
	}
	public String getMotDePasse() {
		// TODO Auto-generated method stub
		return motDePasse;
	}
	public String getVille() {
		// TODO Auto-generated method stub
		return ville;
	}
	public String getTelephone() {
		// TODO Auto-generated method stub
		return telephone;
	}
	public Sexe getSexe() {
		// TODO Auto-generated method stub
		return sexe;
	}
	public String getPrenom() {
		// TODO Auto-generated method stub
		return prenom;
	}
	public String getNom() {
		// TODO Auto-generated method stub
		return nom;
	}
	public Date getDateNaissance() {
		// TODO Auto-generated method stub
		return dateNaissance;
	}
	public String getCodePostal() {
		// TODO Auto-generated method stub
		return codePostal;
	}
	public String getCin() {
		// TODO Auto-generated method stub
		return cin;
	}
	public String getAdresse() {
		// TODO Auto-generated method stub
		return adresse;
	}
	public SituationFamiliale getSituationFamiliale() {
		// TODO Auto-generated method stub
		return situationFamiliale;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public void setNom(String nom) {
		this.nom = nom;
	}
	public void setPrenom(String prenom) {
		this.prenom = prenom;
	}
	public void setDateNaissance(Date dateNaissance) {
		this.dateNaissance = dateNaissance;
	}
	public void setCin(String cin) {
		this.cin = cin;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public void setTelephone(String telephone) {
		this.telephone = telephone;
	}
	public void setMotDePasse(String motDePasse) {
		this.motDePasse = motDePasse;
	}
	public void setAdresse(String adresse) {
		this.adresse = adresse;
	}
	public void setVille(String ville) {
		this.ville = ville;
	}
	public void setCodePostal(String codePostal) {
		this.codePostal = codePostal;
	}
	public void setSexe(Sexe sexe) {
		this.sexe = sexe;
	}
	public void setSituationFamiliale(SituationFamiliale situationFamiliale) {
		this.situationFamiliale = situationFamiliale;
	}
	

	
}
