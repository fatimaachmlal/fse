package net.javaguides.springboot.dto;




import javax.validation.constraints.NotNull;

//import javax.validation.constraints.*;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import net.javaguides.springboot.model.Adherent;
import net.javaguides.springboot.model.Beneficiaire;
@Data
@AllArgsConstructor(staticName = "build")
@NoArgsConstructor


public class AldRequest {
	

	public String getCodeAld() {
		return codeAld;
	}
	public void setCodeAld(String codeAld) {
		this.codeAld = codeAld;
	}
	public String getnDossier() {
		return nDossier;
	}
	public void setnDossier(String nDossier) {
		this.nDossier = nDossier;
	}
	public Beneficiaire getBeneficiaire() {
		return beneficiaire;
	}
	public void setBeneficiaire(Beneficiaire beneficiaire) {
		this.beneficiaire = beneficiaire;
	}
	public Adherent getAdherent() {
		return adherent;
	}
	public void setAdherent(Adherent adherent) {
		this.adherent = adherent;
	}
	//@NotNull
	private String codeAld;
	//@NotNull
	private String nDossier;
	//@NotNull
    private Beneficiaire beneficiaire;
	//@NotNull
    private Adherent adherent;
       
	
	
	
	
	
	
	
	
   

}






