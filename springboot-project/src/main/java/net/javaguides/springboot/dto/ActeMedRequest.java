package net.javaguides.springboot.dto;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonFormat;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import net.javaguides.springboot.model.Adherent;
import net.javaguides.springboot.model.Beneficiaire;
import net.javaguides.springboot.model.CodeActe;
import net.javaguides.springboot.model.Medecin;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ActeMedRequest {
	@NotNull
	private String codeActe;
	@NotNull
	private String lettreCle;
	@NotNull
	private double prix;
	@NotNull
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss")
	private Date dateActe;
		
	//@NotNull
	private String Ordonnance_Biologie;	
	//@NotNull
	private String Ordonnance_Radiologie;
	//@NotNull
	private String Ordonnance_Pharmacie;
	//@NotNull
	private String Type_de_soins;
	//@NotNull
	private String Ald;	
	@NotNull
	private Medecin medecin;	
	@NotNull
	private Adherent adherent;	
	private Beneficiaire beneficiaire;
	
	public Beneficiaire getBeneficiaire() {
		return beneficiaire;
	}
	public void setBeneficiaire(Beneficiaire beneficiaire) {
		this.beneficiaire = beneficiaire;
	}
	
	public double getPrix() {
		// TODO Auto-generated method stub
		return prix;
	}
	public void setCodeActe(String codeActe) {
		this.codeActe = codeActe;
	}
	public void setLettreCle(String lettreCle) {
		this.lettreCle = lettreCle;
	}
	public void setPrix(double prix) {
		this.prix = prix;
	}
	public void setDateActe(Date dateActe) {
		this.dateActe = dateActe;
	}
	public void setOrdonnance_Biologie(String ordonnance_Biologie) {
		Ordonnance_Biologie = ordonnance_Biologie;
	}
	public void setOrdonnance_Radiologie(String ordonnance_Radiologie) {
		Ordonnance_Radiologie = ordonnance_Radiologie;
	}
	public void setOrdonnance_Pharmacie(String ordonnance_Pharmacie) {
		Ordonnance_Pharmacie = ordonnance_Pharmacie;
	}
	public void setType_de_soins(String type_de_soins) {
		Type_de_soins = type_de_soins;
	}
	public void setAld(String ald) {
		Ald = ald;
	}
	public void setMedecin(Medecin medecin) {
		this.medecin = medecin;
	}
	public void setAdherent(Adherent adherent) {
		this.adherent = adherent;
	}
	public Medecin getMedecin() {
		// TODO Auto-generated method stub
		return medecin;
	}
	public Adherent getAdherent() {
		// TODO Auto-generated method stub
		return adherent;
	}
	public String getLettreCle() {
		// TODO Auto-generated method stub
		return lettreCle;
	}
	public Date getDateActe() {
		// TODO Auto-generated method stub
		return dateActe;
	}
	public String getCodeActe() {
		// TODO Auto-generated method stub
		return codeActe;
	}
	public String getOrdonnance_Biologie() {
		// TODO Auto-generated method stub
		return Ordonnance_Biologie;
	}
	public String getOrdonnance_Radiologie() {
		// TODO Auto-generated method stub
		return Ordonnance_Radiologie;
	}
	public String getOrdonnance_Pharmacie() {
		// TODO Auto-generated method stub
		return Ordonnance_Pharmacie;
	}
	public String getType_de_soins() {
		// TODO Auto-generated method stub
		return Type_de_soins;
	}
	public String getAld() {
		// TODO Auto-generated method stub
		return Ald;
	}
	


}
