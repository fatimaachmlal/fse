package net.javaguides.springboot.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import net.javaguides.springboot.model.ActeMed;
import net.javaguides.springboot.model.Fse;
import net.javaguides.springboot.model.Sexe;
import net.javaguides.springboot.model.SituationFamiliale;

import java.io.File;
import java.sql.Date;
import java.util.HashSet;
import java.util.Set;

import javax.validation.constraints.*;

import org.springframework.web.multipart.MultipartFile;
@Data
@AllArgsConstructor(staticName = "build")
@NoArgsConstructor
@EqualsAndHashCode(callSuper = true)
public class MedecinRequest extends PersonneRequest{

	@NotNull(message = "specialite ne doit pas être null")
	private String specialite;
	
    @Pattern(regexp = "^\\d{9}$",message = "L'inpe est invalide ")
	private String inpe;
	
    @Pattern(regexp = "^\\d{15}$",message = "L'ice est invalide ")
	private String ice;

	private MultipartFile cachet;
    
	private File cartePro;
	
    @NotNull
    private String typeMutuelle;
	private Set<ActeMed> actes=new HashSet<>();
	private Set<Fse> fses=new HashSet<>();
	private Set<String> role;
	public String getInpe() {
		// TODO Auto-generated method stub
		return inpe;
	}
	public String getIce() {
		// TODO Auto-generated method stub
		return ice;
	}
	public String getSpecialite() {
		// TODO Auto-generated method stub
		return specialite;
	}
	public String getTypeMutuelle() {
		// TODO Auto-generated method stub
		return typeMutuelle;
	}
	public Set<String> getRole() {
		// TODO Auto-generated method stub
		return role;
	}
	
	
	
	

}
